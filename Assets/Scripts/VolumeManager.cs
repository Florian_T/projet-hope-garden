﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class VolumeManager : MonoBehaviour
{
    [Header("Volumes")]
    public float volumeMaster;
    public float volumeMusic;
    public float volumeSFX;
    [Header("Sliders")]
    public Slider sliderMaster;
    public Slider sliderMusic;
    public Slider sliderSFX;

    [Header("LowPass")]
    public bool activeLowPass;
    [Tooltip("Fréquence minimale et maximal dans le menu pause")]
    public Vector2 lowPassMinMax;
    [Tooltip("Vitesse pour passer de la valeur minimale et maximale du lowpass")]
    public float speedLowPass;

    [Header("Pitch")]
    public bool activePitch;
    [Tooltip("Pitch minimum et maximum")]
    public Vector2 pitchMinMax;
    [Tooltip("Vitesse pour passer de la valeur minimale et maximale du pitch")]
    public float speedPitch;

    [Header("Mixers")]
    public AudioMixerGroup mixerMaster;
    public AudioMixerGroup mixerMusic;
    public AudioMixerGroup mixerSFX;

    [HideInInspector] public AudioMixer mixer;
    private GameManager gm;

    void Start()
    {
        mixer = mixerMaster.audioMixer;
        gm = FindObjectOfType(typeof(GameManager)) as GameManager;
    }

    void Update()
    {
        #region ADD MIXER TO ALL SOUND
        AudioSource[] allAudios = FindObjectsOfType<AudioSource>() as AudioSource[];
        foreach (AudioSource audio in allAudios)
        {
            if (audio.outputAudioMixerGroup == null)
            {
                audio.outputAudioMixerGroup = mixerSFX;
            }
        }
        #endregion

        #region VOLUME SETTINGS
        mixer.SetFloat("masterVolume", Mathf.Log10(volumeMaster) * 20);
        mixer.SetFloat("musicVolume", Mathf.Log10(volumeMusic) * 20);
        mixer.SetFloat("SFXVolume", Mathf.Log10(volumeSFX) * 20);
        #endregion

        #region MENU PAUSE
        //Qaund le jeu est en pause
        if (gm.gameIsPause)
        {
            //On applique un lowpass sur la musique
            if (activeLowPass)
            {
                float temp;
                mixer.GetFloat("masterLowPass", out temp);

                float newLowPassValue = Mathf.Lerp(temp, lowPassMinMax.x, speedLowPass * Time.deltaTime);

                mixer.SetFloat("masterLowPass", newLowPassValue);
            }

            //On modifie le pitch de la musique
            if (activePitch)
            {
                float temp;
                mixer.GetFloat("masterPitch", out temp);

                float newPitchValue = Mathf.Lerp(temp, pitchMinMax.x, speedPitch * Time.deltaTime);

                mixer.SetFloat("masterPitch", newPitchValue);
            }
        }
        //Quand le jeu n'est pas en pause
        else
        {
            //On remet le lowpass par défaut
            if (activeLowPass)
            {
                float temp;
                mixer.GetFloat("masterLowPass", out temp);

                float newLowPassValue = Mathf.Lerp(temp, lowPassMinMax.y, speedLowPass * Time.deltaTime);

                mixer.SetFloat("masterLowPass", newLowPassValue);
            }

            //On remet le pitch par défaut
            if (activePitch)
            {
                float temp;
                mixer.GetFloat("masterPitch", out temp);

                float newPitchValue = Mathf.Lerp(temp, pitchMinMax.y, speedPitch * Time.deltaTime);

                mixer.SetFloat("masterPitch", newPitchValue);
            }

        }
        #endregion
    }
}
