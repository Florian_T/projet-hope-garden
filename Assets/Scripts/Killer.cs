﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Killer : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "LavaCollision")
        {
            other.GetComponentInParent<PlayerLife>().TakeDamage(99999);
        }
    }
}
