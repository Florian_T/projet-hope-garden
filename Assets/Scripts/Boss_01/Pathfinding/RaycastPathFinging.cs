﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastPathFinging : MonoBehaviour
{
    [Tooltip("Vitesse de déplacement du boss"),HideInInspector]
    public float speed;
    [Tooltip("Angle entre chaque raycast")]
    public float raycastAngle = 0.1f;
    [Tooltip("Distance de détection maximale des obstacles")]
    public float obstacleDistanceDetect = 150;
    [Tooltip("Nombre de raycast par angle (et donc aussi rayon du cone de détection)")]
    public int iteration = 15;
    [Tooltip("Cible du boss"),HideInInspector]
    public Transform target;

    //La meilleure direction prise par le boss et donc la direction vers laquelle il se déplace.
    private Vector3 bestDirection;
    //Liste des vecteurs valides pour calculer la meilleure direction
    private List<Vector3> vectorsComposeTheBestDirection = new List<Vector3>();
    //Le gameManager
    private GameManager gm;

    //Le component character controller
    CharacterController cc;

    void Start()
    {
        //Récupération du component CharacterController
        cc = GetComponent<CharacterController>();
        //Recherche du game manager
        gm = FindObjectOfType<GameManager>() as GameManager;

        //S'il n'y a pas de target, le boss prend pour cible le joueur.
        if (target == null)
            target = gm.player.transform;
    }

    void Update()
    {
        //Le delta time
        float dt = Time.deltaTime;

        //Initialisation du vecteur de mouvement à 0
        Vector3 movement = Vector3.zero;

        //Recherche autour de l'oiseau pour détecter des objets
        TestCollide();
        //Défini la meilleure direction à prendre
        GetBestDirection();

        //Le mouvement devient la meilleure direction.
        movement = bestDirection * speed * gm._timeScale;

        //Multiplication du vecteur de déplacement par le delta time
        movement *= dt;

        //Fait bouger le boss
        cc.Move(movement);
    }

    void TestCollide()
    {
        //Création de 3 array de vecteurs pour détecter où se trouve les obstacles
        Vector3[] vectorsRight = new Vector3[iteration];
        Vector3[] vectorsLeft = new Vector3[iteration];
        Vector3[] vectorsUp = new Vector3[iteration];
        Vector3[] vectorsDown = new Vector3[iteration];

        //Netoie la liste de vecteurs valides
        vectorsComposeTheBestDirection.Clear();

        for (int i = 0; i < iteration; i++)
        {
            //Initialisations des différents vecteurs
            vectorsRight[i] = transform.forward + Vector3.right * iteration * raycastAngle;
            vectorsLeft[i] = transform.forward + Vector3.left * iteration * raycastAngle;
            vectorsUp[i] = transform.forward + Vector3.up * iteration * raycastAngle;
            vectorsDown[i] = transform.forward + Vector3.down * iteration * raycastAngle;

            //Détection des obstacles
            DetectObstacle(vectorsRight[i]);
            DetectObstacle(vectorsLeft[i]);
            DetectObstacle(vectorsUp[i]);
            DetectObstacle(vectorsDown[i]);
        }
    }

    void DetectObstacle(Vector3 dir)
    {
        RaycastHit hit;

        //Si le vecteur ne touche aucun obstacle, celui ci s'ajoute dans la liste des vecteurs valides.
        if (!Physics.Raycast(transform.position, dir, out hit, obstacleDistanceDetect, ~2))
        {
            vectorsComposeTheBestDirection.Add(dir);
        }
    }

    void GetBestDirection()
    {
        //La bestdirection est l'addition de tous les vecteurs valides normalizés
        foreach (Vector3 v in vectorsComposeTheBestDirection)
        {
            bestDirection += v.normalized;
        }

        //La direction de la cible
        Vector3 targetDir = (target.position - transform.position).normalized;
        
        //Normalization du vecteur de bestDirection
        bestDirection = bestDirection.normalized;

        //On tourne le boss vers le joueur et vers sa direction de déplacement
        transform.forward = Vector3.Lerp(transform.forward, bestDirection + targetDir * 2,40*Time.deltaTime*gm._timeScale);
    }
}
