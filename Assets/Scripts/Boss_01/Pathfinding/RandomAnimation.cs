﻿/*using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomAnimation : MonoBehaviour
{
    private Animator anim;
    public float timeLeft = 1.0f;
    public int pickAnim;
    public int currentAnim;
    private bool random = true;

    public Text txtAnim;
    public Text txtNextAnim;

    private bool animIsDone = true;
    
    private GameManager gm;

    void Start()
    {
        anim = GetComponent<Animator>();
        gm = FindObjectOfType(typeof(GameManager)) as GameManager;
    }

    private void Update()
    {
        timeLeft -= Time.deltaTime;

        if (timeLeft <= 0 && anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1 )
        {

            if (pickAnim == 0 && random == true)
            {
                currentAnim = Random.Range(1, 6);
                anim.SetInteger("RandomAnim", currentAnim);
                timeLeft = 1.0f;
            }

            if (pickAnim > 0 && random == false)
            {
                currentAnim = pickAnim;
                anim.SetInteger("RandomAnim", currentAnim);
                timeLeft = 1.0f;
                pickAnim = 0;
                random = true;
            }
            animIsDone = true;
        }

        anim.speed = gm._timeScale;

        if (Input.GetKeyDown("1"))
        {
            pickAnim = 1;
            random = false;
            txtNextAnim.text = "Prochaine animation : Pheonix qui marche";
        }
        if (Input.GetKeyDown("2"))
        {
            pickAnim = 2;
            random = false;
            txtNextAnim.text = "Prochaine animation : Pheonix qui vol";
        }
        if (Input.GetKeyDown("3"))
        {   
            pickAnim = 3;
            random = false;
            txtNextAnim.text = "Prochaine animation : Attaque boule de feu";
        }
        if (Input.GetKeyDown("4"))
        {
            pickAnim = 4;
            random = false;
            txtNextAnim.text = "Prochaine animation : Attaque meteorite";
        }
        if (Input.GetKeyDown("5"))
        {
            pickAnim = 5;
            random = false;
            txtNextAnim.text = "Prochaine animation : Attaque nova";
        }

        if (anim.GetCurrentAnimatorStateInfo(0).IsName("FireBall") && animIsDone == true && anim.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.1f)
        {
            animIsDone = false;
            //StartCoroutine(gameObject.GetComponent<PhoenixFireball>().ShootFireball());

        }

        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Meteor") && animIsDone == true && anim.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.1f)
        {
            animIsDone = false;
            gameObject.GetComponent<PhoenixMeteorites>().LaunchMeteorites();
        }

        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Nova") && animIsDone == true && anim.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.1f)
        {

            animIsDone = false;
            StartCoroutine(Nova());
        }

        if (anim.GetCurrentAnimatorStateInfo(0).IsName("GroundMovement"))
        {
            txtAnim.text = "Animation en cours : Pheonix qui marche";
        }
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("PheonixFlight"))
        {
            txtAnim.text = "Animation en cours : Pheonix qui vol";
        }
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("FireBall"))
        {
            txtAnim.text = "Animation en cours : Attaque boule de feu";
        }
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Meteor"))
        {
            txtAnim.text = "Animation en cours : Attaque meteorite";
        }
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Nova"))
        {
            txtAnim.text = "Animation en cours : Attaque nova";
        }

        if (anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1f)
        {
            txtNextAnim.text = "Prochaine animation : Animation aléatoire";
        }

    }

    IEnumerator Nova()
    {
        yield return new WaitForSeconds(9f);
        gameObject.GetComponent<PhoenixNova>().CreateNova();
    }
}*/
