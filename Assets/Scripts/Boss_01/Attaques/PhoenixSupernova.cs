﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhoenixSupernova : MonoBehaviour
{
    [Header("Objets et Transform")]
    [Tooltip("Particules chargement de la Nova")]
    public GameObject ChargingNova;
    [Tooltip("Particules Nova")]
    public GameObject NovaParticles;
    [Tooltip("Sphere Nova")]
    public GameObject NovaSphere;
    
    private GameObject player;
    [Space(10)]
    [Tooltip("Zone de Spawn de la Nova")]
    public Transform Head;   

    [Header("Gestion de l'attaque")]
    [Tooltip("Est ce qu'une Nova existe ?")]
    public bool NovaExists = false;
    [Tooltip("Temps de chargement de la Nova")]
    public float _chargingTime;
    [Tooltip("Vitesse de Propagation de la Nova")]
    public float _speedNova;

    [Header("HUD")]
    [Tooltip("Panel UI de l'indicateur Nova")]
    public GameObject UiNovaIndicator;
    [Tooltip("Opacité maximum de l'indicateur entre 0 et 1")]
    [Range(0, 1)]
    public float _opacityMax;
    [Tooltip("Couleur de l'indicateur, ne pas se soucier de l'alpha")]
    public Color colorIndicator;
    [Tooltip("Vitesse d'apparition de l'indicateur")]
    public float _speedFadeIn;
    [Tooltip("Vitesse de disparition de l'indicateur")]
    public float _speedFadeOut;

    private float _alpha = 0;
    private GameManager gm;

    public AudioClip novaFireSound;
    public AudioClip novaExplosion;
    public AudioClip novaPhoenixSound;

    public AudioSource audSrcNova;

    void Start()
    {
        player = FindObjectOfType<GameManager>().player;

        audSrcNova = gameObject.GetComponent<AudioSource>();
        audSrcNova.spatialBlend = 1;
    }

    void FixedUpdate()
    {
        // HUD INDICATEUR NOVA
        // Si une Nova existe
        if (NovaExists)
        {
            RaycastHit hit;

            // Linecast pour vérifier si il y a un objet entre le boss et le joueur
            if (Physics.Linecast(Head.position, player.transform.position, out hit))
            {
                // Si le Linecast touche autre chose que le joueur
                if (hit.transform.tag != "Player")
                {
                    // l'opacité de l'indicateur baisse
                    _alpha = Mathf.Lerp(_alpha, 0, _speedFadeOut);
                }
                // Si le Linecast touche le joueur
                else
                {
                    // l'opacité de l'indicateur monte                   
                    _alpha = Mathf.Lerp(_alpha, _opacityMax, _speedFadeIn);
                }
            }
        }
        // Si il n'y a pas de Nova
        else
        {
            // l'opacité de l'indicateur baisse
            _alpha = Mathf.Lerp(_alpha, 0, _speedFadeOut);
        }

        // Changement de la couleur et de l'opacité en fonction des valeurs calculées précédemment
        UiNovaIndicator.GetComponent<Image>().color = new Color(colorIndicator.r, colorIndicator.g, colorIndicator.b, _alpha);
    }

    // Fonction de création de Nova
    public void CreateSupernova()
    {
        StartCoroutine("Charge");
    }

    //Coroutine de Création de Nova
    IEnumerator Charge()
    {
        // Chargement de la Nova
        GameObject chargingNova = Instantiate(ChargingNova) as GameObject;
        chargingNova.transform.position = Head.position;

        /*GameObject SM = FindObjectOfType<SoundManager>().gameObject;
        SM.GetComponent<SoundManager>().ChargeNova();*/

        audSrcNova.PlayOneShot(novaPhoenixSound);

        // Attendre x secondes avant de créer la Nova
        yield return new WaitForSeconds(_chargingTime);

        // Création des particules de la Nova
        GameObject newNova = Instantiate(NovaParticles) as GameObject;
        newNova.GetComponent<ParticleSystem>().startSpeed *= _speedNova;
        newNova.transform.position = Head.position;
        newNova.transform.LookAt(player.transform);

        //SM.GetComponent<SoundManager>().FireNova();
        //Animation play
        GetComponentInChildren<Animator>().speed = 1;

        audSrcNova.PlayOneShot(novaExplosion);
        audSrcNova.clip = novaFireSound;
        audSrcNova.Play();

        // Création de la sphère de collision de la Nova
        GameObject newNovaSphere = Instantiate(NovaSphere) as GameObject;
        newNovaSphere.GetComponent<NovaSphere>()._maxSize *= _speedNova;
        NovaExists = true;
        newNovaSphere.transform.position = Head.position;

        // Attendre 10 secondes et détruire la Nova
        yield return new WaitForSeconds(10f);
        Destroy(newNovaSphere);
        NovaExists = false;
    }
}
