﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageIndicator : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject != this.gameObject)
        {
            StartCoroutine(TakeDamage(0));
        }
    }

    IEnumerator TakeDamage(int damage)
    {
        Color baseColor = GetComponent<Renderer>().material.color;

        for (int i = 0; i < 3; i++)
        {
            GetComponent<Renderer>().material.SetColor("_Color", Color.red);
            yield return new WaitForSeconds(0.2f);

            GetComponent<Renderer>().material.SetColor("_Color", baseColor);
            yield return new WaitForSeconds(0.2f);
        }
    }
}
