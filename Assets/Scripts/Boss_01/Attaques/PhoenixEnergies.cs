﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhoenixEnergies : MonoBehaviour
{
    [Header("Objets et prefab")]

    [Tooltip("Prefab de l'énergie Feu")]
    public GameObject EnergyFire;
    [Tooltip("Liste des GO des possibles positions de spawn")]
    public GameObject[] EnergyPositions;

    [Header("Paramètres du pattern")]

    [Tooltip("Nombre d'objets créés")]
    public int _numberOfEnergies;

    [Tooltip("Temps maximum pour détruire les objets")]
    public float _timerDuration;
    private float _actualTimer;

    [Tooltip("Dégât par énergie")]
    public int _damageEnergy;
    [Tooltip("Heal par énergie")]
    public int _healEnergy;
    [Tooltip("Est ce que le boss est en train de faire cette attaque ?")]
    public bool _isAttacking = false;

    void Start()
    {
        // Appel de la fonction d'activation des tous les lieux des spawns d'objets
        ActiveAllEnergyPoints();
    }

    void Update()
    {
        // DEV - Lancer l'attaque
        if (Input.GetKeyDown(KeyCode.W) && _isAttacking == false)
        {           
            CreateEnergy();
        }

        // Si une attaque est en cours
        if (_isAttacking)
        {
            // Fonctionnement du timer
            _actualTimer -= Time.deltaTime;


            GameObject[] FireList;
            FireList = GameObject.FindGameObjectsWithTag("FireEnergy");

            GameObject[] IceList;
            IceList = GameObject.FindGameObjectsWithTag("IceEnergy");

            // Si tous les objets sont détruits - SUCCES
            if (FireList.Length <= 0)
            {
                // Le boss n'attaque plus, le timer est remis à 0 et on réactive les points de spawn
                _isAttacking = false;
                _actualTimer = _timerDuration;

                ActiveAllEnergyPoints();

                foreach (GameObject Ice in IceList)
                {
                    GetComponentInParent<PhoenixSoin>().BossDamage(_damageEnergy);
                    Destroy(Ice);
                }
            }

            // Si le timer est à 0 - ECHEC
            if(_actualTimer <= 0)
            {
                // Le boss n'attaque plus, le timer est remis à 0 et on réactive les points de spawn
                _isAttacking = false;
                _actualTimer = _timerDuration;

                ActiveAllEnergyPoints();

                // Destruction des objets restants
                foreach (GameObject Fire in FireList)
                {
                    GetComponentInParent<PhoenixSoin>().BossEnergyHeal(_healEnergy);
                    Destroy(Fire);
                }

                foreach (GameObject Ice in IceList)
                {
                    GetComponentInParent<PhoenixSoin>().BossDamage(_damageEnergy);
                    Destroy(Ice);
                }

                // Malus d'échec ! ------------------------------------------------------------------------------------------------
            }
        }        
    }

    // Fonction d'activation des lieux de spawn
    public void ActiveAllEnergyPoints()
    {
        foreach (GameObject EnergyPosition in EnergyPositions)
        {
            EnergyPosition.SetActive(true);
        }
    }

    // Fonction d'attaque
    public void CreateEnergy()
    {
        // Pour le nombre d'objets à créer
        for (int i = 0; i < _numberOfEnergies; i++)
        {
            // Choix aléatoire de l'index du spawn
            int _findLocation = Random.Range(0, EnergyPositions.Length);

            // Vérification que le spawn est pas déjà utilisé
            if(EnergyPositions[_findLocation].activeSelf == true)
            {
                // Création de l'objet et désactivation de ce spawn
                EnergyPositions[_findLocation].SetActive(false);
                GameObject newLo = Instantiate(EnergyFire);
                newLo.transform.position = EnergyPositions[_findLocation].transform.position;               
            }
            else
            {
                // Sinon i reprend la dernière valeur pour bien finir la boucle
                i = i - 1;
            }
        }

        // Le boss est en train d'attaquer et le timer est remis à zéro pour cette attaque
        _isAttacking = true;
        _actualTimer = _timerDuration;
    }


}
