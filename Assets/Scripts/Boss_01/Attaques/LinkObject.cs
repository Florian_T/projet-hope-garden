﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinkObject : MonoBehaviour
{
    public GameObject EnergieGlace;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnTriggerEnter(Collider collision)
    {
        // Flèche
        if (collision.gameObject.tag == "Arrow")
        {
            GameObject glace = Instantiate(EnergieGlace);
            glace.transform.position = this.gameObject.transform.position;

            Debug.Log("touché");
            Destroy(this.gameObject);
            Destroy(collision.gameObject);
        }


    }
}
