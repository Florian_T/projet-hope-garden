﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhoenixProjectileExplosion : MonoBehaviour
{
    [Tooltip("Vitesse de propagation")]
    public float speed = 10;
    [Tooltip("Durée de vie de l'objet")]
    public float lifeTime = 5;

    [Tooltip("Le mesh de l'objet contenant le material")]
    public GameObject mesh;

    //Conteur de temps de vie
    private float lifeTimeCount = 0;

    public AudioClip[] explosionSounds;

    private AudioSource audSrcExplosion;

    void Start()
    {
        audSrcExplosion = GetComponent<AudioSource>();

        int _chooseIndex = Random.Range(0, explosionSounds.Length);
        audSrcExplosion.PlayOneShot(explosionSounds[_chooseIndex]);
    }

    void Update()
    {
        //Augmentation de la taille de l'explosion
        transform.localScale += (Vector3.one - Vector3.up) * speed * Time.deltaTime;

        //Conteur de temps de vie, on rajoute du temps
        lifeTimeCount += Time.deltaTime;
        float lerpLifeTime = Mathf.InverseLerp(lifeTime, 0, lifeTimeCount);

        //Changement de l'alpha de la couleur de l'objet pour baisser l'opacité jusqu'à disparaitre
        Color newColor = mesh.GetComponent<Renderer>().material.GetColor("_BaseColor");
        newColor.a = lerpLifeTime -0.001f;

        mesh.GetComponent<Renderer>().material.SetColor("_BaseColor",newColor);

        //S'il atteint son temps de vie max, l'objet se détruit
        if (lerpLifeTime <= 0)
            Destroy(this.gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            PlayerLife pl = other.GetComponent<PlayerLife>();
            pl.TakeDamage(pl.minorDamage);
            Destroy(this.gameObject);
        }
    }
}
