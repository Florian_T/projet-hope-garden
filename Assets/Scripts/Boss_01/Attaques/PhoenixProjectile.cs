﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhoenixProjectile : MonoBehaviour
{
    [Header("Ne pas toucher, rb et script")]
    //Rigidbody de la boule de feu
    private Rigidbody rb;
    //Le script de la vie présent sur le joueur
    private PlayerLife playerLifeScript;

    //La vitesse du projectile
    public float speed = 10f;
    
    //Le gameManager
    private GameManager gm;
    
    void Start()
    {
        //On récupère le rigidbody et le gameManager
        rb = GetComponent<Rigidbody>();
        gm = FindObjectOfType(typeof(GameManager)) as GameManager;
    }
    
    void FixedUpdate()
    {
        //Vitesse de la boule de feu
        rb.velocity = transform.forward * speed * Time.deltaTime * gm._timeScale;
    }

    //normal de la zone de contacte
    Vector3 normalContact = Vector3.zero;
    //Position de la zone de contacte
    Vector3 positionContact = Vector3.zero;
    [Tooltip("Objet de l'onde de choc")]
    public GameObject ondeDeChoc;
    //Le nom de l'onde de choc
    private string ondeName = "OndeDeChoc";

    public void OnCollisionEnter(Collision other)
    {
        //Si l'objet entre en contacte avec autre chose que le phoenix
        if (other.gameObject.tag != "Phoenix" && other.gameObject.name != ondeName)
        {
            //Si cet objet est le joueur
            if (other.gameObject.tag == "Player")
            {
                //On récupère le script du joueur et on lui retire une vie
                playerLifeScript = other.gameObject.GetComponent<PlayerLife>();
                playerLifeScript.TakeDamage(playerLifeScript.majorDamage);
            }
            else
            {
                //Pour tout les points de contactes
                foreach (ContactPoint contact in other.contacts)
                {
                    //On additionne les normals et point de contactes afin d'en faire la moyenne
                    normalContact += contact.normal;
                    positionContact += contact.point;
                }

                //On trouve la normale moyenne
                normalContact /= other.contactCount;
                normalContact = normalContact.normalized;
                //Position moyenne de la collision
                positionContact /= other.contactCount;
                
                //Création de l'onde
                GameObject onde = Instantiate(ondeDeChoc);
                //assignation du nom
                onde.name = ondeName;
                //position de l'onde
                onde.transform.position = positionContact + normalContact;
                //orientation de l'onde
                onde.transform.up = normalContact;

                createFireCondamnation();
            }
            

            //On détruit le projectile après la collision
            Destroy(gameObject);
        }
    }

    [Header("CondamnationItem")]
    public int numberPerAttack;
    public GameObject fireCondamnationPrefab;
    public float explosionForce;
    public float explosionRadius;

    void createFireCondamnation()
    {
        for (int i = 0; i < numberPerAttack; i++)
        {
            GameObject newFireCondamnation = Instantiate(fireCondamnationPrefab);
            newFireCondamnation.transform.position = this.transform.position + new Vector3(Random.Range(-1,1), Random.Range(-1, 1), Random.Range(-1, 1));

            newFireCondamnation.GetComponent<fireCondamnation>().explosionForce = explosionForce;
            newFireCondamnation.GetComponent<fireCondamnation>().explosionPosition = transform.position;
            newFireCondamnation.GetComponent<fireCondamnation>().explosionRadius = explosionRadius;
        }
    }
}
