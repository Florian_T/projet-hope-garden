﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PhoenixSoin : MonoBehaviour
{
    [Header("Vie / Soin / Dégât")]
    [Space(10)]
    [Tooltip("Vie du Phoenix")]
    public float _phoenixLife;
    //Vie maximum = première valeure prise par _phoenixLife
    public float maxLife;
    [Space(10)]
    [Tooltip("Points de vie maximum que peut récupérer le boss par soin")]
    public float _maxHealHP;
    // _healHP est utilisé pour le calcul de points de vie récupérés pendant un soin
    private float _healHP;
    [Tooltip("Santé maximum que le boss peut avoir pour se soigner")]
    public float _maxLifeToHeal;
    [Tooltip("Vitesse de soin: en sec, le temps entre 2 points de vie récupérés")]
    public float _healSpeed;
    [Tooltip("Le boss est il en train de se soigner ?")]
    public bool isHealing = false;

    public GameObject Portal;

    // Liste de zones de heal du boss
    private GameObject[] healBossPoints;
    // Zone de Heal choisie
    private Transform healPointChosen;

    private GameObject player;

    private BossParameters bp;

    private bool asDeath;
    private AudioSource audSrcPhoenixLife;
    public AudioClip[] hitSound;

    public GameObject particlesObject;
    private bool asPlayParticles;

    private AudioSource audSrcHeal;
    public AudioClip healSound;

    void Start()
    {
        // Recherche de toutes les zones de heal de la map
        healBossPoints = GameObject.FindGameObjectsWithTag("HealPoint");

        player = FindObjectOfType<GameManager>().player;

        maxLife = _phoenixLife;

        bp = GetComponent<BossParameters>();

        audSrcPhoenixLife = gameObject.AddComponent<AudioSource>();
        audSrcHeal = gameObject.AddComponent<AudioSource>();

        audSrcHeal.spatialBlend = 0.5f;

    }

    void Update()
    {
        if (isHealing && !asPlayParticles)
        {
            asPlayParticles = true;

            audSrcHeal.clip = healSound;
            audSrcHeal.Play();
            audSrcHeal.loop = true;

            foreach (ParticleSystem particles in particlesObject.GetComponentsInChildren<ParticleSystem>())
            {
                particles.Play();
                particles.loop = true;
            }
        }
        else if (!isHealing)
        {
            audSrcHeal.Stop();

            foreach (ParticleSystem particles in particlesObject.GetComponentsInChildren<ParticleSystem>())
            {
                particles.Stop();
                particles.loop = false;
            }
        }
        
        // Appel de la fonction de mort du boss si sa vie est inferieure ou égale à 0
        if(_phoenixLife <= 0 && !asDeath)
        {
            asDeath = true;
            BossDeath();
        }

        // Si un healPoint est choisi, calcul de la distance boss/heal point
        /*
        if(healPointChosen != null)
        {
            float _distToHealPoint = Vector3.Distance(healPointChosen.transform.position, transform.position);
            
            // Si le boss est assez proche du healPoint il se heal
            if (_distToHealPoint < 10 && _doOnce == false)
            {
                _doOnce = true;
                bp.ChangeState(BossParameters.States.Idle);
                isHealing = true;
                _healHP = 0;
                StartCoroutine(Heal());
            }
        }
        */

        //2vite les valeurs négatives
        _phoenixLife = Mathf.Clamp(_phoenixLife, 0, maxLife);
    }

    // Coroutine liée à la fonction heal
    /*
    IEnumerator Heal()
    {
        // Le phoenix gagne une vie, on rajoute 1 au nombre d'hp gagné(s) pendant ce heal et on attend avant le prochain hp gagné
        _phoenixLife += 1;
        _healHP += 1;

        yield return new WaitForSeconds(_healSpeed);

        // Si il est en train de régénerer mais qu'il n'a pas récupéré tous les hp maximum par heal alors on relance la coroutine
        if(isHealing && _healHP < _maxHealHP)
        {
            StartCoroutine(Heal());
        }
        // Sinon on arrête le heal
        else
        {
            isHealing = false;
            healPointChosen = null;
            _doOnce = false;

            //Recherche le joueur de nouveau
            bp.ChangeTarget(player.transform);
        }
    }*/

    // Début de la fonction heal
    public void BossSearchHeal()
    {
        // Si le boss a moins d'un certain nombre de pv on lance la recherche d'une zone de heal
        if(_phoenixLife <= _maxLifeToHeal)
        {
            bp.ChangeState(BossParameters.States.DeplacementOnTheWeb);
            
            // distance max
            float _maxDist = 0;

            // Pour chaque healPoint on regarde lequel est le plus loin
            foreach (GameObject healBossPoint in healBossPoints)
            {
                float _dist = Vector3.Distance(healBossPoint.transform.position, transform.position);

                if(_dist > _maxDist)
                {
                    healPointChosen = healBossPoint.transform;
                    _maxDist = _dist;
                }
            }
            healPointChosen.gameObject.SetActive(true);
            bp.ChangeTarget(healPointChosen);           
        }        
    }

    // Dégât par flèche
    public void BossDamage(int _damage)
    {
        _phoenixLife -= _damage;

        int _chooseSound = Random.Range(0, hitSound.Length);
        audSrcPhoenixLife.PlayOneShot(hitSound[_chooseSound]);

        // Soit il est en train de se heal, alors son heal est arrêté
        if (isHealing)
        {
            isHealing = false;

        }
    }

    public void BossEnergyHeal(int _heal)
    {
        _phoenixLife += _heal;
    }

    // Fonction de mort du boss
    public void BossDeath()
    {
        Portal.SetActive(true);
        StartCoroutine(Win());
    }

    public float speedFade;
    public Image winImage;
    public GameObject particleWin;

    IEnumerator Win()
    {
        SoundManager sm = FindObjectOfType<SoundManager>();
        //Son de mort du boss

        GameObject particleWin_clone = Instantiate(particleWin);
        particleWin_clone.transform.position = this.transform.position;

        GetComponent<BossBehaviour>().mesh.SetActive(false);
        GetComponent<BossBehaviour>().enabled = false;

        yield return new WaitForSeconds(2);

        /*for (float a = 0; a < 1; a += Time.deltaTime * speedFade)
        {
            Color newColor = new Color(winImage.color.r, winImage.color.g, winImage.color.b, a);

            winImage.color = newColor;
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(2f);

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);*/
    }

    public Transform getRandomHealPoint()
    {
        int random = Random.Range(0, healBossPoints.Length);

        return healBossPoints[random].transform;
    }

    //Getters

    public Transform getHealPointChosen()
    {
        return healPointChosen;
    }


}
