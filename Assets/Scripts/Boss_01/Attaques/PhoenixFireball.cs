﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhoenixFireball : MonoBehaviour
{
    [Header("Objets et Transform")]
    //Le prefab de la fireBall
    public GameObject fireballPrefab;
    //le point de spawn de la tête
    public Transform head;

    //Le gameManager
    private GameManager gm;
    //Le joueur
    private GameObject player;

    [Header("Tir")]
    //le premier tir avec plusieurs boules de feu
    [Tooltip("Nombre de Boules de feu")] public int nbrOfBalls = 5;
    //L'espace de temps entre deux boules de feu tiré
    [Tooltip("Temps entre chaque tir")] public float shootRate = 0.5f;
    //la vitesse du projectile
    [Tooltip("Vitesse des boules de feu")] public float ballSpeed = 30f;


    [Header("Random de l'angle de tir")]
    //L'angle de tir
    [Tooltip("Random de l'angle de tir autour du joueur"),Range(0,1)] public float angleRangeShoot = 1;

    public AudioClip fireballSound;
    public AudioClip phoenixSound;

    public AudioSource audSrcFireball;

    private bool isInAttack;

    private void Start()
    {
        //Récupération du GameManager
        gm = FindObjectOfType(typeof(GameManager)) as GameManager;
        //récupération du joueur
        player = gm.player;

        audSrcFireball = gameObject.GetComponent<AudioSource>();
        //audSrcFireball.spatialBlend = 1;
    }

    private void Update()
    {
        if (isInAttack)
            transform.LookAt(gm.player.transform);
    }

    //Fonction pour tirer les projectiles
    public IEnumerator ShootingProjectile(int nbOfBall,float time, float randomRangeAngle)
    {
        isInAttack = true;

        //Division du range de random pour une meilleure maniabilité dans l'inspecteur
        randomRangeAngle /= 10;

        audSrcFireball.PlayOneShot(phoenixSound);
        yield return new WaitForSeconds(1);

        //Pour chaque boule de feu
        for (int i = 0; i < nbOfBall; i++)
        {
            //On recalcul un angle
            Vector3 randomAngle = new Vector3(Random.Range(-randomRangeAngle, randomRangeAngle), Random.Range(-randomRangeAngle, randomRangeAngle), Random.Range(-randomRangeAngle, randomRangeAngle));
            
            //Tir du projectile
            Shoot(randomAngle);

            //Temps entre chaque tir
            yield return new WaitForSeconds(time);
        }

        isInAttack = false;
    }

    void Shoot(Vector3 random)
    {
        //On instancie la boule de feu
        GameObject newFball = Instantiate(fireballPrefab);
        //On lui donne la position de spawn
        newFball.transform.position = head.position;

        audSrcFireball.PlayOneShot(fireballSound);

        //On affecte la vrai vitesse du projectile en fonction de ballSpeed
        float ballSpeedValue = ballSpeed * 1000;

        //On donne à la boule sa vitesse de déplacement
        newFball.GetComponent<PhoenixProjectile>().speed = ballSpeedValue;

        //Direction vers le joueur
        Vector3 calculateForward = player.transform.position - head.transform.position;
        newFball.transform.forward = calculateForward.normalized + random;

        #region CalculComplexe
        /*
        //fwdBoule = (Vjoueur * fwdJoueur + PosJoueur - PosBoule) / vBoule
        //Calcule des valeurs pour correspondre à l'équation ci dessus

        float _vJoueur = 0;
        Vector3 _fwdJoueur = Vector3.zero;

        if (gm.player.GetComponent<PlayerSwitcher>().form == 0)
        {
            _vJoueur = player.GetComponent<PlayerHumanController>().GetMovement(true, true, true).magnitude;
            _fwdJoueur = player.GetComponent<PlayerHumanController>().GetMovement(true, true, true).normalized;
        }
        else
        {
            _vJoueur = player.GetComponent<PlayerBirdController>().GetMovement().magnitude;
            _fwdJoueur = player.GetComponent<PlayerBirdController>().GetMovement().normalized;
        }

        Vector3 _posJoueur = player.transform.position;
        Vector3 _posBoule = head.transform.position;
        float _vBoule = ballSpeedValue;

        //Calcule du vecteur forward de la boule en fonction des informations récolté précédement
        Vector3 calculateForward = (_vJoueur * _fwdJoueur + _posJoueur - _posBoule) / _vBoule;
        
        //Orientation de la boule avec le nouveau vecteur forward
        newFball.transform.forward = calculateForward + random;

        */
        #endregion
    }
}
