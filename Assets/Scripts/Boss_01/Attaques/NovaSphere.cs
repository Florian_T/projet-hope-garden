﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NovaSphere : MonoBehaviour
{
    public AnimationCurve SphereSizeCurve;
    public float _maxSize;
    public float _speedMultiplier;

    private Transform player;

    Vector3 pos;

    public LayerMask IgnoreMe;



    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerHumanController>().transform;

        pos = transform.position;

        ChangeSize();
    }

    public void ChangeSize()
    {
        StartCoroutine(_changeSize());
    }

    IEnumerator _changeSize()
    {
        float curveTime = 0f;
        float curveAmount = SphereSizeCurve.Evaluate(curveTime);

        while(curveAmount < 1.0f)
        {
            curveTime += Time.deltaTime * _speedMultiplier * FindObjectOfType<GameManager>()._timeScale;
            curveAmount = SphereSizeCurve.Evaluate(curveTime);
            transform.localScale = Vector3.one * _maxSize * curveAmount;
            yield return null;
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            /*SoundManager SM = FindObjectOfType<SoundManager>();
            StartCoroutine(SM.FadeOut(SM.audSrcNova, 1));*/

            Vector3 toTarget;
            toTarget = new Vector3(player.position.x - pos.x,
                                    player.position.y - pos.y,
                                    player.position.z - pos.z);
            Ray ray = new Ray(transform.position, toTarget);
            RaycastHit hit;

            GameObject PhNova = FindObjectOfType<PhoenixSupernova>().gameObject;
            PhNova.GetComponent<PhoenixSupernova>().NovaExists = false;

            if (Physics.Raycast(ray, out hit, Mathf.Infinity, ~IgnoreMe))
            {
                if (hit.transform.tag == "Player")
                {
                    PlayerLife PlLife = (PlayerLife)hit.transform.gameObject.GetComponent(typeof(PlayerLife));
                    PlLife.TakeDamage(PlLife.majorDamage);
                    if(hit.transform.gameObject.GetComponent<PlayerSwitcher>().form == 1)
                    {
                        hit.transform.gameObject.GetComponent<PlayerSwitcher>().ChangeForm();
                    }                   
                    Destroy(gameObject);
                }
                else
                {
                    Destroy(gameObject);
                }
            }
        }
    }
}
