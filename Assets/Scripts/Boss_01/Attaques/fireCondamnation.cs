﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fireCondamnation : MonoBehaviour
{
    [HideInInspector] public float explosionForce;
    [HideInInspector] public Vector3 explosionPosition;
    [HideInInspector] public float explosionRadius;
    public float reducer;

    void Start()
    {
    }

    private void Update()
    {

        GetComponent<Rigidbody>().AddExplosionForce(explosionForce, explosionPosition, explosionRadius);

    }

    public float timer;
    private float timed;

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            other.GetComponent<PlayerLife>().isTakingDamageFromFire = true;
        }

        if (other.TryGetComponent(out Killer killer))
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            other.GetComponent<PlayerLife>().isTakingDamageFromFire = false;
        }
    }
}
