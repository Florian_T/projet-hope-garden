﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pauseAnimNova : MonoBehaviour
{
    public void pauseNova()
    {
        GetComponent<Animator>().speed = 0;
    }
}
