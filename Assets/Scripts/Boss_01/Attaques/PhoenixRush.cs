﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhoenixRush : MonoBehaviour
{
    [Header("AttackRush")]
    [Tooltip("Temps entre le début de l'attaque et la fin de l'attaque")]
    public float timeOfAttack = 3;
    [Tooltip("Vitesse prise lorsque le boss attaque")]
    public float speedOfAttack = 120;
    [Tooltip("Rayon de détection des obstacles devant le boss")]
    public float radiusDetection = 2;

    public int nbOfRush;

    [HideInInspector]
    //Si l'attaque est complété ou non
    public bool asCompleteAttack = false;
    //
    public bool isAttacking = false;

    //Le gamemanager
    GameManager gm;
    //le bossbehaviour
    BossBehaviour bb;

    void Start()
    {
        //Récupération du gameManager
        gm = FindObjectOfType(typeof(GameManager)) as GameManager;
        //Récupération du BossBehaviour
        bb = GetComponent<BossBehaviour>();
    }

    //Fonction pour la mise en place de l'attaque rush
    public void AttackRush(int numberOfRushs)
    {
        isAttacking = true;

        //S'il n'a pas complété la dernière attaque de rush alors il ne fait rien
        if (!asCompleteAttack)
            return;
        
        //On sauvegarde la vitesse avant l'attaque
        float stackedSpeed = bb.speed;

        //Pour toutes les itérations
        for (int i = 0; i < numberOfRushs; i++)
        {
            //Il débute l'attaque
            asCompleteAttack = false;
            //on set le boolean
            bool asSeeObstacle = false;

            //On prend le vecteur entre le boss et le joueur
            Vector3 dir = (transform.position - gm.player.transform.position).normalized;
            //La distance entre le joueur et le boss
            float obstacleDistanceDetect = Vector3.Distance(transform.position, gm.player.transform.position);

            //On set les différents points de vérification des vecteurs
            Vector3[] startPositions = new Vector3[4];
            startPositions[0] = new Vector3(transform.position.x + radiusDetection, transform.position.y, transform.position.z);
            startPositions[1] = new Vector3(transform.position.x - radiusDetection, transform.position.y, transform.position.z);
            startPositions[2] = new Vector3(transform.position.x, transform.position.y, transform.position.z + radiusDetection);
            startPositions[3] = new Vector3(transform.position.x, transform.position.y, transform.position.z - radiusDetection);

            //Pour tous les points de vérification
            foreach (Vector3 startPosition in startPositions)
            {
                //On tire un raycast vers le joueur
                RaycastHit hit;

                //S'il voit un obstacle il set asSeeObstacle à true;
                if (Physics.Raycast(startPosition, dir, out hit, obstacleDistanceDetect, ~2))
                {
                    asSeeObstacle = true;
                }
            }

            //Si aucun raycast n'a collider d'object, on lance l'attaque
            if (!asSeeObstacle)
            {
                StartCoroutine(Attack(numberOfRushs,i,stackedSpeed));
            }
        }
    }

    //L'attaque rush
    IEnumerator Attack(int maxNb, int i, float stackedSpeed)
    {
        //On set la vitesse de déplacement du boss à la vitesse de déplacement de l'attaque
        bb.speed = speedOfAttack;
        //On change la terget vers le joueur
        bb.target = gm.player.transform;
        //On permet au boss de se déplacer avec les raycast
        bb.ToggleRaycastMovement();
        //On update le pathFinding
        bb.UpdatePathfinding();

        //On déclenche l'animation de l'attaque
        bb.mesh.GetComponent<Animator>().SetTrigger("AttackRushStart");

        //Nombre de seconde entre le début et la fin de l'attaque
        yield return new WaitForSeconds(timeOfAttack);

        //On déclenche l'animation de fin d'attaque
        bb.mesh.GetComponent<Animator>().SetTrigger("AttackRushEnd");

        //Si c'était la dernière attaque
        if (i >= maxNb - 1)
        {
            //On remet les valeurs de base
            bb.speed = stackedSpeed;

            //On arrète le rush en revenant à la base
            bb.isEndingRush = true;

            //On dit qu'il n'attaque plus
            isAttacking = false;
        }
        
        //On dit qu'il a complété l'attaque
        asCompleteAttack = true;
    }
}
