﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhoenixPunch : MonoBehaviour
{
    [HideInInspector]
    public bool canAttack = true;

    [Tooltip("Distance pour déclancher l'attaque")]
    public float _distanceToAttack;
    [Tooltip("L'objet de la zone de collision")]
    public GameObject boxCollisionDetection;
    
    //Quand l'oiseau entre en collision
    public void OnTriggerEnter(Collider other)
    {
        //Retire de la vie au joueur s'il se trouve en contact avec le boss
        if (other.gameObject.tag == "Player")
        {
            PlayerLife plLife = other.GetComponent<PlayerLife>();
            plLife.TakeDamage(plLife.mediumDamage);
        }
    }

    //Attaque punch
    public void Attack()
    {
        //Il ne peut plus attaquer avant d'avoir finir son attaque
        canAttack = false;
        //On actuve le collider de détection
        boxCollisionDetection.SetActive(true);
        //On retire la détection lorsque l'animation est terminée.
        StartCoroutine(StopDetect(GetComponentInChildren<Animator>().GetCurrentAnimatorStateInfo(0).length));
    }

    //On arrête de détecter après un certain temps
    IEnumerator StopDetect(float time)
    {
        //On attends
        yield return new WaitForSeconds(time);
        
        //On suprime le collider et on ractive le fait de pouvoir attaquer
        boxCollisionDetection.SetActive(false);
        canAttack = true;
    }
}
