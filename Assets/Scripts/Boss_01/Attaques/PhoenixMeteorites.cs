﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhoenixMeteorites : MonoBehaviour
{
    [Header("Objets et Transform")]
    private Transform player;
    public GameObject Meteor;

    [Header("Répartition Météorites")]
    [Tooltip("Zone de spawn autour du joueur")] public int Gap = 30;
    [Tooltip("Nombre de météorites")] public int NbrOfMet = 10;
    [Space(10)]
    [Tooltip("Hauteur Minimum au dessus du joueur")] public int HeightMin = 50;
    [Tooltip("Hauteur Maximum au dessus du joueur")] public int HeightMax = 70;

    [Header("Chutes")]
    [Tooltip("Différence d'angle de chute")] public int AngleMet = 30;
    [Tooltip("Vitesse de chute")] public int FallingSpeed = 30;

    private void Start()
    {
        player = FindObjectOfType<PlayerHumanController>().transform;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            LaunchMeteorites();
        }
    }

    public void LaunchMeteorites()
    {
        float angle1 = Random.Range(-AngleMet, AngleMet);
        float angle2 = Random.Range(-AngleMet, AngleMet);

        for (int i = 0; i < NbrOfMet; i++)
        {
            GameObject newMeteorite = Instantiate(Meteor) as GameObject;
            newMeteorite.transform.position = new Vector3(player.position.x + Random.Range(-Gap, Gap), player.position.y + Random.Range(HeightMin, HeightMax), player.position.z + Random.Range(-Gap, Gap));
            newMeteorite.transform.Rotate(angle1, angle2, 0);
            newMeteorite.GetComponent<PhoenixProjectile>().speed = FallingSpeed;
        }
    }
}
