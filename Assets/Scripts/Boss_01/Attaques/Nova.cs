﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Nova : MonoBehaviour
{
    public GameObject player;

    Vector3 pos;

    public float NovaSpeed = 0.1f;

    private float timeScale = 1;
    private GameManager gm;

    void Start()
    {
        player = FindObjectOfType<PlayerHumanController>().gameObject;
        gm = FindObjectOfType(typeof(GameManager)) as GameManager;

        pos = transform.position;
    }

    void Update()
    {
        timeScale = gm._timeScale;

        transform.localScale += Vector3.one * NovaSpeed * Time.deltaTime * timeScale;

        //Debug.DrawLine(pos, player.transform.position, Color.red);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Vector3 toTarget;
            toTarget = new Vector3(player.transform.position.x - pos.x,
                                    player.transform.position.y - pos.y,
                                    player.transform.position.z - pos.z);
            Ray ray = new Ray(transform.position, toTarget);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if(hit.transform.tag == "Player")
                {
                    PlayerLife PlLife = (PlayerLife)hit.transform.gameObject.GetComponent(typeof(PlayerLife));
                    PlLife.TakeDamage(PlLife.majorDamage);
                    Destroy(gameObject);
                }
                else
                {
                    Destroy(gameObject);
                }
            }
        }
    }
}
