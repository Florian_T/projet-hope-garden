﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossStateMachine : MonoBehaviour
{
    //Imports
    public GameManager gm;                      //Get the game manager to fetch the time scale below
    public GameObject player;                   //Get the player to reference it later
    public float timeScale;                     //Time multiplier (between 0 and 1, primarily used when the player initiates slow-motion while aiming)

    //Basic variables related to decision making
    public string state = "IDLE";               //Current state of the bird. Possible states include :
                                                //IDLE : No movement state when out of combat
                                                //MOVING : Currently  moving towards its destination
                                                //RECOVERY : set time after each attack pattern
                                                //ANY_ATTACK : each attack specific to the bird has states named after its attack patterns

    public bool isInCombat = false;             //Whether the bird is currently fighting the player or not. Will be set once to true, and will not be set back to false until the player or the bird dies
    public float detectionRange = 15.0f;        //Distance where the bird is able to detect the player (if there is a clear line of sight)

    public float health = 100.0f;               //Health points of the bird, when it is reduced to zero the boss dies
    public float ap = 0;                        //Short for Action Points. Current amount of action points the bird has, used to determine what the bird is able to do currently
    public float aps = 5;                       //Short for Action Points per Second. 
    public float currentRecovery;               //Timer for how long to stay in RECOVERY. Is set according to the last attack pattern
    public float currentIdle;                   //Timer for how long to stay in IDLE.

    //Movement related variables
    public float idealDistance = 10.0f;                             //Ideal distance between the player and the bird. Will affect the bird decisionmaking
    public int currentMovementWeight = 1;                           //Weight of movement in the decision. Can fluctuate
    public float movementWeightFluctuation = 0;                     //Value that increases currentMovementWeight by 1 when it reaches 1, then resets itself to 0
    public float movementWeightFluctuationFactor = 0.2f;            //How quickly distance translates into weight gain for MOVING
    public Vector3 destination;                                     //Destination point for movement. 


    public List<AttackPattern> patternList = new List<AttackPattern>();         //Complete list of all the boss patterns
    public List<AttackPattern> decisionList = new List<AttackPattern>();        //List of all possible moves that the bird can do after recovery. Is emptied after each decision

    public void Start()
    {
        //Initialize basic variables
        gm = FindObjectOfType(typeof(GameManager)) as GameManager;
        player = GameObject.FindWithTag("Player");

        currentRecovery = 0;
        currentIdle = 5.0f;

        //Initialize all of the attack patterns
        AttackPattern p = new AttackPattern("BASE_FIREBALL", 50.0f, 15.0f, 3.0f, 5, false);
        patternList.Add(p);
        p = new AttackPattern("BASE_NOVA", 80.0f, 20.0f, 5.0f, 8, false);
        patternList.Add(p);
        p = new AttackPattern("BASE_MELEE", 40.0f, 5.0f, 2.0f, 20, false);
        patternList.Add(p);
    }



    // Update is called once per frame
    public void Update()
    {
        timeScale = gm._timeScale;

        switch (state)
        {
            case "IDLE":

                CheckForCombat();

                if (isInCombat)
                {
                    currentRecovery = 1.0f;
                    state = "RECOVERY";
                }

                //Keep going idle if the combat is not initiated
                if (currentIdle > 0)
                {
                    currentIdle = currentIdle - (Time.deltaTime * timeScale);
                    if(currentIdle < 0)
                    {
                        currentIdle = 0;
                    }
                }
                else
                {
                    //Pick a new direction
                    //TODO

                    //Change state 
                    state = "MOVING";

                }

                



                break;

            case "MOVING":
                //Keep going towards the designated direction until it's reached
                if (Vector3.Distance(destination, transform.position) < 1)
                {
                    //Reached, check if you're in combat
                    if (isInCombat)
                    {

                    }
                    else
                    {
                        //Otherwise, go idle for a few seconds before picking a new point to go to
                        state = "IDLE";
                        currentIdle = Random.Range(2.0f, 5.0f);
                    }
                }


                break;

            case "RECOVERY":

                if (currentRecovery > 0)
                {
                    currentRecovery = currentRecovery - (Time.deltaTime * timeScale);
                    if (currentIdle < 0)
                    {
                        currentIdle = 0;
                    }
                }
                else
                {
                    //Make a decision

                }

                break;

            case "BASE_FIREBALL":

                break;

            case "BASE_NOVA":

                break;

            case "BASE_MELEE":

                break;
        }
    }

    //Functions

        //This function is called every frame when not in combat
        //It searches for the player to see if you must initiate combat.
    public void CheckForCombat()
    {
        //Raycast for the player, check distance then line of sight
        float distance = Vector3.Distance(transform.position, player.transform.position);

        if(distance <= detectionRange)
        {
            if(!Physics.Linecast(transform.position, player.transform.position))
            {
                isInCombat = true;
            }
        }
    }
        
        //This function is called every frame while in combat
        //Modifies the weight of MOVING depending on the distance between the player and the 
    public void AjustMovementWeight()
    {
        //Check how different from the ideal distance the actual distance between the player and the bird is
        float distance = Mathf.Abs(idealDistance - (Vector3.Distance(transform.position, player.transform.position)));

        //Gain value
        movementWeightFluctuation = movementWeightFluctuation + (timeScale * Time.deltaTime * movementWeightFluctuationFactor);

        //Convertion into real weight
        float gain = Mathf.Floor(movementWeightFluctuation);

        currentMovementWeight = currentMovementWeight + (int)gain;
        movementWeightFluctuation = movementWeightFluctuation - gain;


    }

    public class AttackPattern
    {
        //Variables
        public string stateName;                //Name of the bird state associated with the pattern
        public float apCost;                    //Cost of the pattern in action points. Is a float not to mess things up with variable type differences
        public float cooldown;                  //Cooldown of the pattern in seconds. Float allows for decimals if necessary.
        public float remainingCooldown;         //Value set to "cooldown" when the pattern is used, will decrease over time. This value is checked to know if the pattern is available
        public float recovery;                  //Recovery time after the pattern
        public int weight;                      //Amount of times the pattern will be added into the decision list

        public bool isMovementPattern;          //Is true when the pattern makes the bird move (affects movement weight)



        //NOTE : the effect of the pattern will be scripted in the associated state


        //Standard constructor
        public AttackPattern(string sn, float apc, float c, float r, int w, bool imp)
        {
            this.stateName = sn;
            this.apCost = apc;
            this.cooldown = c;
            this.remainingCooldown = 0.0f;
            this.recovery = r;
            this.weight = w;
            this.isMovementPattern = imp;
        }


    }
}
