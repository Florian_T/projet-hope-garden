using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBehaviour : MonoBehaviour
{
    #region VARIABLE DECLARATION

    //MOVEMENT RELATED
    //Movespeed
    [Tooltip("Vitesse de déplacement du boss")]
    public float speed = 100f;
    //Current movement target
    [Tooltip("Cible du boss")]
    public Transform target;
    //Nodes for navigation
    [Tooltip("La liste des nodes")]
    public GameObject node;
    //The bird's mesh
    [Tooltip("Le mesh de l'IA")]
    public GameObject mesh;
    //Ideal distance between the player and the bird. Will affect the bird decisionmaking
    public float idealDistance = 200.0f;
    //Weight of movement in the decision. Can fluctuate
    public int currentMovementWeight = 1;
    //Value that increases currentMovementWeight by 1 when it reaches 1, then resets itself to 0
    public float movementWeightFluctuation = 0;
    //Limit moving to prevent the bird from getting stuck somewhere
    private float currentMoveTimer = 0.0f;
    public float maxMoveTimer = 10.0f;


    //AI
    //Finite state machine
    public string state = "IDLE";

    //Combat status, starts out of combat
    public bool isInCombat = false;
    //Detection range for entering combat
    public float detectionRange = 1000.0f;
    //Current distance between the bird and the player
    public float currentDistance = 0;

    //Action points
    public float ap;   
    //Action points granted per second
    public float aps;             
    //Remaining time in recovery state
    public float currentRecovery;     
    //Remaining time in idle state
    public float currentIdle;
    //Related to the base_heal state
    public bool finishedMovementToHeal = false;
    //Start heal only once, and cancel it
    public bool healStarted = false;
    //Heal float to int converter
    public float wholeHP = 0.0f;
    //HP healed per second in the healing state
    public int healPerSecond = 5;
    //Related to the base_rush state
    public bool isEndingRush;
    public bool asArriveRush;

    //Complete list of all the boss patterns
    public List<AttackPattern> patternList = new List<AttackPattern>();
    //List of all possible moves that the bird can do after recovery. Is emptied after each decision
    public List<AttackPattern> decisionList = new List<AttackPattern>();

    //Attaque Melee
    public float distanceToDoTheAttackMelee;


    //SCRIPTS AND ACCESS
    //Pathfinding related
    Pathfinding.AILerp _AILerp;
    Pathfinding.AIDestinationSetter _AIDestinationSetter;
    RaycastPathFinging _RPF;

    //Get the game manager to fetch the time scale below
    public GameManager gm;
    //Get the player to reference it later
    public GameObject player;                   
    public float timeScale;



    #endregion


    // Start is called before the first frame update
    public void Awake()
    {
        //Initialize basic variables
        gm = FindObjectOfType(typeof(GameManager)) as GameManager;
        player = gm.player;
        node = GameObject.FindWithTag("Nodes");

        _AILerp = GetComponent<Pathfinding.AILerp>();
        _AIDestinationSetter = GetComponent<Pathfinding.AIDestinationSetter>();
        _RPF = GetComponent<RaycastPathFinging>();

        StopMovement();

        currentRecovery = 0;
        currentIdle = 5.0f;

        //Initialize all of the attack patterns
        AttackPattern p = new AttackPattern("MOVING", 0.0f, 0.0f, 1.5f, 0, true);
        patternList.Add(p);
        p = new AttackPattern("BASE_FIREBALL", 30.0f, 15.0f, 7.0f, 6, false);
        patternList.Add(p);
        p = new AttackPattern("BASE_NOVA", 60.0f, 30.0f, 11.0f, 4, false);
        patternList.Add(p);
        p = new AttackPattern("BASE_MELEE", 20.0f, 5.0f, 5.0f, 25, false);
        patternList.Add(p);
        p = new AttackPattern("BASE_HEAL", 40.0f, 180.0f, 2.0f, 100, true);
        patternList.Add(p);
        p = new AttackPattern("BASE_RUSH", 40.0f, 60.0f, 5.0f, 8, true);
        patternList.Add(p);
    }

    // Update is called once per frame
    public void Update()
    {
        timeScale = gm._timeScale;

        //Cheat code to set the bird's health to 10
        if(Input.GetKey(KeyCode.L))
        {
            GetComponent<PhoenixSoin>()._phoenixLife = 10;
        }

        currentDistance = Vector3.Distance(transform.position, player.transform.position);

        AssignSpeed(speed * timeScale);

        switch(state)
        {
            //The boss is not doing anything here, and is stationary.
            case "IDLE":

                CheckForCombat();

                //Starts combat 1.0s later
                if(isInCombat)
                {
                    currentRecovery = 1.0f;
                    state = "RECOVERY";
                }

                //Otherwise keep going idle
                if(currentIdle > 0)
                {
                    currentIdle = currentIdle - (Time.deltaTime * timeScale);
                    if (currentIdle < 0)
                    {
                        currentIdle = 0;
                    }
                }
                else
                {
                    //Pick a new direction
                    target = SearchClosestNodePlayer();
                    //Change state 
                    state = "MOVING";
                    ToggleNodeMovement();
                    UpdatePathfinding();


                }


                break;

            //The bird is moving towards his current destination.
            case "MOVING":

                

                //Get into combat if not already the case
                if(!isInCombat)
                {
                    CheckForCombat();
                    if(isInCombat)
                    {
                        //Immediately go into recovery to prepare for an attack pattern
                        state = "RECOVERY";
                        currentRecovery = 1.5f;
                        StopMovement();
                    }
                }

                //Keep going towards the designated direction until it's reached
                if((Vector3.Distance(target.position, transform.position) < 10.0f) || (currentMoveTimer >= maxMoveTimer))
                {
                    //Reached, check if you're in combat
                    if (isInCombat)
                    {
                        state = "RECOVERY";
                        currentRecovery = 1.5f;
                        currentMoveTimer = 0;
                        StopMovement();
                        speed = 100.0f * timeScale;
                    }
                    else
                    {
                        //Otherwise, go idle for a few seconds before picking a new point to go to
                        state = "IDLE";
                        currentIdle = Random.Range(2.0f, 5.0f);
                        currentMoveTimer = 0;
                        StopMovement();
                        speed = 100.0f * timeScale;
                    }
                }

                //Continuously gain AP
                if(isInCombat)
                {
                    GainAP();
                }

                //Increase the moving timer
                currentMoveTimer += Time.deltaTime * timeScale;

                break;
            
            //The bird is not doing anything, similar to IDLE but when in combat.
            case "RECOVERY":


                AjustMovementWeight();
                GainAP();

                if (currentRecovery > 0)
                {
                    currentRecovery = currentRecovery - (Time.deltaTime * timeScale);
                    if (currentRecovery < 0)
                    {
                        currentRecovery = 0;
                    }
                }
                else
                {
                    //Make a decision

                    //Put every possible move in the decision list
                    //Movement
                    for(int i = 0; i < currentMovementWeight; i++)
                    {
                        decisionList.Add(patternList[0]);
                    }

                    //Hardcode every attackPattern
                    //BASE_FIREBALL
                    if((true) && (ap >= patternList[1].apCost) && (patternList[1].remainingCooldown == 0))
                    {
                        for(int j = 0; j < patternList[1].weight; j++)
                        {
                            decisionList.Add(patternList[1]);
                        }      
                    }

                    //BASE_NOVA
                    if((true) && (ap >= patternList[2].apCost) && (patternList[2].remainingCooldown == 0))
                    {
                        for (int j = 0; j < patternList[2].weight; j++)
                        {
                            decisionList.Add(patternList[2]);
                        }
                    }

                    //BASE_MELEE
                    if((currentDistance < distanceToDoTheAttackMelee) && (ap >= patternList[3].apCost) && (patternList[3].remainingCooldown == 0))
                    {
                        for (int j = 0; j < patternList[3].weight; j++)
                        {
                            decisionList.Add(patternList[3]);
                        }
                    }


                    //BASE_HEAL
                    if((GetComponent<PhoenixSoin>()._phoenixLife <= GetComponent<PhoenixSoin>().maxLife/2) && (ap >= patternList[4].apCost) && (patternList[4].remainingCooldown == 0))
                    {
                        for(int j = 0; j < patternList[4].weight; j++)
                        {
                            decisionList.Add(patternList[4]);
                        }
                    }

                    //BASE_RUSH     [can't be chosen right now]
                    if((ap >= patternList[5].apCost) && (patternList[4].remainingCooldown == 0) && (false))
                    {
                        for (int j = 0; j < patternList[5].weight; j++)
                        {
                            decisionList.Add(patternList[5]);
                        }
                    }

                    //Once everything available is in the list, pick a pattern at random

                    int chosen = Random.Range(0, decisionList.Count);
                    

                    if (decisionList[chosen].stateName == "MOVING")
                    {
                        //Reset movement weight
                        currentMovementWeight = 1;

                        state = "MOVING";
                        target = SearchClosestNodePlayer();
                        UpdatePathfinding();

                        ToggleNodeMovement();
                    }
                    else if (decisionList[chosen].stateName == "BASE_FIREBALL")
                    {
                        //Cooldown and ap cost
                        ap = ap - patternList[1].apCost;
                        patternList[1].remainingCooldown = patternList[1].cooldown;

                        state = "BASE_FIREBALL";
                    }
                    else if (decisionList[chosen].stateName == "BASE_NOVA")
                    {
                        //Cooldown and ap cost
                        ap = ap - patternList[2].apCost;
                        patternList[2].remainingCooldown = patternList[2].cooldown;

                        state = "BASE_NOVA";
                    }
                    else if (decisionList[chosen].stateName == "BASE_MELEE")
                    {
                        //Cooldown and ap cost
                        ap = ap - patternList[3].apCost;
                        patternList[3].remainingCooldown = patternList[3].cooldown;

                        state = "BASE_MELEE";
                    }
                    else if (decisionList[chosen].stateName == "BASE_HEAL")
                    {
                        //Cooldown and ap cost
                        ap = ap - patternList[4].apCost;
                        patternList[4].remainingCooldown = patternList[4].cooldown;

                        finishedMovementToHeal = false;
                        //Pick a heal spot at random 
                        target = SearchClosestNodeToPosition(GetComponent<PhoenixSoin>().getRandomHealPoint());
                        ToggleNodeMovement();

                        

                        state = "BASE_HEAL";

                    }
                    else if (decisionList[chosen].stateName == "BASE_RUSH")
                    {
                        //Cooldown and ap cost
                        ap = ap - patternList[5].apCost;
                        patternList[5].remainingCooldown = patternList[5].cooldown;


                    }



                    //Empty the list afterwards
                    decisionList = new List<AttackPattern>();

                }

                break;
            
            //The bird is doing an attack pattern.
            case "BASE_FIREBALL":
                mesh.GetComponent<Animator>().SetTrigger("AttackFireBall");
                PhoenixFireball pf = GetComponent<PhoenixFireball>();
                StartCoroutine(pf.ShootingProjectile(pf.nbrOfBalls, pf.shootRate, pf.angleRangeShoot));

                state = "RECOVERY";
                currentRecovery = patternList[1].recovery;


                break;

            //The bird is doing an attack pattern.
            case "BASE_NOVA":
                mesh.GetComponent<Animator>().SetTrigger("AttackNova");
                GetComponent<PhoenixSupernova>().CreateSupernova();

                state = "RECOVERY";
                currentRecovery = patternList[1].recovery;

                break;

            //The bird is doing an attack pattern.
            case "BASE_MELEE":

                mesh.GetComponent<Animator>().SetTrigger("AttackClaw");
                GetComponent<PhoenixPunch>().Attack();

                state = "RECOVERY";
                currentRecovery = patternList[1].recovery;

                break;

            case "BASE_HEAL":


                //The pattern is segmented into two parts : going to the heal point and the actual healing

                //MOVING TO THE HEAL POINT
                UpdatePathfinding();

                //GETTING TO THE HEAL SPOT
                if (((currentMoveTimer >= maxMoveTimer) || (Vector3.Distance(transform.position, target.position) < 20.0f)) && (!finishedMovementToHeal))
                {
                    //The bird has arrived to his destination
                    
                    StopMovement();
                    GetComponent<PhoenixSoin>().isHealing = true;
                    finishedMovementToHeal = true;
                    healStarted = true;

                }
                //HEALING
                else if ((finishedMovementToHeal) && (healStarted) && (GetComponent<PhoenixSoin>().isHealing))
                {
                    //The bird is currently healing

                    //Do the calculations for healing each second
                    if(GetComponent<PhoenixSoin>()._maxHealHP >= GetComponent<PhoenixSoin>()._phoenixLife)
                    {
                        wholeHP += Time.deltaTime * timeScale;
                        if(wholeHP * healPerSecond >= 1)
                        {
                            GetComponent<PhoenixSoin>()._phoenixLife += 1;
                            wholeHP -= 1 / healPerSecond;
                        }
                    }
                    else
                    {
                        //The bird has healed enough to go out of the healing phase
                        GetComponent<PhoenixSoin>().isHealing = false;
                    }
                }

                //LEAVING THE STATE WHEN IT'S DONE/CANCELED BY DAMAGE
                else if((finishedMovementToHeal) && (healStarted) && (!GetComponent<PhoenixSoin>().isHealing))
                {
                    state = "RECOVERY";
                    currentRecovery = patternList[4].recovery;
                    finishedMovementToHeal = false;
                    healStarted = false;
                }

                //Increase the moving timer
                currentMoveTimer += Time.deltaTime * timeScale;

                break;

            case "BASE_RUSH":

                PhoenixRush rush = GetComponent<PhoenixRush>();

                if (!rush.isAttacking && !isEndingRush)
                {
                    rush.AttackRush(rush.nbOfRush);
                }
                else if (isEndingRush)
                {
                    SearchClosestNodeBird();
                    UpdatePathfinding();

                    if (asArriveRush)
                    {
                        asArriveRush = false;
                        isEndingRush = false;

                        ToggleNodeMovement();

                        //When the rush is over :
                        state = "RECOVERY";
                        currentRecovery = patternList[5].recovery;
                    }
                }
                
                break;

        }

        UpdateCooldowns();
    }


    #region FUNCTIONS

    //This function is called every frame when not in combat
    //It searches for the player to see if you must initiate combat.
    public void CheckForCombat()
    {
        if (currentDistance <= detectionRange)
        {
            
            isInCombat = true;
            
        }
    }

    //This function is called every frame while in combat
    //Modifies the weight of MOVING depending on the distance between the player and the 
    public void AjustMovementWeight()
    {
        /* Original code, deprecated
        //Check how different from the ideal distance the actual distance between the player and the bird is
        float distance = Mathf.Abs(idealDistance - currentDistance);
        */

        
        movementWeightFluctuation = movementWeightFluctuation + (timeScale * Time.deltaTime * 0.5f);

        //Convertion into real weight
        float gain = Mathf.Floor(movementWeightFluctuation);

        currentMovementWeight = currentMovementWeight + (int)gain;
        movementWeightFluctuation = movementWeightFluctuation - gain;
        

    }

    public Transform  SearchClosestNodeToPosition(Transform point)
    {
        float minDistance = 9999999.0f;
        Transform closestNode = gm.player.transform;

        //On cherche pour chaque node si elle est plus proche que la précédente
        Transform[] nodes = node.GetComponentsInChildren<Transform>();

        foreach (Transform n in nodes)
        {
            float actualDist = Vector3.Distance(point.position, n.position);

            if (actualDist < minDistance)
            {
                minDistance = actualDist;
                closestNode = n;
            }
        }


        return closestNode;
    }

    //Sets movement to node 
    public void ToggleNodeMovement()
    {
        _AILerp.enabled = true;
        _AIDestinationSetter.enabled = true;
        _RPF.enabled = false;
    }

    //Sets movement to raycast
    public void ToggleRaycastMovement()
    {
        _AILerp.enabled = false;
        _AIDestinationSetter.enabled = false;
        _RPF.enabled = true;
    }

    //Stops movement (outside of MOVING state)
    public void StopMovement()
    {
        _AILerp.enabled = false;
        _AIDestinationSetter.enabled = false;
        _RPF.enabled = false;
    }

    //Update values of the pathfinding script
    public void UpdatePathfinding()
    {
        //Update speed
        _AILerp.speed = speed;
        _RPF.speed = speed;

        //Update target
        _AIDestinationSetter.target = target;
        _RPF.target = target;
    }

    private void AssignSpeed(float newSpeed)
    {
        _AILerp.speed = newSpeed;
        _RPF.speed = newSpeed;

        GetComponentInChildren<Animator>().SetFloat("Speed", timeScale);
    }

    //Gain action points and update action point growth
    public void GainAP()
    {
        aps = 5 * (GetComponent<PhoenixSoin>().maxLife/GetComponent<PhoenixSoin>()._phoenixLife);

        ap = ap + aps * Time.deltaTime * timeScale;
    }

    //Get the closest node to the bird
    public Transform SearchClosestNodeBird()
    {
        float minNodeDist = 9999999f;
        Transform closestNode = gm.player.transform;

        //l'IA cherche la node la plus proche
        Transform[] nodes = node.GetComponentsInChildren<Transform>();

        foreach (Transform n in nodes)
        {
            float actualDist = Vector3.Distance(transform.position, n.position);

            if (actualDist < minNodeDist)
            {
                minNodeDist = actualDist;
                closestNode = n;
            }
        }

        return closestNode;
    }

    //Get the closest node to the player
    public Transform SearchClosestNodePlayer()
    {
        float minNodeDist = 9999999f;
        Transform closestNode = transform;

        //l'IA cherche la node la plus proche
        Transform[] nodes = node.GetComponentsInChildren<Transform>();

        foreach (Transform n in nodes)
        {
            float actualDist = Vector3.Distance(player.transform.position, n.position);

            if (actualDist < minNodeDist)
            {
                minNodeDist = actualDist;
                closestNode = n;
            }
        }

        return closestNode;
    }

    //Update remaining cooldowns of every pattern
    public void UpdateCooldowns()
    {
        for(int i = 0; i < patternList.Count; i++)
        {
            patternList[i].ReduceCooldown(Time.deltaTime * timeScale);
        }
    }

    #endregion

    #region CLASSES

    public class AttackPattern
    {
        //Variables
        public string stateName;                //Name of the bird state associated with the pattern
        public float apCost;                    //Cost of the pattern in action points. Is a float not to mess things up with variable type differences
        public float cooldown;                  //Cooldown of the pattern in seconds. Float allows for decimals if necessary.
        public float remainingCooldown;         //Value set to "cooldown" when the pattern is used, will decrease over time. This value is checked to know if the pattern is available
        public float recovery;                  //Recovery time after the pattern
        public int weight;                      //Amount of times the pattern will be added into the decision list

        public bool isMovementPattern;          //Is true when the pattern makes the bird move (affects movement weight)



        //NOTE : the effect of the pattern will be scripted in the associated state


        //Standard constructor
        public AttackPattern(string sn, float apc, float c, float r, int w, bool imp)
        {
            this.stateName = sn;
            this.apCost = apc;
            this.cooldown = c;
            this.remainingCooldown = 0.0f;
            this.recovery = r;
            this.weight = w;
            this.isMovementPattern = imp;
        }

        //Functions
        public void ReduceCooldown(float time)
        {
            if(this.remainingCooldown > 0)
            {
                this.remainingCooldown = this.remainingCooldown - time;
                if (this.remainingCooldown < 0)
                {
                    this.remainingCooldown = 0;
                }
            }
            
            
        }
    }

    #endregion

}
