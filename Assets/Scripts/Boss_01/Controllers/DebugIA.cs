﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugIA : MonoBehaviour
{
    public Text textStateIA;

    public Text textInfoBoss;

    public Text textCollidersInformation;
    
    void Update()
    {
        textStateIA.text = "Mode déplacement actuel de l'IA: \n" + GetComponent<BossParameters>().state;

        textInfoBoss.text = "Vie du boss : " + GetComponent<PhoenixSoin>()._phoenixLife.ToString();

        if (textCollidersInformation != null)
            textCollidersInformation.text = "Point faible actuel:\n" + GetComponent<BossParameters>().actualColliders.name;

    }
}
