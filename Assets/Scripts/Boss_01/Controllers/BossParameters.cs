﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossParameters : MonoBehaviour
{
    //Variables
    //Vitesse de déplacement
    [Tooltip("Vitesse de déplacement du boss")]
    public float speed = 60f;
    public float rotationSpeed = 10f;
    //Cible de l'IA
    [Tooltip("Cible du boss")]
    public Transform target;

    [Tooltip("Changement automatique du mode pour attaquer le joueur s'il est trop proche")]
    public bool automaticChangeDeplacementMode;
    [Tooltip("La distance à laquelle le boss change de mode de déplacement")]
    public float distanceToSwitch;
    [Tooltip("La liste des nodes")]
    public GameObject node;
    [Tooltip("Le mesh de l'IA")]
    public GameObject mesh;

    [System.Serializable]
    public enum States
    {
        None,
        Idle,
        DeplacementOnTheWeb,
        DeplacementWithRaycast
    }

    //Le statut actuel de l'IA
    public States state = States.None;

    //Variable pour vérifier que l'IA bouge
    private Vector3 positionMovement;
    private bool isMoving;

    //Scripts
    Pathfinding.AILerp _AILerp;
    Pathfinding.AIDestinationSetter _AIDestinationSetter;
    RaycastPathFinging _RPF;

    GameManager gm;

    [Header("Colliders")]

    [Tooltip("Le statut de la gestion de vie du boss: 0 = Gestion de la prise de dégât par défaut, 1 = Gestion de la prise de dégât seulement sur les colliders spécifié")]
    public int lifeState = 0;

    [Tooltip("Les paramètres des colliders qui prennent plus ou moins de dégâts lorsque le boss est dans son système de dégat par défaut: lifeState = 0")]
    public CollidersParameters collidersParametersDefaultState;

    [Tooltip("Les paramètres des colliders lorsque seulement quelques un peuvent infliger des dégâts: lifeState = 1")]
    //public CollidersParameters[] collidersParametersStateResist;

    [HideInInspector]
    public CollidersParameters actualColliders;

    //Les paramètres des colliders
    [System.Serializable]
    public class CollidersParameters
    {
        [Tooltip("Nom de la configuration")]
        public string name;
        public int damageOnWeakPoints = 1;
        public int damageOnOtherPoints = 0;

        [Tooltip("Liste des colliders infligeant plus de dégâts")]
        public List<string> colliders;
    }

    [Tooltip("Liste des noms de colliders disponnible"),SerializeField]
    private List<string> collidersList;
    
    void Awake()
    {
        //Recherche des différents scripts
        _AILerp = GetComponent<Pathfinding.AILerp>();
        _AIDestinationSetter = GetComponent<Pathfinding.AIDestinationSetter>();
        _RPF = GetComponent<RaycastPathFinging>();
        gm = FindObjectOfType<GameManager>() as GameManager;

        //Assignation de la vitesse dans les différents scripts
        ChangeSpeed(speed);

        //Assignation de la target dans les différents scripts
        ChangeTarget(target);

        //Assignation du state
        ChangeState(state);
        
        //Manipule les liste de colliders
        ManipulateColliders();
    }

    private void Update()
    {
        //Update du speed en fonction du timeScale
        ChangeSpeed(speed * gm._timeScale);
        ChangeRotationSpeed(rotationSpeed * gm._timeScale);

        if (state == States.Idle)
        {
            transform.rotation = Quaternion.Euler(0, transform.eulerAngles.y, transform.eulerAngles.z);
        }

        #region Contrôles manuel
        //Avec l'appuie sur la touche C on peut passer d'un mode de déplacement à un autre
        if (Input.GetKeyDown(KeyCode.C))
        {
            SwitchStates();
        }

        //Déclenchement de l'attaque de la NOVA
        if (Input.GetKeyDown(KeyCode.I))
        {
            AttackNova();
        }

        //Déclenchement de l'attaque de FireBall
        if (Input.GetKeyDown(KeyCode.O))
        {
            AttackFireBall();
        }

        //Déclenchement de l'attaque de CAC
        if (Input.GetKeyDown(KeyCode.P))
        {
            AttackClaw();
        }

        //Change la répartition des dégâts à défaut
        if (Input.GetKeyDown(KeyCode.N))
        {
            ChangeToDefaultColliderPreset();
        }

        //Change la répartition des dégâts en résistance
        /*if (Input.GetKeyDown(KeyCode.B))
        {
            ChangeToRandomColliderPreset();
        }*/

        //Remet la target au joueur
        if (Input.GetKeyDown(KeyCode.J))
        {
            ChangeTarget(gm.player.transform);
        }

        /*if (Input.GetKeyDown(KeyCode.W))
        {
            AttackRush();
        }*/

        #endregion

        #region Changement automatique du mode de déplacement

        //Si le changement de mode de déplacement est automatique
        if (automaticChangeDeplacementMode)
        {
            //Quand le joueur est suffisament proche, qu'il n'a pas attaquer, et que le cooldown est terminé
            if (Vector3.Distance(transform.position, gm.player.transform.position) <= distanceToSwitch && !asAttack && attackCooldown <= 0)
            {
                //Il peut attaquer le joueur
                canAttackPlayer = true;
            }

            //S'il peut attaquer le joueur
            if (canAttackPlayer)
            {
                //Il rentre dans la boucle d'attaque
                AttackPlayer();
            }

            //Si le cooldown est activé
            if (activeCooldown)
            {
                //On retire du temps au cooldown et on le désactive quand il arrive à 0
                if (attackCooldown > 0)
                {
                    attackCooldown -= Time.deltaTime;
                }
                else
                {
                    attackCooldown = 0;
                    activeCooldown = false;
                }
            }
        }
        #endregion

        #region Attaque automatique au corps à corps
        //Attaque automatique de corps à corps
        //Récupération du script
        PhoenixPunch pPunch = GetComponent<PhoenixPunch>();

        //Calcule de la distance entre le boss et le joueur
        float _distFromPlayer = Vector3.Distance(gm.player.transform.position, transform.position);

        //Si la distance est suffisante pour attaquer et qu'il le peut alors il attaque
        if (_distFromPlayer < pPunch._distanceToAttack && pPunch.canAttack)
        {
            AttackClaw();
        }
        //
        #endregion

        #region Changement automatique de la target
        if (Vector3.Distance(transform.position,target.position) <= 20 && target != gm.player.transform)
        {
            ChangeTarget(gm.player.transform);
        }
        #endregion

        Animate();
    }

    #region Attaques automatiques
    [Header("Gestion des attaques automatiques")]
    [Tooltip("Le cooldown entre deux attaques consécutives")]
    public float cooldownBeforeTwoConsecutiveAttack = 10;

    //Si les informations ont été prise au début du moment où il peut attaquer
    bool asGetInformationForAttackingThePlayer;
    //La position de l'IA avant d'attaquer
    Vector3 lastPosition;
    //La distance entre le joueur et l'IA avant d'attaquer
    float stackedDistance;
    //La target actuelle (qui est une node)
    Transform actualTarget;
    //Si l'IA à terminé d'attaquer
    bool asAttack;
    //Si l'IA peut attaquer le joueur
    bool canAttackPlayer;
    //Le cooldown entre deux attaques consécutives calculé
    float attackCooldown = 0;
    //Si le cooldown est activé
    bool activeCooldown;

    //Boucle d'attaque de l'IA
    void AttackPlayer()
    {
        //S'il n'a pas encore attaquer
        if (!asAttack)
        {
            //Le start, on récupère les informations nécessaires pour la suite
            if (!asGetInformationForAttackingThePlayer)
            {
                asGetInformationForAttackingThePlayer = true;
                activeCooldown = true;
                attackCooldown = cooldownBeforeTwoConsecutiveAttack;
                lastPosition = transform.position;
                stackedDistance = Vector3.Distance(lastPosition, gm.player.transform.position)*2;
                actualTarget = gm.player.transform;

                //On change le statut de l'IA en recherche avec détection des obstacles avec Raycast
                ChangeState(States.DeplacementWithRaycast);
            }

            //Si l'IA a parcourue la distance stacké
            if (Vector3.Distance(transform.position, lastPosition) >= stackedDistance)
            {
                //l'IA retourne à la node la plus proche
                ChangeTarget(SearchClosestNode());

                //On considère qu'il a attaqué
                asAttack = true;
            }
        }

        //S'il a attaqué et qu'il a atteint la node la plus proche
        if (Vector3.Distance(transform.position,actualTarget.position) <= 10 && asAttack)
        {
            //On remet à zero les variables
            asAttack = false;
            canAttackPlayer = false;
            asGetInformationForAttackingThePlayer = false;
            
            //On change la cible au joueur
            ChangeTarget(gm.player.transform);
            //On fait que l'IA se déplace sur la toile
            ChangeState(States.DeplacementOnTheWeb);
        }
    }

    #endregion

    #region Pathfinding
    //Le changement entre les déplacement sur la toile ou avec les raycast
    void SwitchStates()
    {
        if (state == States.DeplacementOnTheWeb)
            ChangeState(States.Idle);
        else if (state == States.Idle)
            ChangeState(States.DeplacementWithRaycast);
        else if (state == States.DeplacementWithRaycast)
            ChangeState(States.DeplacementOnTheWeb);
    }

    //Change la vitesse
    public void ChangeSpeed(float newSpeed)
    {
        _AILerp.speed = newSpeed;
        _RPF.speed = newSpeed;
    }

    //Change la vitesse de rotation
    public void ChangeRotationSpeed(float newSpeed)
    {
        _AILerp.rotationSpeed = newSpeed;
    }

    //Change la target
    public void ChangeTarget(Transform newTarget)
    {
        target = newTarget;
        _AIDestinationSetter.target = newTarget;
        _RPF.target = newTarget;
    }

    //Changement de state
    public void ChangeState(States newState)
    {
        state = newState;

        if (newState == States.None)
        {
            SetAstarPathFindingEnable(false);
            SetRaycastPathFinding(false);
        }
        else if (newState == States.Idle)
        {
            SetAstarPathFindingEnable(false);
            SetRaycastPathFinding(false);
        }
        else if (newState == States.DeplacementOnTheWeb)
        {
            SetAstarPathFindingEnable(true);
            SetRaycastPathFinding(false);
        }
        else if (newState == States.DeplacementWithRaycast)
        {
            SetAstarPathFindingEnable(false);
            SetRaycastPathFinding(true);
        }
    }

    //active ou non les déplacement en pathfinding sur la toile
    void SetAstarPathFindingEnable(bool enable)
    {
        _AILerp.enabled = enable;
        _AIDestinationSetter.enabled = enable;
    }

    //Active ou non les déplacement en pathfinding avec des raycasts
    void SetRaycastPathFinding(bool enable)
    {
        _RPF.enabled = enable;
    }

    Transform SearchClosestNode()
    {
        float minNodeDist = 9999999f;
        Transform closestNode = gm.player.transform;

        //l'IA cherche la node la plus proche
        Transform[] nodes = node.GetComponentsInChildren<Transform>();

        foreach (Transform n in nodes)
        {
            float actualDist = Vector3.Distance(transform.position, n.position);

            if (actualDist < minNodeDist)
            {
                minNodeDist = actualDist;
                closestNode = n;
            }
        }

        return closestNode;
    }
    #endregion

    #region Animation
    //Anime l'IA
    void Animate()
    {
        Animator MeshAnimator = mesh.GetComponent<Animator>();
        MeshAnimator.speed = gm._timeScale;

        if (state == States.Idle)
        {
            MeshAnimator.SetBool("Idle", true);
        }
        else
        {
            MeshAnimator.SetBool("Idle", false);
        }
    }
    #endregion

    #region Attaques
    void AttackNova()
    {
        ChangeState(States.Idle);
        GetComponent<PhoenixSupernova>().CreateSupernova();

        mesh.GetComponent<Animator>().SetTrigger("AttackNova");
    }

    void AttackFireBall()
    {
        ChangeState(States.Idle);
        PhoenixFireball pf = GetComponent<PhoenixFireball>();
        StartCoroutine(pf.ShootingProjectile(pf.nbrOfBalls,pf.shootRate,pf.angleRangeShoot));

        mesh.GetComponent<Animator>().SetTrigger("AttackFireBall");
    }

    public void AttackClaw()
    {
        ChangeState(States.Idle);

        mesh.GetComponent<Animator>().SetTrigger("AttackClaw");
        GetComponent<PhoenixPunch>().Attack();
    }
    
    #endregion
    
    #region Fonction Colliders

            /*
             * Liste des noms de colliders:
             * 
             * Tous les colliders du phoenix commencent par "Collider_"
             * 
             * [nom du prefab] --> est aussi un collider
             * 
             * Head
             * Neck
             * 
             * RArm_1
             * RArm_2
             * RArm_3
             * RArm_4
             * RArm_5
             * RArm_6
             * 
             * LArm_1
             * LArm_2
             * LArm_3
             * LArm_4
             * LArm_5
             * LArm_6
             * 
             * Spine_1
             * Spine_2
             * Belly_1
             * Belly_2
             * Tail_1
             * Tail_2
             * 
             */
             
    void ManipulateColliders()
    {
        //On récupère tous les colliders du phoenix
        Collider[] childsColliders = GetComponentsInChildren<Collider>();
        //On clear la liste de nom des colliders
        collidersList.Clear();

        //pour tous les colliders
        foreach (Collider col in childsColliders)
        {
            //On ajoute le tag "Phoenix" pour que les flèches puissent les détecter
            col.tag = "Phoenix";
            //On ajoute le nom du collider dans une liste
            collidersList.Add(col.name);
        }

        //On set le preset de colliders actuel à celui de défaut
        actualColliders = collidersParametersDefaultState;
    }

    void ChangeToDefaultColliderPreset()
    {
        actualColliders = collidersParametersDefaultState;
    }

    /*void ChangeToRandomColliderPreset()
    {
        int random = Random.Range(0, collidersParametersStateResist.Length - 1);

        actualColliders = collidersParametersStateResist[random];
    }*/

    #endregion
}
