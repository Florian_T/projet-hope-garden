﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wingsPhoenix : MonoBehaviour
{
    public AudioSource audSrcWingsPhoenix;

    public AudioClip wingsPhoenixSound;

    private void Start()
    {
        audSrcWingsPhoenix = gameObject.GetComponent<AudioSource>();
    }

    public void WingsPhoenix()
    {
        audSrcWingsPhoenix.PlayOneShot(wingsPhoenixSound);
    }
}
