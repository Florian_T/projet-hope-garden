﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PortalEnd : MonoBehaviour
{
    public float speedFade;
    public Image winImage;

    // Start is called before the first frame update
    void Start()
    {
        gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<PlayerSwitcher>().meshBird.SetActive(false);
            other.gameObject.GetComponent<PlayerSwitcher>().meshHuman.SetActive(false);
            StartCoroutine(Win());
            GameManager.FindObjectOfType<GameManager>()._IsGameFinish = true;
        }
    }

    private IEnumerator Win()
    {
        for (float a = 0; a < 1; a += Time.deltaTime * speedFade)
        {
            Color newColor = new Color(winImage.color.r, winImage.color.g, winImage.color.b, a);

            winImage.color = newColor;
            yield return new WaitForEndOfFrame();
        }

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
