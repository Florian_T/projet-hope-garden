﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    public float volume = 0.33f;

    public AudioClip musicMenu;

    public AudioClip[] musicGames;
    public float[] palliers;
    private AudioClip actualMusicGame;

    private AudioSource audioSrc;

    public GameObject boss;
    private PhoenixSoin phoenixLife;
    private BossBehaviour bb;
    private float bossLife;

    [HideInInspector] public bool isInGame;
    private bool[] asPlay;

    [Header("Tempo")]
    public float bpm = 90;
    private float bps;
    private float calculateTempo;
    private bool battement;
    private int nbBattement = 0;

    [Range(1,16)]
    public int updateTousLesXTemps;

    // Start is called before the first frame update
    void Start()
    {
        audioSrc = GetComponent<AudioSource>();
        audioSrc.volume = volume;

        if (boss == null)
            boss = FindObjectOfType(typeof(BossBehaviour)) as GameObject;
        phoenixLife = boss.GetComponent<PhoenixSoin>();
        bb = boss.GetComponent<BossBehaviour>();

        ChangeMusique(musicMenu);

        asPlay = new bool[musicGames.Length];

        //Calcul du battement par seconde:
        bps = bpm / 60;

        for (int i = 0; i < palliers.Length; i++)
        {
            palliers[i] *= 0.01f;
        }
    }

    private void Update()
    {
        #region Update du battement
        //Update du battement
        if (battement)
        {
            battement = false;
        }

        calculateTempo += Time.deltaTime;

        if (calculateTempo >= (1 / bps))
        {
            calculateTempo -= (1 / bps);
            nbBattement++;

            if (nbBattement%updateTousLesXTemps == 0)
                battement = true;
        }
        //
        #endregion

        //Calcul de la vie du boss --> valeur de 0 à 1.
        bossLife = Mathf.InverseLerp( 0, phoenixLife.maxLife, phoenixLife._phoenixLife);

        #region Conditions de changement de musiques
        if (isInGame && !asPlay[0])
        {
            NextSound(0);
        }

        if (isInGame && !asPlay[1] && bb.isInCombat && battement)
        {
            NextSound(1);
        }

        if (isInGame && !asPlay[2] && bossLife <= palliers[0] && battement)
        {
            NextSound(2);
        }

        if (isInGame && !asPlay[3] && bossLife <= palliers[1] && battement)
        {
            NextSound(3);
        }

        if (isInGame && !asPlay[4] && bossLife <= palliers[2] && battement)
        {
            NextSound(4);
        }

        if (isInGame && !asPlay[5] && bossLife <= palliers[3] && battement)
        {
            NextSound(5);
        }

        if (isInGame && !asPlay[6] && bossLife <= palliers[4] && battement)
        {
            NextSound(6);
        }
        #endregion

    }

    void NextSound(int ID)
    {
        //On dit qu'il a déjà joué la musique
        asPlay[ID] = true;
        //On la fait jouer
        ChangeMusique(musicGames[ID]);
    }

    public void ChangeMusique(AudioClip clip)
    {
        audioSrc.clip = clip;
        audioSrc.Play();
    }

}
