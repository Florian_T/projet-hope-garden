﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sine : MonoBehaviour
{
    public float angle;
    public float speed;

    private Vector3 motion;
    private CharacterController cc;

    void Start()
    {
        cc = GetComponent<CharacterController>();
    }
    
    void Update()
    {
        motion.y = Mathf.Sin(Time.time*speed) * angle * Time.deltaTime;

        cc.Move(motion);
    }
}
