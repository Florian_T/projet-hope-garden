﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugPlayer : MonoBehaviour
{
    void Update()
    {
        /*Vector3 fwd = transform.forward * 20;
        Debug.DrawRay(transform.position, fwd, Color.green,0.2f);*/

        Debug.DrawRay(transform.position, GetComponent<PlayerSwitcher>().movementVector * 1000, Color.red, 0.1f);
    }
}
