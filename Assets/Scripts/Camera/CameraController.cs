﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    //assignation du joueur
    private GameObject player;
    
    //La forme actuelle du joueur
    private int form;

    //Delta Time
    private float dt;

    [System.Serializable]
    public class ParametersForHuman
    {
        //Le nom de la forme
        [Tooltip("La forme du player correspondant à ces paramètres")]
        public string formName = "Human";
        //target de la camera
        [Tooltip("L'objet que la camera doit suivre (se trouve dans Player/TargetForCamera[form]/CameraPositionAndRotation[form]")]
        public Transform target;

        //Vitesse de rotation autour du joueur
        [Tooltip("Vitesse de rotation autour du joueur")]
        public Vector2 rotationSpeed = new Vector2(2.8f,2.8f);
        //Le clamp de l'angle de la camera
        [Tooltip("Clamp de l'angle de la camera pour éviter d'aller trop haut ou trop bas")]
        public Vector2 clampAngleOnFloor = new Vector2(-60,80);
        public Vector2 clampAngleInAir = new Vector2(-80, 99);

        [Header("Lerp X")]
        [Header("Lerp de position selon les axes x,y,z")]
        public float positionLerpXMin = 10;
        public float positionLerpXMax = 30;

        [Header("Lerp Y")]
        public float positionLerpYMin = 10;
        public float positionLerpYMax = 30;

        [Header("Lerp Z")]
        public float positionLerpZMin = 10;
        public float positionLerpZMax = 30;

        //Le lerp actuel
        [HideInInspector] public float positionLerpX;
        [HideInInspector] public float positionLerpY;
        [HideInInspector] public float positionLerpZ;

        [Header("Lerp de rotation")]
        [Range(1, 35)] public float rotationLerp = 10;

        [Header("Collision détection et distance par rapport au joueur")]
        [Tooltip("Distance minimale")]
        public float minDistance = 2;
        [Tooltip("Distance maximale (si = 0; la distance prise est la distance actuelle de la target à son point de pivot)")]
        public float maxDistance = 0;
    }

    public ParametersForHuman parametersHuman;
    private float baseFov;

    [System.Serializable]
    public class ParametersForBird
    {
        //Le nom de la forme
        [Tooltip("La forme du player correspondant à ces paramètres")]
        public string formName = "Bird";
        //target de la camera
        [Tooltip("L'objet que la camera doit suivre (se trouve dans Player/TargetForCamera[form]/CameraPositionAndRotation[form]")]
        public Transform target;

        //Vitesse de rotation autour du joueur
        [Tooltip("Vitesse de rotation autour du joueur")]
        public Vector2 rotationSpeed = new Vector2(2.8f, 2.8f);
        //Le clamp de l'angle de la camera
        [Tooltip("Clamp de l'angle de la camera pour éviter d'aller trop haut ou trop bas")]
        public Vector2 clampAngle = new Vector2(-60, 60);

        [Header("Lerp X")]
        [Header("Lerp de position selon les axes x,y,z")]
        [Range(1, 30)] public float positionLerpXMin = 4;
        [Range(1, 30)] public float positionLerpXMax = 4;

        [Header("Lerp Y")]
        [Range(1, 30)] public float positionLerpYMin = 5;
        [Range(1, 30)] public float positionLerpYMax = 5;

        [Header("Lerp Z")]
        [Range(1, 30)] public float positionLerpZMin = 4;
        [Range(1, 30)] public float positionLerpZMax = 4;

        //Le lerp actuel
        [HideInInspector] public float positionLerpX;
        [HideInInspector] public float positionLerpY;
        [HideInInspector] public float positionLerpZ;

        [Header("Lerp de rotation")]
        [Range(1, 35)] public float rotationLerp = 2.7f;

        [Header("Collision détection et distance par rapport au joueur")]
        [Tooltip("Distance minimale")]
        public float minDistance = 1;
        [Tooltip("Distance maximale (si = 0; la distance prise est la distance actuelle de la target à son point de pivot)")]
        public float maxDistance = 0;

        [Header("Contrôles pour le point de position"),Tooltip("L'angle maximal que le point de target peut prendre par rapport au joueur")]
        public float clampTurnMax = 35;
        [Tooltip("L'angle minimal que le point de target peut prendre par rapport au joueur")]
        public float clampTurnMin = 5;
        [Tooltip("La vitesse à laquelle le point de target revient derrière le joueur")]
        public float speedReturnBaseTurn = 20;
        [Tooltip("Active la rotation autour du joueur selon la direction vers laquelle il se dirige")]
        public bool activateRotationOffset = false;
        
        //Rotations
        //Inverser les axes de rotation pour l'offset
        [Tooltip("Inverse la direction de la rotation horizontale")]
        public bool inverseHOffset = false;
        [Tooltip("Inverse la direction de la rotation verticale")]
        public bool inverseVOffset = false;
        //Sensibilité de la rotation
        [Tooltip("La sensibilité de la rotation selon les axe horizontal et vertical")]
        public Vector2 sensibilityOfOffsetRotation = new Vector2(3, 3);

        //Translation
        [Tooltip("Active la translation par raport au joueur selon la direction vers laquelle il se dirige")]
        public bool activatePositionOffset = false;
        [Tooltip("La sensibilité de la translation")]
        public float posOffsetSensibility;
        [Tooltip("Clamp de la translation")]
        public Vector2 clampPosOffset = new Vector2(5,2);

        [Header("Clamp de distance minimal et maximal")]
        public Vector2 clampDistance = new Vector2(2, 5);

        [Header("FOV")]
        [Tooltip("FOV minimum et maximum")]
        public Vector2 fovMinMax = new Vector2(60,106);
        [Tooltip("Intensité de l'augmentation du fov au moment d'appuyer sur la touche pour accélérer")]
        public float fovOffset;
        [Tooltip("La vitesse de décroissement du fov")]
        public float speedDecayFovOffset;
        [Tooltip("La vitesse de l'augmentation du fov")]
        public float speedIncreaseFovOffset;
        //S'il a arrété d'accélérer
        [HideInInspector] public bool asStopAccelerate = true;
        //Si calculateFovOffset est presque égal à fovOffset
        [HideInInspector] public bool asReachObjective;
        //le calculateFovOffset
        [HideInInspector] public float calculateFovOffset;
    }

    public ParametersForBird parametersBird;

    void Start()
    {
        player = GetComponentInParent<PlayerSwitcher>().gameObject;
        baseFov = GetComponent<Camera>().fieldOfView;

        //Retire la camera du parent (pour qu'elle ne soit plus en enfant du joueur et permettre de se déplacer librement)
        transform.SetParent(null);

        //Cache le curseur
        Cursor.visible = false;
        //passe le curseur en "Locked" pour éviter de voir la sourie lorsqu'on se déplace
        Cursor.lockState = CursorLockMode.Locked;

        //Assignation des paramètres précédement configuré au script de la target pour que celle ci puisse se mouvoir selon les paramètres donnés.

        //Forme Humaine:
        CameraPositionAndRotationController cprc = parametersHuman.target.gameObject.GetComponent<CameraPositionAndRotationController>();
        cprc.rotationSpeed = parametersHuman.rotationSpeed;

        cprc.clampAngle = parametersHuman.clampAngleOnFloor;

        cprc.minDistance = parametersHuman.minDistance;
        if (parametersHuman.maxDistance == 0)
            parametersHuman.maxDistance = cprc.maxDistance;
        else
            cprc.maxDistance = parametersHuman.maxDistance;

        //Forme Oiseau:
        cprc = parametersBird.target.gameObject.GetComponent<CameraPositionAndRotationController>();
        cprc.rotationSpeed = parametersBird.rotationSpeed;
        cprc.clampAngle = parametersBird.clampAngle;
        cprc.minDistance = parametersBird.minDistance;
        if (parametersBird.maxDistance == 0)
            parametersBird.maxDistance = cprc.maxDistance;
        else
            cprc.maxDistance = parametersBird.maxDistance;

        cprc.clampTurnMax = parametersBird.clampTurnMax;
        cprc.clampTurnMin = parametersBird.clampTurnMin;
        cprc.speedReturnBaseTurn = parametersBird.speedReturnBaseTurn;

        cprc.birdRotationSpeed = player.GetComponent<PlayerBirdController>().mouseSensibility;
        cprc.speedUp = player.GetComponent<PlayerBirdController>().clampSpeed.x;
        cprc.speedDown = player.GetComponent<PlayerBirdController>().clampSpeed.y;

        cprc.inverseHOffset = parametersBird.inverseHOffset;
        cprc.inverseVOffset = parametersBird.inverseVOffset;
        cprc.sensibilityHandV = parametersBird.sensibilityOfOffsetRotation;
        cprc.activateRotationOffset = parametersBird.activateRotationOffset;
        cprc.activatePositionOffset = parametersBird.activatePositionOffset;
        cprc.posOffsetSensibility = parametersBird.posOffsetSensibility;
        cprc.clampPosOffset = parametersBird.clampPosOffset;

        parametersBird.asStopAccelerate = true;
    }

    void Update()
    {
        dt = Time.deltaTime;

        //On récupère la forme actuelle du joueur et la stoque dans la variable form
        if (form != player.GetComponent<PlayerSwitcher>().form)
            form = player.GetComponent<PlayerSwitcher>().form;

        if (form == 0)
        {
            //Camera en forme Humaine
            HumanCameraController();
            AproachCamIfPlayerIsShooting();
            AdaptLerpingOnShooting();

            CameraPositionAndRotationController cprc = parametersHuman.target.gameObject.GetComponent<CameraPositionAndRotationController>();
            PlayerHumanController phc = FindObjectOfType(typeof(PlayerHumanController)) as PlayerHumanController;

            if (phc.GetComponent<CharacterController>().isGrounded)
                cprc.clampAngle = parametersHuman.clampAngleOnFloor;
            else if (!phc.GetComponent<CharacterController>().isGrounded)
                cprc.clampAngle = parametersHuman.clampAngleInAir;
        }
        else if (form == 1)
        {
            //Camera en forme Oiseau
            clampPosition(parametersBird.clampDistance);
            BirdCameraController();
            
        }

    }

    void HumanCameraController()
    {
        //Déplacement de la camera avec un Lerp
        transform.position = new Vector3(
            Mathf.Lerp(transform.position.x, parametersHuman.target.position.x, parametersHuman.positionLerpX * dt),
            Mathf.Lerp(transform.position.y, parametersHuman.target.position.y, parametersHuman.positionLerpY * dt),
            Mathf.Lerp(transform.position.z, parametersHuman.target.position.z, parametersHuman.positionLerpZ * dt)
            );

        //Rotation de la camera avec un Lerp
        transform.rotation = Quaternion.Lerp(transform.rotation, parametersHuman.target.rotation, parametersHuman.rotationLerp * dt);
        GetComponent<Camera>().fieldOfView = baseFov;
    }
    
    void BirdCameraController()
    {
        parametersBird.target.GetComponent<CameraPositionAndRotationController>().actualSpeed = player.GetComponent<PlayerBirdController>().speed;

        PlayerBirdController pbc = player.GetComponent<PlayerBirdController>();

        //Calcule du lerping de la camera en fonction de la vitesse du joueur
        float unLerpSpeed = Mathf.InverseLerp(pbc.clampSpeed.x, pbc.clampSpeed.y + pbc.clampAcceleration.y, pbc.speed);

        parametersBird.positionLerpX = Mathf.Lerp(parametersBird.positionLerpXMin, parametersBird.positionLerpXMax, unLerpSpeed);
        parametersBird.positionLerpY = Mathf.Lerp(parametersBird.positionLerpYMin, parametersBird.positionLerpYMax, unLerpSpeed);
        parametersBird.positionLerpZ = Mathf.Lerp(parametersBird.positionLerpZMin, parametersBird.positionLerpZMax, unLerpSpeed);


        //Déplacement de la camera avec un Lerp
        transform.position = new Vector3(
            Mathf.Lerp(transform.position.x, parametersBird.target.position.x, parametersBird.positionLerpX * dt),
            Mathf.Lerp(transform.position.y, parametersBird.target.position.y, parametersBird.positionLerpY * dt),
            Mathf.Lerp(transform.position.z, parametersBird.target.position.z, parametersBird.positionLerpZ * dt)
            );

        //Rotation de la camera avec un Lerp
        transform.rotation = Quaternion.Lerp(transform.rotation, parametersBird.target.rotation, parametersBird.rotationLerp * dt);
        
        //Tout ce qui concerne le FOV
        //Regarde si l'on a arrêter d'appuyer sur la touche Z
        if (Input.GetButtonUp("Vertical") && Input.GetAxis("Vertical") > 0)
        {
            //Si c'est le cas, on dit qu'il arrête d'accélérer
            parametersBird.asStopAccelerate = true;
        }

        //S'il appuie sur une touche vertical, que l'objectif de fov n'est pas atteint et qu'il a arrêter d'accélérer
        if (Input.GetButton("Vertical") && !parametersBird.asReachObjective && parametersBird.asStopAccelerate)
        {
            //Alors, si on appuie bien sur Z
            if (Input.GetAxis("Vertical") > 0)
            {
                //On augmente peu à peu le fov
                parametersBird.calculateFovOffset = Mathf.Lerp(parametersBird.calculateFovOffset, parametersBird.fovOffset, parametersBird.speedIncreaseFovOffset * dt);

                //Lorsqu'on atteint la valeur voulue, on fait décroitre le fov
                if (parametersBird.calculateFovOffset >= parametersBird.fovOffset - 0.2f)
                {
                    parametersBird.asReachObjective = true;
                    parametersBird.asStopAccelerate = false;
                }
            }
            
        }
        //Si non le fov décroit
        else
        {
            //Le fov décroit
            parametersBird.calculateFovOffset = Mathf.Lerp(parametersBird.calculateFovOffset, 0, parametersBird.speedDecayFovOffset * dt);
            //Tant qu'il n'est pas proche de 0, on ne permet pas à nouveau l'augmentation de fov
            if (parametersBird.calculateFovOffset <= 0.01f)
            {
                parametersBird.asReachObjective = false;
            }
        }

        //modification du fov en fonction de la vitesse et de l'appuie sur la touche d'accélération
        GetComponent<Camera>().fieldOfView = Mathf.Lerp(parametersBird.fovMinMax.x, parametersBird.fovMinMax.y, unLerpSpeed + parametersBird.calculateFovOffset);
    }

    private float initDistance = 0;
    private bool asTakeDistance = false;

    void AproachCamIfPlayerIsShooting()
    {
        //Récupération du script gérant la position et la rotation de la caméra
        CameraPositionAndRotationController cprc = parametersHuman.target.gameObject.GetComponent<CameraPositionAndRotationController>();

        //Si le joueur est en train de tirer
        if (player.GetComponent<ShootBow>().isShooting)
        {
            if (!asTakeDistance)
            {
                //On prend la distance actuelle de la camera
                asTakeDistance = true;
                initDistance = cprc.distance;
            }

            //On fait bouger la distance maximale lorsque le joueur charge l'arc
            cprc.maxDistance = Mathf.Lerp(initDistance, parametersHuman.minDistance, player.GetComponent<ShootBow>().chargeShoot);
        }
        //Si le joueur a finit de tirer.
        else
        {
            initDistance = 0;
            asTakeDistance = false;

            //La distance maximale revient à la normale
            if (cprc.maxDistance != parametersHuman.maxDistance)
            {
                cprc.maxDistance = parametersHuman.maxDistance;
            }
        }
    }

    //Adaptation du lerp de position lorsque la caméra s'approche du joueur. pour éviter que le joueur passe devant le réticule au moment de tirer ou qu'il quitte notre vision
    void AdaptLerpingOnShooting()
    {
        parametersHuman.positionLerpX = Mathf.Lerp(parametersHuman.positionLerpXMin, parametersHuman.positionLerpXMax, player.GetComponent<ShootBow>().chargeShoot);
        parametersHuman.positionLerpY = Mathf.Lerp(parametersHuman.positionLerpYMin, parametersHuman.positionLerpYMax, player.GetComponent<ShootBow>().chargeShoot);
        parametersHuman.positionLerpZ = Mathf.Lerp(parametersHuman.positionLerpZMin, parametersHuman.positionLerpZMax, player.GetComponent<ShootBow>().chargeShoot);
    }

    void clampPosition(Vector2 clampDist)
    {
        Vector3 dir = transform.position - player.transform.position;
        dir = dir.normalized;

        
        float dist = Vector3.Distance(this.transform.position, player.transform.position);
        dist = Mathf.Clamp(dist,clampDist.x,clampDist.y);

        //Mise à jour de la position locale de l'objet
        transform.position = player.transform.position + dir * dist;
    }
}
