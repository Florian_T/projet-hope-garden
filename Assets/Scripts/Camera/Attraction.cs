﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attraction : MonoBehaviour
{
    public Transform follow;
    public float followSpeed;
    public float distance = 2.0f;

    private float distanceCheck;
    
    void Update()
    {
        distanceCheck = (transform.position - follow.position).magnitude;

        if(distanceCheck > distance)
        {
            Vector3 temp = new Vector3(follow.position.x - transform.position.x, follow.position.y - transform.position.y, follow.position.z - transform.position.z);
            temp = temp.normalized;
            temp = temp * followSpeed * Time.deltaTime;

            transform.Translate(temp, Space.World);
        }
    }
}
