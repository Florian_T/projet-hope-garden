﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraStateSwitcher : MonoBehaviour
{
    public float speed = 0.1f;

    private bool asChangeState;

    private CameraController cc;
    private Animator animator;
    private GameManager.State state = GameManager.State.Options;
    private GameManager gm;

    void Start()
    {
        gm = FindObjectOfType(typeof(GameManager)) as GameManager;
        state = gm.state;

        cc = GetComponent<CameraController>();
        animator = GetComponent<Animator>();
    }
    
    void Update()
    {
        if (state != gm.state)
        {
            state = gm.state;
            asChangeState = true;
        }
        else
        {
            asChangeState = false;
        }

        animator.SetFloat("Speed", speed);

        if (asChangeState)
        {
            if (state == GameManager.State.MenuPrincipal)
            {
                GoToMenuPrincipal();
                animator.enabled = true;
            }

            if (state == GameManager.State.Credits)
            {
                GoToCredits();
                animator.enabled = true;
            }

            if (state == GameManager.State.Options)
            {
                GoToOptions();
                animator.enabled = true;
            }

            if (state == GameManager.State.Game)
            {
                GoToGame();
            }
        }
    }

    public void DisableAnimator()
    {
        animator.enabled = false;
    }

    public void GoToMenuPrincipal()
    {
        animator.SetTrigger("ReturnToMenu");
    }

    public void GoToCredits()
    {
        animator.SetTrigger("GoToCredit");
    }

    public void GoToOptions()
    {
        animator.SetTrigger("GoToOptions");
    }

    public void GoToGame()
    {
        animator.SetTrigger("GoToGame");
    }
}
