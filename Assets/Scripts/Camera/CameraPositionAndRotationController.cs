﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPositionAndRotationController : MonoBehaviour
{
    //Vitesse de rotation autour du joueur
    [HideInInspector] public Vector2 rotationSpeed;
    
    //Les inputs de la sourie
    [HideInInspector] public float mouseX;
    [HideInInspector] public float mouseY;

    //Le clamp de l'angle
    [HideInInspector] public Vector2 clampAngle = new Vector2(-60,80);

    //Le point de rotation
    private Transform rotationPoint;

    //Colision détection
    //Distance minimale
    [HideInInspector] public float minDistance;
    //Distance maximale
    [HideInInspector] public float maxDistance;

    //Distance actuelle
    public float distance;
    //Vecteur dirigé vers l'enfant en partant du parent
    private Vector3 directionFromTheParent;
    //

    //Si cet objet est destinée à guider la camera lorsque le joueur est en forme oiseau
    [Header("Bird")]
    public bool isBird;

    //Les valeurs pour l'offset à l'horizontal et à la vertical
    private float h;
    private float v;

    //Clamp la valeur de rotation
    [HideInInspector] public float clampTurnMax;
    [HideInInspector] public float clampTurnMin;
    //Fait revenir l'objet derrière le joueur
    [HideInInspector] public float speedReturnBaseTurn;

    //La valeur de la vitesse de rotation de l'oiseau
    [HideInInspector] public float birdRotationSpeed;

    //Speed
    [HideInInspector] public float speedUp, speedDown, actualSpeed;

    //delta time
    private float dt;

    //Inverser les axes de rotation pour l'offset
    [HideInInspector] public bool activateRotationOffset = false;
    [HideInInspector] public bool activatePositionOffset = false;
    [HideInInspector] public bool inverseHOffset = false;
    [HideInInspector] public bool inverseVOffset = false;
    [HideInInspector] public Vector2 sensibilityHandV = new Vector2(3,3);

    [HideInInspector] public Vector2 clampPosOffset = new Vector2(5, 2);
    [HideInInspector] public float posOffsetH;
    [HideInInspector] public float posOffsetV;
    [HideInInspector] public float posOffsetSensibility;
    private Vector3 rotationPointStackedPosition;

    [HideInInspector] public int form;
    public CameraPositionAndRotationController theOtherRotationPoint;
    public Transform testColliderOrigin;

    public bool asCamRocketLeague;
    private Transform targetRocketLeague;
    private Transform bossTransform;
    private Transform portalTransform;

    void Awake()
    {
        //Assigne le point de rotation comme étant le transform du parent
        rotationPoint = transform.parent;

        //Assignation des variables pour les collisions
        directionFromTheParent = transform.localPosition.normalized;
        distance = Vector3.Distance(transform.position, transform.parent.position);

        maxDistance = Vector3.Distance(transform.position, transform.parent.position);

        bossTransform = FindObjectOfType<BossBehaviour>().transform;
        portalTransform = FindObjectOfType<PortalEnd>().transform;
    }

    private void Start()
    {
        rotationPointStackedPosition = rotationPoint.transform.localPosition;
    }

    void Update()
    {
        //rotationSpeed *= FindObjectOfType<GameManager>()._sensitivity;

        dt = Time.deltaTime;

        GameManager gm = FindObjectOfType(typeof(GameManager)) as GameManager;
        form = gm.player.GetComponent<PlayerSwitcher>().form;
        
        if (FindObjectOfType<PhoenixSoin>()._phoenixLife > 0)
        {
            targetRocketLeague = bossTransform;
        }
        else
        {
            targetRocketLeague = portalTransform;
        }

        //Update les mouvement de l'objet
        UpdateTargetCam();
        //Regarde s'il y a un objet entre le rotationPoint et l'objet
        TestColliding();

        if (Input.GetKeyDown(KeyCode.A) && form == 1 && !gm.gameIsPause || Input.GetButtonDown("CamRL") && form == 1 && !gm.gameIsPause)
        {
            if (asCamRocketLeague)
                ChangeToRocketLeague(false);
            else
                ChangeToRocketLeague(true);
        }

        if (form == 0)
        {
            ChangeToRocketLeague(false);
        }
    }

    void UpdateTargetCam()
    {
        //Regarde le point de pivot en continu
        transform.LookAt(rotationPoint);

        if (asCamRocketLeague)
            rotationPoint.LookAt(targetRocketLeague);

        //Fait tourner le point de pivot en fonction de la position de la sourie ou non
        if (isBird)
        {
            //Valeur de 0 à 1; 0 étant lorsque la vitesse de l'oiseau est au minimum et 1 au maximum
            float unlerpBirdSpeed = Mathf.InverseLerp(speedUp, speedDown, actualSpeed);

            //Le clamp de la valeur pour tourner est affecté par la vitesse du joueur
            float clampTurn = Mathf.Lerp(clampTurnMin, clampTurnMax, unlerpBirdSpeed);

            //h est l'offset au niveau de l'axe horizontal
            h += Input.GetAxis("Mouse X") * (birdRotationSpeed+90) * unlerpBirdSpeed * dt;
            h = Mathf.Clamp(h,-clampTurn,clampTurn);

            //v est l'offset au niveau de l'axe vertical
            v += Input.GetAxis("Mouse Y") * (birdRotationSpeed+90) * unlerpBirdSpeed * dt;
            v = Mathf.Clamp(v, -clampTurn, clampTurn);

            //Remise de h vers sa position derrière l'oiseau lorsque le joueur ne touche plus aux inputs de l'axe horizontal
            if (Input.GetAxis("Mouse X") == 0)
            {
                if ((h < 0.01f && h > -0.01f) || h == 0)
                    h = 0;
                else if (h > 0)
                    h -= speedReturnBaseTurn * dt;
                else if (h < 0)
                    h += speedReturnBaseTurn * dt;
            }

            //Remise de v vers sa position derrière l'oiseau lorsque le joueur ne touche plus aux inputs de l'axe vertical
            if (Input.GetAxis("Mouse Y") == 0)
            {
                if ((v < 0.01f && v > -0.01f) || v == 0)
                    v = 0;
                else if (v > 0)
                    v -= speedReturnBaseTurn * dt;
                else if (v < 0)
                    v += speedReturnBaseTurn * dt;
            }

            //Rotation du point en local en fonction des inputs horizontaux et verticaux
            if (activateRotationOffset && !asCamRocketLeague)
            {
                if (inverseHOffset)
                {
                    if (inverseVOffset)
                    {
                        rotationPoint.localRotation = Quaternion.Euler(-v / sensibilityHandV.y, -h / sensibilityHandV.x, 0);
                    }
                    else
                    {
                        rotationPoint.localRotation = Quaternion.Euler(v / sensibilityHandV.y, -h / sensibilityHandV.x, 0);
                    }
                }
                else
                {
                    if (inverseVOffset)
                    {
                        rotationPoint.localRotation = Quaternion.Euler(-v / sensibilityHandV.y, h / sensibilityHandV.x, 0);
                    }
                    else
                    {
                        rotationPoint.localRotation = Quaternion.Euler(v / sensibilityHandV.y, h / sensibilityHandV.x, 0);
                    }
                }
            }
            
            if (activatePositionOffset)
            {
                //Offset de position lorsqu'on tourne
                posOffsetH += Input.GetAxis("Mouse X") * unlerpBirdSpeed * dt * posOffsetSensibility;
                posOffsetV += Input.GetAxis("Mouse Y") * unlerpBirdSpeed * dt * posOffsetSensibility;

                posOffsetH = Mathf.Clamp(posOffsetH, -clampPosOffset.x, clampPosOffset.x);
                posOffsetV = Mathf.Clamp(posOffsetV, -clampPosOffset.y, clampPosOffset.y);
                
                //Remise à 0 de l'offset de position horizontal
                if ((posOffsetH < 0.1f && posOffsetH > -0.1f) || posOffsetH == 0)
                {
                    posOffsetH = 0;
                }
                else if (posOffsetH > 0)
                {
                    posOffsetH -= posOffsetSensibility / 3 * dt;
                }
                else if (posOffsetH < 0)
                {
                    posOffsetH += posOffsetSensibility / 3 * dt;
                }

                //Remise à 0 de l'offset de position vertical
                if ((posOffsetV < 0.1f && posOffsetV > -0.1f) || posOffsetV == 0)
                {
                    posOffsetV = 0;
                }
                else if (posOffsetV > 0)
                {
                    posOffsetV -= posOffsetSensibility / 3 * unlerpBirdSpeed * dt;
                }
                else if (posOffsetV < 0)
                {
                    posOffsetV += posOffsetSensibility / 3 * unlerpBirdSpeed * dt;
                }

                Vector3 offset = new Vector3(posOffsetH, posOffsetV, 0);

                if (!asCamRocketLeague)
                    rotationPoint.transform.localPosition = rotationPointStackedPosition + offset;
            }
        }
        else
        {
            if (form == 0)
            {
                //Rotation du point de pivot du point de vu du world pour que la rotation du parent n'affecte pas la rotation de la camera.
                if (!asCamRocketLeague)
                    rotationPoint.rotation = Quaternion.Euler(mouseY, mouseX, 0);
                
                //Récupère les mouvements et positions de la sourie
                mouseX += Input.GetAxis("Mouse X") * (rotationSpeed.x * FindObjectOfType<GameManager>()._sensitivity);
                mouseY -= Input.GetAxis("Mouse Y") * (rotationSpeed.y * FindObjectOfType<GameManager>()._sensitivity);
                //Clamp la position Y avec le clampAngle pour éviter de regarder trop haut ou trop bas
                mouseY = Mathf.Clamp(mouseY, -clampAngle.y, -clampAngle.x);
            }
            else
            {
                mouseX = theOtherRotationPoint.rotationPoint.transform.eulerAngles.y;
            }
                
        }
    }

    void TestColliding()
    {
        //Position désiré de l'objet
        Vector3 desiredPos = Vector3.zero;

        if (!isBird)
            desiredPos = transform.parent.TransformPoint(directionFromTheParent * maxDistance);
        else
            desiredPos = testColliderOrigin.TransformPoint(directionFromTheParent * maxDistance);

        if (!isBird)
        {
            CalculateDistance(transform.parent.position);
        }
        else
        {
            CalculateDistance(testColliderOrigin.position);
        }

        //Raycast depuis le parent vers l'objet
        RaycastHit hit;
        //Si le raycast collide un élément
        void CalculateDistance(Vector3 origin)
        {
            if (Physics.Linecast(origin, desiredPos, out hit))
            {
                //La distance s'update et se recalcule pour placer cet objet devant l'objet qui cache la vision
                distance = Mathf.Clamp((hit.distance * 0.87f), minDistance, maxDistance);
            }
            else
            {
                //La distance est la distance maximale
                distance = maxDistance;
            }
        }

        //Mise à jour de la position locale de l'objet
        transform.localPosition = directionFromTheParent * distance;
    }

    void ChangeToRocketLeague(bool truc)
    {
        asCamRocketLeague = truc;
    }
}
