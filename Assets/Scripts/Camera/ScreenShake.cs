﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenShake : MonoBehaviour
{
    public bool activePivotShake;
    public Transform[] cameraPivots;
    private Transform[] cameraPivotsBasePosition;

    [HideInInspector] public bool isShaking;

    private void Start()
    {
        cameraPivotsBasePosition = new Transform[cameraPivots.Length];

        for (int i = 0; i < cameraPivots.Length; i++)
        {
            cameraPivotsBasePosition[i] = cameraPivots[i];
        }
    }

    void Update()
    {
        /*if (Input.GetKeyDown(KeyCode.Keypad1))
        {
            StartCoroutine(Shake(.25f, 0.4f));
        }*/
    }


    public IEnumerator Shake (float duration, float magnitude)
    {
        //Vector3 originalPos = transform.localPosition;
        isShaking = true;

        float elapsed = 0.0f;

        while(elapsed < duration)
        {
            float x = Random.Range(-1f, 1f) * magnitude;
            float y = Random.Range(-1f, 1f) * magnitude;

            transform.localPosition = new Vector3(transform.position.x + x, transform.position.y + y, transform.position.z);

            if (activePivotShake)
            {
                foreach (Transform cp in cameraPivots)
                {
                    cp.localPosition = new Vector3(cp.localPosition.x + x, cp.localPosition.y + y, cp.localPosition.z);
                }
            }

            elapsed += Time.deltaTime;

            yield return null;
        }

        isShaking = false;

        if (activePivotShake)
        {
            for (int i = 0; i < cameraPivots.Length; i++)
            {
                cameraPivots[i].localPosition = cameraPivotsBasePosition[i].localPosition;
            }
        }
        
        //transform.localPosition = originalPos;
    }
}
