﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour
{
    public Vector2[] coord = new Vector2[2];
    public GameObject theObject;
    public float speed;


    private Vector2 targetCoord = Vector2.zero;

    public void _MoveToPosition()
    {
        if (coord.Length != 2)
            Debug.LogError("The object can't move");

        if (coord.Length == 2)
        {

            float dist1 = Vector2.Distance(theObject.GetComponent<RectTransform>().localPosition, coord[0]);
            float dist2 = Vector2.Distance(theObject.GetComponent<RectTransform>().localPosition, coord[1]);

            if (dist1 < dist2)
            {
                targetCoord = coord[1];
            }
            else
            {
                targetCoord = coord[0];
            }
        }
    }

    private void Start()
    {
        targetCoord = theObject.GetComponent<RectTransform>().localPosition;
    }

    private void Update()
    {
        float dt = Time.deltaTime;

        theObject.GetComponent<RectTransform>().localPosition = Vector2.Lerp(theObject.GetComponent<RectTransform>().localPosition,targetCoord,speed*dt);
    }
}
