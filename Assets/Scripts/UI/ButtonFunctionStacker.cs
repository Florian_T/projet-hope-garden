﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonFunctionStacker : MonoBehaviour
{
    private void Update()
    {
        //Si on appuie sur la touche M le jeu quitte.
        if (Input.GetKeyUp(KeyCode.M))
        {
            Application.Quit();
        }
    }

    public void _QuitGame()
    {
        Application.Quit();
    }

    public void _ChangeScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
}
