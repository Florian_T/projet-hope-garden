﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReinitialiseurDePointeur : MonoBehaviour
{
    void Start()
    {
        //Réaffiche le curseur
        Cursor.visible = true;
        //passe le curseur en "None" pour voir les mouvements de la sourie
        Cursor.lockState = CursorLockMode.None;
    }
}
