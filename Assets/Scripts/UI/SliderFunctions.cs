﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderFunctions : MonoBehaviour
{
    VolumeManager volManager;
    public Slider sliderSensitivity;
    public Slider sliderSensitivityBird;


    void Start()
    {
        volManager = FindObjectOfType(typeof(VolumeManager)) as VolumeManager;

        if(sliderSensitivity != null)
        {
            SetSensitivityLevel(sliderSensitivity.value);
        }
        if (sliderSensitivityBird != null)
        {
            SetSensitivityBirdLevel(sliderSensitivityBird.value);
        }
    }
    
    public void SetMasterLevel(float sliderValue)
    {
        volManager.volumeMaster = sliderValue;
    }

    public void SetMusicLevel(float sliderValue)
    {
        volManager.volumeMusic = sliderValue;
    }

    public void SetSFXLevel(float sliderValue)
    {
        volManager.volumeSFX = sliderValue;
    }

    public void SetSensitivityLevel(float sliderValue)
    {
        GameManager.FindObjectOfType<GameManager>()._sensitivity = sliderValue;
    }

    public void SetSensitivityBirdLevel(float sliderValue)
    {
        GameManager.FindObjectOfType<GameManager>()._sensitivityBird = sliderValue;
    }
}
