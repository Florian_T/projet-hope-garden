﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHumanController : MonoBehaviour
{
    [Header("Paramètres de la vitesse, du saut et de la gravité")]
    //Movement inputs
    
    private float speedMovementH = 15;
    private float speedMovementV = 19;
    [Tooltip("La vitesse de déplacement du joueur minimale et maximale selon l'axe horizontal")]
    public Vector2 speedMovementHMinMax = new Vector2(7,15);
    [Tooltip("La vitesse de déplacement du joueur minimale et maximale selon l'axe vertical")]
    public Vector2 speedMovementVMinMax = new Vector2(9,19);

    public float globalSpeed = 1;
    //Jump
    [Tooltip("Force du saut")]
    public float jumpForce = 49.6f;
    
    [Tooltip("A quel point le vecteur de saut décroit")]
    public float decroissanceOfJump= 0.6f;

    //public Vector3 movementVel;
    
    //La force du saut calculé (avec la décroissance)
    private float calculateJumpForce;

    //Est ce que le joueur à sauté
    private bool asjump;

    [Tooltip("Gravité")]
    public float gravity = 9.81f;
    private float g = 0;

    //Transformation
    private float lastOnGround = 0;
    private float lastOnGroundMax = 3.0f;

    //Les inputs horizontaux et verticaux
    
    private float inputHorizontal;
    private float inputVertical;

    [Tooltip("L'accélération du joueur au moment où il commence à appuyer sur la touche de déplacement Horizontale")]
    [Header("Accélération et décéleration")]
    [Range(0.0001f, 35)] public float lerpDeplacementHorizontalStart;
    [Tooltip("La décélération du joueur au moment de relacher la touche de déplacement Horizontale")]
    [Range(0.0001f, 35)] public float lerpDeplacementHorizontalStop;

    [Tooltip("L'accélération du joueur au moment où il commence à appuyer sur la touche de déplacement Verticale")]
    [Range(0.0001f, 35)] public float lerpDeplacementVerticalStart;
    [Tooltip("La décélération du joueur au moment de relacher la touche de déplacement Verticale")]
    [Range(0.0001f, 35)] public float lerpDeplacementVerticalStop;

    //Le lerp pour la rotation du player
    [Range(1, 20)]
    [Header("Lerp de rotation")]
    public float lerpRotation;
    public bool canRotateInAir;

    [Tooltip("Le temps global (pour le ralentissement lors de la visé)")]
    [Header("Echelle du temps global")]
    private float timeScale = 1;
    private GameManager gm;

    //Le component CharacterController
    private CharacterController cc;

    //valeur pour faire tourner le joueur s'il est sur le sol.
    [HideInInspector]
    public float mouseX;
    [Header("Fonctionnement")]
    public CameraPositionAndRotationController objetPourPositionnerLaCamera;

    [Header("CharacterControllerParameters")]
    public Vector3 ccCenter = Vector3.zero;
    public float ccHeight = 1.9f;

    //Variable qui permet de vérifier localement l'état du joueur (humain/oiseau, modifié dans PlayerSwitcher)
    public bool humanMode = true;

    //Vitesse conservée depuis la forme oiseau
    public Vector3 birdVelocity = Vector3.zero;
    //Friction en l'air et sur le sol
    public float stopBirdVelocityAir = 0.01f;
    public float stopBirdVelocityGround = 0.2f;
    //Temps après la transformation pendant laquelle le joueur ne subit pas la friction (réduit en permanence, ajouté dans PlayerSwitcher)
    public float postTransformationFloating = 0f;

    [HideInInspector] public bool isInGame;

    //Animations:
    public Animator animBody = null;

    private bool onGroundTouched;

    void Start()
    {
        cc = GetComponent<CharacterController>();
        gm = FindObjectOfType(typeof(GameManager)) as GameManager;
        g = gravity;

        asjump = true;
    }
    
    void Update()
    {
        if (isInGame)
        {
            if (humanMode)
            {
                timeScale = gm._timeScale;
                float dt = Time.deltaTime;

                //Variable stockant le vecteur mouvement
                Vector3 movement = Vector3.zero;

                if (Input.GetButton("Horizontal"))
                {
                    inputHorizontal = Mathf.Lerp(inputHorizontal, Input.GetAxis("Horizontal"), lerpDeplacementHorizontalStart * dt);
                }
                else
                {
                    inputHorizontal = Mathf.Lerp(inputHorizontal, Input.GetAxis("Horizontal"), lerpDeplacementHorizontalStop * dt);
                }

                if (Input.GetButton("Vertical"))
                {
                    inputVertical = Mathf.Lerp(inputVertical, Input.GetAxis("Vertical"), lerpDeplacementVerticalStart * dt);
                }
                else
                {
                    inputVertical = Mathf.Lerp(inputVertical, Input.GetAxis("Vertical"), lerpDeplacementVerticalStop * dt);
                }


                //Lorsque l'on saute
                if (Input.GetButtonDown("Jump") && cc.isGrounded && !asjump)
                {
                    asjump = true;
                    calculateJumpForce = jumpForce;
                }

                //Le joueur à sauter --> La force est ajouter au mouvement vertical et décroit au fur et à mesure.
                if (asjump)
                {
                    movement.y = calculateJumpForce;

                    if (calculateJumpForce > 0)
                        calculateJumpForce -= decroissanceOfJump * timeScale * Time.deltaTime;

                    if (cc.isGrounded && calculateJumpForce <= 0.75f * jumpForce)
                    {
                        asjump = false;
                    }
                }

                //Adaptation de la vitesse du joueur selon s'il charge l'arc ou non
                ShootBow sb = GetComponent<ShootBow>();
                speedMovementH = Mathf.Lerp(speedMovementHMinMax.y, speedMovementHMinMax.x, sb.chargeShoot);
                speedMovementV = Mathf.Lerp(speedMovementVMinMax.y, speedMovementVMinMax.x, sb.chargeShoot);

                //Calcul du mouvement 
                movement = transform.TransformDirection(inputHorizontal * speedMovementH, movement.y, inputVertical * speedMovementV);

                //Ajout du mouvement d'oiseau restant
                movement += birdVelocity;

                //Application de la gravité
                if (!cc.isGrounded)
                {
                    g += gravity * dt * timeScale;
                    movement += Vector3.up * g * -1;

                    //Lié à la transformation
                    if (lastOnGround > 0)
                    {
                        lastOnGround = lastOnGround - Time.deltaTime;
                        if (lastOnGround <= 0)
                        {
                            lastOnGround = 0;
                        }
                    }

                    //Animation
                    animBody.SetBool("Jump", true);
                }

                if (!gm.gameIsPause)
                {
                    mouseX = objetPourPositionnerLaCamera.mouseX;

                    //Rotation du joueur
                    if (canRotateInAir)
                        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, mouseX, 0), lerpRotation * dt);
                    else if (cc.isGrounded)
                        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, mouseX, 0), lerpRotation * dt);
                }
                
                if (cc.isGrounded)
                {
                    g = 0;

                    //Lié à la transformation
                    lastOnGround = lastOnGroundMax;

                    //Réduction du mouvement oiseau restant (friction du sol)
                    if ((birdVelocity != Vector3.zero) && (postTransformationFloating == 0))
                    {
                        var vx = Mathf.Lerp(birdVelocity.x, 0, stopBirdVelocityGround * timeScale);
                        var vy = Mathf.Lerp(birdVelocity.y, 0, stopBirdVelocityGround * 2 * timeScale); //Doublé sur la verticale pour éviter les rebonds
                        var vz = Mathf.Lerp(birdVelocity.z, 0, stopBirdVelocityGround * timeScale);

                        //Annuler les valeurs lorsqu'elles sont trop basses
                        if (Mathf.Abs(vx) < 0.01f)
                        {
                            vx = 0;
                        }
                        if (Mathf.Abs(vy) < 0.01f)
                        {
                            vy = 0;
                        }
                        if (Mathf.Abs(vz) < 0.01f)
                        {
                            vz = 0;
                        }

                        birdVelocity = new Vector3(vx, vy, vz);
                    }

                    //Animation
                    animBody.SetBool("Jump", false);
                }
                else
                {
                    //Réduction du mouvement oiseau restant (Friction de l'air)
                    if ((birdVelocity != Vector3.zero) && (postTransformationFloating == 0))
                    {
                        var vx = Mathf.Lerp(birdVelocity.x, 0, stopBirdVelocityAir * timeScale);
                        var vy = Mathf.Lerp(birdVelocity.y, 0, stopBirdVelocityAir * timeScale);
                        var vz = Mathf.Lerp(birdVelocity.z, 0, stopBirdVelocityAir * timeScale);

                        //Annuler les valeurs lorsqu'elles sont trop basses
                        if (Mathf.Abs(vx) < 0.001f)
                        {
                            vx = 0;
                        }
                        if (Mathf.Abs(vy) < 0.001f)
                        {
                            vy = 0;
                        }
                        if (Mathf.Abs(vz) < 0.001f)
                        {
                            vz = 0;
                        }

                        birdVelocity = new Vector3(vx, vy, vz);
                    }
                }

                //Baisse standard du maintien de la vitesse d'oiseau
                if (postTransformationFloating > 0)
                {
                    postTransformationFloating = postTransformationFloating - dt * timeScale;
                    if (postTransformationFloating < 0)
                    {
                        postTransformationFloating = 0;
                    }
                }

                //Application du dt et du temps global
                movement.y += -9.81f;

                movement *= dt * globalSpeed;

                //movementVel = movement;

                if (!GetComponent<PlayerLife>().isDead)
                    cc.Move(movement * timeScale);

                stackedMovement = movement;

                //Les animations
                animBody.speed = timeScale;
                if (!gm.gameIsPause)
                {
                    animBody.SetFloat("X", inputHorizontal);
                    animBody.SetFloat("Y", inputVertical);
                }

                if (onGroundTouched != cc.isGrounded)
                {
                    onGroundTouched = cc.isGrounded;

                    if (onGroundTouched)
                    {
                        animBody.SetTrigger("FallToGround");

                        SoundManager sm = FindObjectOfType<SoundManager>();
                        sm.Fall();
                    }
                }
            }
        }
    }

    

    //Accessers
    public float getLastOnGround()
    {
        return lastOnGround;
    }

    //Functions
    public void resetGravity()
    {
        g = 0;
    }

    private Vector3 stackedMovement;

    public Vector3 GetMovement(bool x, bool y, bool z)
    {
        Vector3 theMovement = Vector3.zero;

        if (x)
            theMovement.x = stackedMovement.x;
        if (y)
            theMovement.y = stackedMovement.y;
        if (z)
            theMovement.z = stackedMovement.z;

        return theMovement;
    }


}
