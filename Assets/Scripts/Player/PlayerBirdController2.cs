﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBirdController2 : MonoBehaviour
{
    public float speed = 90;
    public Vector2 clampSpeed;


    public Vector2 clampAngle = new Vector2(-80, 80);
    public float rotationSpeed = 3;

    public float tilt;
    public float lerpTilt;
    private float calculateTilt;

    private CharacterController cc;

    public bool birdMode;

    public float globalSpeedMultiplier = 1;

    public Vector2 clampAcceleration = new Vector2(-35, 60);

    // Start is called before the first frame update
    void Start()
    {
        cc = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (birdMode)
        {
            Vector3 movement = transform.forward * speed * Time.deltaTime;

            AddingInputSpeed();
            speed -= transform.forward.y * Time.deltaTime * 50;
            speed += adder;
            speed = Mathf.Clamp(speed, clampSpeed.x, clampSpeed.y);

            UpdateRotation();

            //move horizontal
            float horizontalTmpMovement = CalculateTranslationHorizontal();
            //passage en space.Self pour assigner la valeur de déplacement horizontale
            movement = transform.InverseTransformDirection(movement);
            movement.x += horizontalTmpMovement;

            //Remise en space.World
            movement = transform.TransformDirection(movement);
            movement *= globalSpeedMultiplier;

            cc.Move(movement);
        }
    }

    void UpdateRotation()
    {
        //Rotations
        //Calculate actual rotation with 0 degrees in vector forward, 90 vector.up and -90 -vector.up
        float rot = Vector3.Angle(-Vector3.up, transform.forward) - 90;

        //Axe vertical
        float mouseY = -Input.GetAxis("Mouse Y");
        mouseY = Mathf.Clamp(mouseY, -1, 1);

        if ((rot > clampAngle.x && mouseY > 0) || (rot < clampAngle.y && mouseY < 0))
            transform.Rotate(new Vector3(mouseY * rotationSpeed, 0, 0), Space.Self);

        //Axe Horizontal
        float mouseX = Input.GetAxis("Mouse X");
        mouseX = Mathf.Clamp(mouseX, -1, 1);

        transform.Rotate(new Vector3(0, mouseX * rotationSpeed, 0), Space.World);

        //Calculate the good value of tilt with horizontal input and lerp
        calculateTilt = Mathf.Lerp(calculateTilt, -mouseX * tilt, lerpTilt * Time.deltaTime);
        //Assign the current tilt
        transform.rotation = Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y, calculateTilt);
    }

    float timerCalculate = 0;
    bool movingH = false;
    float directionH = 0;
    public float timerForRightTranslation;
    public float speedRight;

    float CalculateTranslationHorizontal()
    {
        if (Input.GetButtonDown("Horizontal") && Input.GetAxis("Horizontal") > 0)
        {
            movingH = true;
            directionH = 1;
        }
        else if (Input.GetButtonDown("Horizontal") && Input.GetAxis("Horizontal") < 0)
        {
            movingH = true;
            directionH = -1;
        }

        if (movingH)
        {
            if (timerCalculate < timerForRightTranslation)
            {
                timerCalculate += Time.deltaTime;
            }
            else
            {
                movingH = false;
                timerCalculate = 0;
            }
        }

        float horizontalTmpMovementCalculate = 0;

        if (movingH)
            horizontalTmpMovementCalculate = directionH * speedRight * Time.deltaTime;
        else
            horizontalTmpMovementCalculate = 0;

        return horizontalTmpMovementCalculate;
    }

    [Header("Accélération et décélération")]
    public float speedIncrease = 20;
    public float speedDecrease = 20;
    float adder = 0;

    void AddingInputSpeed()
    {
        if (Input.GetButton("Vertical"))
        {
            adder += Input.GetAxis("Vertical") * Time.deltaTime * speedIncrease;
            adder = Mathf.Clamp(adder, clampAcceleration.x, clampAcceleration.y);
        }
        else
        {
            if (adder > -0.1f && adder < 0.1f)
                adder = 0;
            else if (adder > 0)
                adder -= Time.deltaTime * speedDecrease;
            else if (adder < 0)
                adder += Time.deltaTime * speedDecrease;
        }
    }


    //Les bouts de code fait avant pour les conserver:



    /*void Accelerate()
    {
        //Acceleration
        if (accelerationDelay == 0)
        {
            if (Input.GetKey(KeyCode.Space))
            {
                acceleration = acceleration + accelerationStrength;
                if (acceleration > maxAcceleration)
                {
                    acceleration = maxAcceleration;
                }
                accelerationDelay = accelerationDelayMax;
            }
        }
        else
        {
            accelerationDelay = accelerationDelay - Time.deltaTime;
            if (accelerationDelay < 0)
            {
                accelerationDelay = 0;
            }
        }

        //AccelerationDecay
        if (acceleration > 0)
        {
            acceleration = acceleration - (accelerationDecay * Time.deltaTime);
            if (acceleration < 0)
            {
                acceleration = 0;
            }
        }
    }*/



    /*[Header("AdaptSpeed")]
    public float lerpSpeed;
    public float minLerpSpeed;
    public float maxLerpSpeed;

    private void AdaptSpeedFromAngle(float rotation)
    {
        /*
         * Memo:    baseSpeedN = vitesse normale
         *          baseSpeedU = vitesse quand on monte
         *          baseSpeedD = vitesse quand on descend
         * 
         */

    /*     float lerpRotUp = Mathf.InverseLerp(0, clampAngle.y, rotation);
         float lerpRotDown = Mathf.InverseLerp(0, clampAngle.x, rotation);

         float wantedSpeed = 0;

         if (rotation > 0)
             wantedSpeed = Mathf.Lerp(baseSpeedN, baseSpeedU, lerpRotUp);
         else if (rotation < 0)
             wantedSpeed = Mathf.Lerp(baseSpeedN, baseSpeedD, lerpRotDown);
         else
             wantedSpeed = baseSpeedN;

         float unLerpRot = Mathf.InverseLerp(clampAngle.x, clampAngle.y, rotation);
         lerpSpeed = Mathf.Lerp(minLerpSpeed, maxLerpSpeed, unLerpRot);

         currentBaseSpeed = Mathf.Lerp(currentBaseSpeed, wantedSpeed, lerpSpeed * Time.deltaTime);
     }*/

    //Variables:
    /*[Header("Accélération")]
    public float accelerationDelayMax = 1f;                 //Cooldown between each acceleration
    private float accelerationDelay = 0;                    //Current cooldown
    public float accelerationStrength = 90;                //Added speed each acceleration
    public float accelerationDecay = 90f;                   //Per second
    public float acceleration = 0;                         //Current total bonus speed
    private float maxAcceleration = 300;                    //Threshold 

    [Header("Vitesse de rotation")]
    public float rotationSpeed = 3;                        //Degrees per second of rotation

    [Header("Vitesse")]
    public float baseSpeedU = 20;                           //Normal speed without acceleration going upward
    public float baseSpeedN = 40;                           //Normal speed without acceleration going straight forward
    public float baseSpeedD = 90;                           //Normal speed without acceleration going downward

    public float globalSpeedMultiplier = 1;                 //All the speed values are multiplied by this for the final movespeed

    public float currentBaseSpeed;                          //Given the way the player is looking towards
    public float currentSpeed;                             //CurrentBaseSpeed + Acceleration
    

    private Vector3 tmpMovement;                            //Final movement vector

    public bool birdMode = false;                           //Local variable to check if the player is in bird mode (modified by PlayerSwitcher)
    public Quaternion lastRotation;                         //Is used to keep the last rotation before the transformation, in the case that the player retransforms before touching the ground



    //Angle witch the player can move up and down
    [Header("Angle minimal et maximal")]
    public Vector2 clampAngle;

    //Tilt is for rotate the player around the Z axis when he turn with horizontal inputs.
    [Header("Assignation du tilt")]
    public float tilt;
    //Lerp for the rotation tilt
    public float lerpTilt;
    private float calculateTilt;

    [Header("Vitesse de translation gauche/droite")]
    public float speedRight;
    public float timerRight;

    public float gravity;*/
}
