﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSwitcher : MonoBehaviour
{
    private PlayerHumanController phc;
    private PlayerBirdController pbc;
    private CharacterController cc;
    private Rigidbody rb;
    private ShootBow sb;
    private GameManager gm;

    public GameObject meshHuman;
    public GameObject meshBird;
    public GameObject canvasReticule;
    
    [HideInInspector]
    public int form = 2;

    public GameObject particleTransformationPrefab;

    [HideInInspector] public bool ChangeformStart = false;
    private Renderer[] renderers;

    [HideInInspector]
    public bool isInTransformation;

    [HideInInspector] public bool isInGame;

    void Start()
    {
        phc = GetComponent<PlayerHumanController>();
        pbc = GetComponent<PlayerBirdController>();
        cc = GetComponent<CharacterController>();
        rb = GetComponent<Rigidbody>();
        sb = GetComponent<ShootBow>();
        gm = FindObjectOfType(typeof(GameManager)) as GameManager;

        renderers = GetComponentsInChildren<Renderer>();

        form = 1;
        ChangeForm();
        ChangeformStart = true;

        StartCoroutine(CalculateMovementVector());
    }
    
    void Update()
    {
        if (!gm.gameIsPause && !GetComponent<PlayerLife>().isDead)
        {
            if (isInGame)
            {
                if ((Input.GetButtonDown("Switch")) && GetComponent<PlayerLife>()._life > 0)
                {
                    ChangeForm();
                }

                if (Input.GetAxis("Fire1") > 0 && form == 1)
                {
                    ChangeForm();
                }

                //Remise du réticule en off en oiseau pour éviter de le voir au retour du menu pause
                if (form == 1)
                {
                    canvasReticule.SetActive(false);
                }
            }
        }
    }

    public void ChangeForm()
    {
        if(GameManager.FindObjectOfType<GameManager>()._IsGameFinish == false)
        {
            isInTransformation = true;

            if (form == 0)
                form = 1;
            else
                form = 0;

            GetComponent<ShootBow>().chargeShoot = 0;

            if (form == 0)
            {
                //Changement en humain
                phc.humanMode = true;
                pbc.birdMode = false;

                cc.height = phc.ccHeight;
                cc.center = phc.ccCenter;

                if (ChangeformStart)
                {
                    SoundManager sm = FindObjectOfType<SoundManager>();
                    sm.Transformation();

                    StartCoroutine(EffectChangeFormFirstPart(meshBird, true));
                }
                else
                {
                    ActivateAndDesactivate(true);
                }

                //Conservation de vitesse et de rotation (pour une retransformation éventuelle)
                phc.birdVelocity = pbc.GetMovement();
                phc.resetGravity();
                phc.postTransformationFloating = 1.0f;
            }
            else if (form == 1)
            {
                //Changement en oiseau
                pbc.birdMode = true;
                phc.humanMode = false;

                cc.height = pbc.ccHeight;
                cc.center = pbc.ccCenter;

                if (ChangeformStart)
                {
                    SoundManager sm = FindObjectOfType<SoundManager>();
                    sm.Transformation();

                    StartCoroutine(EffectChangeFormFirstPart(meshHuman, false));
                }
                else
                {
                    ActivateAndDesactivate(false);
                }

                //Deux cas de figure selon si le joueur a touché le sol récemment/le touche actuellement ou est en l'air
                //Est en l'air
                if (!cc.isGrounded)
                {
                    //meshBird.transform.rotation = pbc.lastRotation;
                    transform.LookAt(transform.position + new Vector3(movementVector.x, 0, movementVector.z));
                }
                //Est au sol
                else
                {
                    //meshBird.transform.rotation = meshHuman.transform.rotation;
                }
            }
            else
            {
                form = 1;
                ChangeForm();
            }
        }       
    }

    void ActivateAndDesactivate(bool human)
    {
        rb.isKinematic = !human;

        meshHuman.SetActive(human);
        meshBird.SetActive(!human);

        sb.enabled = human;

        canvasReticule.SetActive(human);
    }

    public Vector3 movementVector = Vector3.zero;
    Vector3 lastPosition;

    IEnumerator CalculateMovementVector()
    {
        lastPosition = transform.position;

        yield return new WaitForEndOfFrame();

        movementVector = transform.position - lastPosition;

        StartCoroutine(CalculateMovementVector());
    }

    [Header("Effet de changement de forme")]
    public float minSize;
    public float speedToMinSize;


    IEnumerator EffectChangeFormFirstPart(GameObject formMesh,bool human)
    {
        if (formMesh.transform.localScale.x <= minSize)
        {
            formMesh.transform.localScale = Vector3.one;

            GameObject particles = Instantiate(particleTransformationPrefab);
            Destroy(particles, particles.GetComponent<ParticleSystem>().startLifetime);
            particles.transform.parent = this.gameObject.transform;
            particles.transform.position = this.transform.position - Vector3.up * 2f + transform.forward * 10;

            

            for (int i = 0; i < renderers.Length; i++)
            {
                renderers[i].material.SetColor("_EmissiveColor", Color.clear);
            }

            ActivateAndDesactivate(human);
        }
        else if (formMesh.transform.localScale.x > minSize)
        {
            float newScaleFrame = formMesh.transform.localScale.x - speedToMinSize * Time.deltaTime;
            Vector3 newScaleFrameVector = Vector3.one * newScaleFrame;

            Color[] newEmissiveColors = new Color[renderers.Length];

            for (int i = 0; i < renderers.Length; i++)
            {
                newEmissiveColors[i] = renderers[i].material.GetColor("_EmissiveColor") + Color.white * Time.deltaTime * speedToMinSize * 800;

                renderers[i].material.SetColor("_EmissiveColor", newEmissiveColors[i]);
            }

            formMesh.transform.localScale = newScaleFrameVector;
            yield return new WaitForEndOfFrame();

            StartCoroutine(EffectChangeFormFirstPart(formMesh,human));
        }

        isInTransformation = false;
    }
}
