﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EasterEggVFXOpdater : MonoBehaviour
{
    public GameObject particles;

    private void Start()
    {
        particles.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" || other.tag == "Arrow")
        {
            UpdateParticles();
        }
    }

    private void UpdateParticles()
    {
        particles.SetActive(false);
        particles.SetActive(true);
    }
}
