﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EasterEggController : MonoBehaviour
{
    public int numberOfEasterEgg;
    public float headScale;
    public Transform[] headBones;

    public List<GameObject> easterEggFinds;
    
    void Start()
    {
        easterEggFinds.Clear();
    }

    
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "EasterEgg")
        {
            bool asFindASame = false;
            
            foreach (GameObject go in easterEggFinds)
            {
                if (other.gameObject == go)
                {
                    asFindASame = true;
                }
            }

            if (!asFindASame)
            {
                easterEggFinds.Add(other.gameObject);

                if (easterEggFinds.Count >= numberOfEasterEgg)
                {
                    foreach (Transform t in headBones)
                    {
                        t.localScale = Vector3.one * headScale;
                    }
                }
            }
        }
    }
}
