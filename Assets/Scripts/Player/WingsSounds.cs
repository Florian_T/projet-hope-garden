﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WingsSounds : MonoBehaviour
{
    public void PlayWings()
    {
        SoundManager sm = FindObjectOfType<SoundManager>();
        sm.WingsFlapping();
    }
}
