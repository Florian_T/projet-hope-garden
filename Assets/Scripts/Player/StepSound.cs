﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StepSound : MonoBehaviour
{
    public bool isWalkingZS = false;
    public bool isWalkingQD = false;

    public bool canStep = false;
    public bool isIdle = true;

    public bool isWalking = false;

    public float stepCooldown = 0.1f;
    public float maxStepCooldown = 0.1f;

    public AudioClip[] footStepsSounds;

    public AudioSource audSrcFootSteps;

    // Start is called before the first frame update
    void Start()
    {
        audSrcFootSteps = gameObject.AddComponent<AudioSource>();
        audSrcFootSteps.volume = 0.5f;
    }

    // Update is called once per frame
    /*void Update()
    {
        if (Input.GetButton("Horizontal") == false && Input.GetButton("Vertical") == false)
        {
            isIdle = true;
        }
        else
        {
            isIdle = false;
        }

        Debug.Log(canStep);
    }

    public void Step()
    {
        if(canStep == false && isIdle == false)
        {
            canStep = true;
            int _chooseIndex = Random.Range(0, footStepsSounds.Length);
            audSrcFootSteps.PlayOneShot(footStepsSounds[_chooseIndex]);
        }
    }

    public IEnumerator BeforeNextStep()
    {
        yield return new WaitForSeconds(0.1f);
        canStep = false;
    }*/

    void Update()
    {
        if (Input.GetButton("Horizontal") || Input.GetButton("Vertical"))
        {
            isWalking = true;
        }
        else
        {
            isWalking = false;
        }

        //Chrono entre les pas
        if (stepCooldown > 0)
        {
            stepCooldown -= Time.deltaTime /** timeScale*/;
        }
    }

    public void Step()
    {
        if ((isWalking) && (stepCooldown <= 0))
        {
            int _chooseIndex = Random.Range(0, footStepsSounds.Length);
            audSrcFootSteps.PlayOneShot(footStepsSounds[_chooseIndex]);

            stepCooldown = maxStepCooldown;
        }
    }
}
