﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBirdController : MonoBehaviour
{


    //Si le joueur est en mode "oiseau"
    [HideInInspector] public bool birdMode;
    [Header("Vitesse")]
    //Vitesse actuelle du joueur
    public float wantedSpeed = 60;
    public float speed = 60;
    public float speedH = 10;
    //Vitesse de modification de la vitesse.
    public float speedModifier = 20;
    public Vector2 speedModifierUpAndDown = new Vector2(10, 25);
    //Vitesse maximale et minimale
    public Vector2 clampSpeed = new Vector2(10,130);
    //Multiplicateur de la vitesse
    public float globalSpeedMultiplier = 1;
    //Vitesse medianne
    public float midSpeed = 60;
    //vitesse pour revenir à midSpeed
    public float returnToMidSpeed = 5;

    [Header("Rotations")]
    //Angle maximal et minimal
    public Vector2 clampAngle = new Vector2(-80,80);
    //Sensibilité de la sourie
    public float mouseSensibility = 2.5f;
    //calcule du tilt actuel
    private float calculateTilt;
    //Force du tilt
    public float tilt = 50;
    //vitesse du tilt 
    public float lerpTilt = 7;
    //Rotation de l'oiseau de -90 à 90
    private float rot;


    [Header("Accélération et décélération")]
    //Vitesse d'augmentation de la vitesse lorsqu'on accélère.
    public float speedIncrease = 20;
    //Vitesse de réduction de la vitesse lorsqu'on ne touches pas aux axes.
    public float speedDecrease = 40;
    //Vitesse d'accélération maximale et minimale
    public Vector2 clampAcceleration = new Vector2(-30,20);
    //Ajout de l'accélération à la vitesse
    private float adder = 0;

    //Stockage de la rotation et du vecteur mouvement pour la détransformation
    private Vector3 tmpMovement;
    [HideInInspector] public Quaternion lastRotation;

    public GameObject accelerationVFX;
    public TrailRenderer[] idleParticle;
    private float timeTrail;

    //Le CharacterController du joueur
    private CharacterController cc;

    [Header("CharacterControllerParameters")]
    public Vector3 ccCenter = Vector3.up * 0.2f;
    public float ccHeight = 0;

    [Header("Animations"),Space(10)]
    public Vector2 clampAnimSpeed = new Vector2(1, 2.7f);

    [Header("Vol stationnaire")]
    public float chanceToQuitStationary = 40;
    public float chanceToReEnterStationary = 60;

    [Header("Vol battement")]
    public float chanceToQuitBattement = 40;
    public float chanceToReEnterBattement = 60;

    [Space(10)]
    public float timeAnimCheck;
    public float animSinSpeed;
    public float animAmountSinOffset;

    private float timeAnimCurrent;

    [HideInInspector] public bool isInGame;
    private float timeScale = 1;
    private GameManager gm;

    void Start()
    {
        cc = GetComponent<CharacterController>();
        gm = FindObjectOfType(typeof(GameManager)) as GameManager;

        GameObject player = FindObjectOfType(typeof(PlayerHumanController)) as GameObject;
        timeTrail = idleParticle[0].time;
    }

    void Update()
    {
        if (isInGame && !GetComponent<PlayerLife>().isDead)
        {
            timeScale = gm._timeScale;

            if (birdMode)
            {
                //Ajout des particules d'accélérations
                accelerationVFX.SetActive(true);

                //Rotations
                //Calcule la rotation actuelle en degrée, 90 étant en haut, -90 en bas et 0 en face
                rot = Vector3.Angle(-Vector3.up, transform.forward) - 90;
                float unlerpRot = Mathf.InverseLerp(clampAngle.y, clampAngle.x, rot);
                speedModifier = Mathf.Lerp(speedModifierUpAndDown.x, speedModifierUpAndDown.y, unlerpRot);

                //Ajout de l'accélération à la vitesse
                AddingInputSpeed();


                speed = Mathf.Lerp(speed, wantedSpeed + adder, 8f * Time.deltaTime);

                //vecteur mouvement avancant devant selon la vitesse
                Vector3 movement = transform.forward * speed * Time.deltaTime;

                //Modification de la vitesse en fonction de l'inclinaison du joueur
                wantedSpeed -= transform.forward.y * Time.deltaTime * speedModifier;

                if (wantedSpeed > midSpeed)
                    wantedSpeed -= returnToMidSpeed * Time.deltaTime;
                else
                    wantedSpeed += returnToMidSpeed * Time.deltaTime;

                //Clamp de la vitesse
                wantedSpeed = Mathf.Clamp(wantedSpeed, clampSpeed.x, clampSpeed.y);

                //Update de la rotation
                UpdateRotation();

                //passage en space.Self pour assigner la valeur de déplacement horizontale
                movement = transform.InverseTransformDirection(movement);

                float inputH = Input.GetAxis("Horizontal") * speedH * Time.deltaTime;
                movement.x = inputH;

                //Remise en space.World
                movement = transform.TransformDirection(movement);
                movement *= globalSpeedMultiplier;

                //Fait bouger le joueur
                cc.Move(movement * timeScale);

                //Collecte des informations pour la détransformation
                tmpMovement = transform.TransformVector(movement);
                lastRotation = transform.rotation;

                TestingEnvironnementDistanceToScreenShake();

                Animator acBird = GetComponentInChildren<Animator>();
                acBird.SetFloat("Speed", acBird.GetFloat("Speed") * gm._timeScale);
                acBird.SetFloat("SpeedStationnaire", acBird.GetFloat("SpeedStationnaire") * gm._timeScale);
            }
            else
            {
                accelerationVFX.SetActive(false);
            }
        }
    }
    
    //Update la rotation du joueur
    void UpdateRotation()
    {
        if (!gm.gameIsPause)
        {

            //Axe vertical
            float mouseY = -Input.GetAxis("Mouse Y");
            mouseY = Mathf.Clamp(mouseY, -1, 1);

            if ((rot > clampAngle.x && mouseY > 0) || (rot < clampAngle.y && mouseY < 0))
                transform.Rotate(new Vector3(mouseY * (mouseSensibility * FindObjectOfType<GameManager>()._sensitivityBird), 0, 0), Space.Self);

            //Axe Horizontal
            float mouseX = Input.GetAxis("Mouse X");
            mouseX = Mathf.Clamp(mouseX, -1, 1);

            transform.Rotate(new Vector3(0, mouseX * (mouseSensibility * FindObjectOfType<GameManager>()._sensitivityBird), 0), Space.World);

            //Calcule du tilt
            calculateTilt = Mathf.Lerp(calculateTilt, -mouseX * tilt, lerpTilt * Time.deltaTime);
            //Assignation du tilt
            transform.rotation = Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y, calculateTilt);
        }
    }

    private bool asPlayParticles;

    //Ajoute l'accélération
    void AddingInputSpeed()
    {
        Animator acBird = GetComponentInChildren<Animator>();
        acBird.SetFloat("Speed", Mathf.Lerp(clampAnimSpeed.x, clampAnimSpeed.y, Mathf.InverseLerp(clampSpeed.x, clampSpeed.y, speed)) + Mathf.Sin(Time.time / animSinSpeed) * animAmountSinOffset);

        if (Input.GetAxis("Vertical") != 0)
        {
            adder += Input.GetAxis("Vertical") * Time.deltaTime * speedIncrease;
            adder = Mathf.Clamp(adder, clampAcceleration.x, clampAcceleration.y);

            //Animations
            acBird.SetBool("Battement", true);

            addVariationInAnimation(acBird, chanceToQuitBattement, chanceToReEnterBattement, "ChanceToStationaryInBattement");

            //Update des particles d'accélération
            if (Input.GetAxis("Vertical") > 0)
            {
                if (!asPlayParticles)
                {
                    asPlayParticles = true;

                    ParticleSystem[] particles = accelerationVFX.GetComponentsInChildren<ParticleSystem>();
                    foreach (ParticleSystem p in particles)
                    {
                        p.loop = true;
                        p.Play();
                    }
                }
            }

            foreach (TrailRenderer t in idleParticle)
            {
                t.time -= Time.deltaTime * 0.5f;
            }
        }
        else
        {
            if (adder > -0.1f && adder < 0.1f)
                adder = 0;
            else if (adder > 0)
                adder -= Time.deltaTime * speedDecrease;
            else if (adder < 0)
                adder += Time.deltaTime * speedDecrease;

            //Animations
            acBird.SetBool("Battement", false);

            addVariationInAnimation(acBird, chanceToQuitStationary, chanceToReEnterStationary, "ChanceToBattementInStationary");

            //Update des particles d'accélération
            ParticleSystem[] particles = accelerationVFX.GetComponentsInChildren<ParticleSystem>();
            foreach (ParticleSystem p in particles)
            {
                p.loop = false;
                asPlayParticles = false;
            }

            foreach (TrailRenderer t in idleParticle)
            {
                t.time = Mathf.Clamp(t.time + Time.deltaTime * 0.3f, 0, timeTrail);
            }
        }
    }

    void addVariationInAnimation(Animator birdAnimatorController, float chanceLeave, float chanceEnter, string nameOfAnimBool)
    {
        timeAnimCurrent += Time.deltaTime;

        if (timeAnimCurrent >= timeAnimCheck)
        {
            timeAnimCurrent = 0;

            float random = Random.Range(0, 100);

            if (birdAnimatorController.GetBool(nameOfAnimBool))
            {
                if (random <= chanceEnter)
                {
                    birdAnimatorController.SetBool(nameOfAnimBool, false);
                }
            }
            else
            {
                if (random <= chanceLeave)
                {
                    birdAnimatorController.SetBool(nameOfAnimBool, true);
                }
            }
        }
    }

    //Récupère le vecteur mouvement
    public Vector3 GetMovement()
    {
        return transform.InverseTransformDirection(tmpMovement);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag != "Player" && other.gameObject.tag != "Phoenix" && other.gameObject.tag != "SuperNova" && other.gameObject.tag != "LavaCollision" && other.gameObject.tag != "Portal" && birdMode)
        {
            GetComponent<PlayerSwitcher>().ChangeForm();
        }
    }

    [Header("ScreenShake")]
    public float rayonMin;
    public Vector2 rayonMax;
    private float actualRayonMax;

    public AnimationCurve howToShakeWithDistance;

    public Vector2 shakeMagnitudeMinMax;
    public LayerMask layers;


    Vector3[] allDir = new Vector3[24];

    void TestingEnvironnementDistanceToScreenShake()
    {
        #region Set vectors
        allDir[0] = new Vector3(1, 0, 0);
            allDir[8] = new Vector3(1, 0, 1);
            allDir[9] = new Vector3(1, 0, -1);
        allDir[1] = new Vector3(0, 1, 0);
            allDir[10] = new Vector3(0, 1, 1);
            allDir[11] = new Vector3(0, 1, -1);
        allDir[2] = new Vector3(-1, 0, 0);
            allDir[12] = new Vector3(-1, 0, 1);
            allDir[13] = new Vector3(-1, 0, -1);
        allDir[3] = new Vector3(0, -1, 0);
            allDir[14] = new Vector3(0, -1, 1);
            allDir[15] = new Vector3(0, -1, -1);

        allDir[4] = new Vector3(1, 1, 0);
            allDir[16] = new Vector3(1, 1, 1);
            allDir[17] = new Vector3(1, 1, -1);
        allDir[5] = new Vector3(-1, 1, 0);
            allDir[18] = new Vector3(-1, 1, 1);
            allDir[19] = new Vector3(-1, 1, -1);
        allDir[6] = new Vector3(1, -1, 0);
            allDir[20] = new Vector3(1, -1, 1);
            allDir[21] = new Vector3(1, -1, -1);
        allDir[7] = new Vector3(-1, -1, 0);
            allDir[22] = new Vector3(-1, -1, 1);
            allDir[23] = new Vector3(-1, -1, -1);
        #endregion

        float unlerpSpeed = Mathf.InverseLerp(clampSpeed.x, clampSpeed.y + clampAcceleration.y, speed);
        actualRayonMax = Mathf.Lerp(rayonMax.x, rayonMax.y, unlerpSpeed);

        float minDist = actualRayonMax;

        foreach (Vector3 dir in allDir)
        {
            RaycastHit hit;

            Ray ray = new Ray(transform.position, dir);

            if (Physics.Raycast(ray , out hit, layers))
            {
                float dist = Vector3.Distance(transform.position, hit.point);

                if (dist <= actualRayonMax && hit.transform.gameObject.tag != "Player")
                {
                    if (minDist >= dist)
                        minDist = dist;

                    Debug.DrawLine(transform.position, hit.point, Color.red);
                }
            }
        }

        float unlerpDist = Mathf.InverseLerp(actualRayonMax, rayonMin, minDist);
        unlerpDist *= unlerpSpeed;
        unlerpDist = howToShakeWithDistance.Evaluate(unlerpDist);

        SoundManager sm = FindObjectOfType<SoundManager>();
        sm.audSrcWindSpeed.pitch = 1 + unlerpDist * 2;

        float shakeMagnitude = Mathf.Lerp(shakeMagnitudeMinMax.x, shakeMagnitudeMinMax.y , unlerpDist);

        ScreenShake ss = Camera.main.GetComponent<ScreenShake>();
        if (!ss.isShaking && unlerpDist > 0)
        {
            StartCoroutine(ss.Shake(0.1f, shakeMagnitude));
        }
            
    }
}