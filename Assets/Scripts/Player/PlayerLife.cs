﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerLife : MonoBehaviour
{
    // VARIABLES ET OBJETS

    [Header("Vie du joueur")]
    [Tooltip("Vie du joueur au départ et vie maximale")]
    public float _life = 100;
    [HideInInspector] public bool isDead;
    //Vie maximum
    private float maxLife = 0f;

    [Header("Visuel")]
    [Tooltip("Objet sur le HUD représentant la vie")]
    public GameObject[] heartObjects;
    //TransformPosition au start
    private Vector3 baseTransformPosition;
    [Tooltip("Taille minimale et maximale")]
    public Vector2 scaleMinMax;
    [Tooltip("Rotation minimale et maximale")]
    public Vector2 rotationMinMax;

    [Tooltip("Le nombre d'itération de screenShake")]
    public int iterationOfShake = 30;
    [Tooltip("La puissance du screenShake")]
    public float forceOfShake = 30;

    [Tooltip("Les particules de dégâts")]
    public GameObject damageParticles;

    [Header("Régénération")]
    [Tooltip("Le temps avant que la régénération de la vie ne commence")]
    public float timeBeforeRegen;
    //Le calcule du temps depuis la derniere prise de dégât
    private float calculateTimeBeforeRegen;
    [Tooltip("Vitesse de régénération")]
    public float regenSpeed;

    [Header("Niveau de dégâts")]
    [Tooltip("Les dégâts les plus important (Nova)")]
    public float majorDamage;
    [Tooltip("Les dégâts courant (Boules de feu)")]
    public float mediumDamage;
    [Tooltip("Les dégâts les moins puissant (Collisions avec le boss, Onde de choc des boules de feu ")]
    public float minorDamage;
    [Tooltip("Dégât des flammes au sol")]
    public float microDamage;

    [Header("La prise de dégât par les flammes")]
    public float timer;
    private float timed;
    public bool isTakingDamageFromFire;

    [Header("Son")]
    [Tooltip("Activation ou désactivation (principalement si vous n'avez pas de sound manager dans votre scène comme moi et que vous avez une flemme monstre de le configurer pour que ça marche dans votre scène mais que vous souhaitez quand même faire des tests parceque c'est important de tester son code avant de push... Bah oui, si on push un code qui fonctionne pas, ça casse le projet des autres et du coup on peut plus travailler.")]
    public bool activeSound;

    [HideInInspector] public bool isInGame;

    //Variables pour faire disparaitre le coeur
    [Header("Disparition du coeur")]
    [Tooltip("Temps avant la disparition du coeur lorsqu'il est plein")]
    public float timeBeforeClear;
    private float calculateTimeBeforeClear;

    [Tooltip("Vitesse pour que le coeur disparaisse")]
    public float speedClear;
    [Tooltip("Vitesse pour que le coeur réaparaisse")]
    public float speedUnClear;

    //Le gameManager
    private GameManager gm;

    private void Start()
    {
        //On récupère le gamemanager
        gm = FindObjectOfType<GameManager>();

        //On set maxLife à la vie telle quelle est de base
        maxLife = _life;

        //On update l'image
        UpdateDisplay();

        baseTransformPosition = heartObjects[0].GetComponent<RectTransform>().position;
    }

    void Update()
    {
        if (isInGame)
        {
            #region DebugInput
            // Test de la fonction de perte de vie
            /*if (Input.GetKeyDown(KeyCode.T))
            {
                TakeDamage(majorDamage);
            }

            //Fait mourir le joueur
            if (Input.GetKeyDown(KeyCode.V))
            {
                Die();
            }*/
            #endregion

            //Clamp des valeurs
            _life = Mathf.Clamp(_life, 0, maxLife);
            calculateTimeBeforeRegen = Mathf.Clamp(calculateTimeBeforeRegen, 0, timeBeforeRegen);

            //Timer avant de regen
            calculateTimeBeforeRegen += Time.deltaTime;

            //Si le timer est complété
            if (calculateTimeBeforeRegen >= timeBeforeRegen)
            {
                //On soigne le joueur
                Healing();
            }

            if (isTakingDamageFromFire)
            {
                timed += Time.deltaTime;

                if (timed >= timer)
                {
                    timed -= timer;
                    TakeDamage(microDamage);
                }
            }

            ClearHeartOnCompleteLife();
        }
    }

    void ClearHeartOnCompleteLife()
    {
        if (_life >= maxLife)
        {
            if (calculateTimeBeforeClear < timeBeforeClear)
            {
                calculateTimeBeforeClear += Time.deltaTime;
            }
            else
            {
                foreach (GameObject heart in heartObjects)
                {
                    Image heartImage = heart.GetComponent<Image>();
                    float a = heartImage.color.a - Time.deltaTime * speedClear;

                    Color newColor = new Color(heartImage.color.r, heartImage.color.g, heartImage.color.b, a);

                    heartImage.color = newColor;
                }
            }
        }
        else
        {
            calculateTimeBeforeClear = 0;

            foreach (GameObject heart in heartObjects)
            {
                Image heartImage = heart.GetComponent<Image>();
                float a = heart.GetComponent<Image>().color.a;
                a = Mathf.Lerp(a, 1, speedUnClear * Time.deltaTime);

                Color newColor = new Color(heartImage.color.r, heartImage.color.g, heartImage.color.b, a);

                heartImage.color = newColor;
            }
        }
    }

    // FONCTION perte d'un point de vie
    public void TakeDamage(float amount)
    {
        if (!isDead)
        {
            //si le joueur n'est pas en godMode
            if (gm._godMod == false && !gm.gameIsPause)
            {
                //Si le son est activé
                if (activeSound)
                {
                    //On joue le son
                    SoundManager sm = FindObjectOfType<SoundManager>();
                    sm.Hurt();
                }

                //On fait bouger l'écran en mode screenShake
                ScreenShake ss = FindObjectOfType<ScreenShake>();
                StartCoroutine(ss.Shake(.25f, .4f));

                if (_life > amount)
                {
                    //On fait perdre le montant de dégât de l'attaque à la vie du joueur
                    _life -= amount;

                    // Mise à jour de l'affichage
                    UpdateDisplay();

                    //On fait jouer les particules
                    ParticleSystem[] particles = damageParticles.GetComponentsInChildren<ParticleSystem>();
                    foreach (ParticleSystem partSystem in particles)
                    {
                        partSystem.Play();
                    }
                }
                else if (!isDead)
                {
                    Die();
                }

                //Après chaque coups, je timer de regen est remi à 0
                calculateTimeBeforeRegen = 0;

                StartCoroutine(Shake(iterationOfShake, forceOfShake));
            }
        }
    }

    // FONCTION récupération d'un point de vie
    public void Healing()
    {
        //Si la vie est bien inférieur à la vie max alors on ajoute de la vie
        if (_life < maxLife)
            _life += Time.deltaTime * regenSpeed;

        UpdateDisplay();
    }

    // Mise à jour de l'affichage des vies
    public void UpdateDisplay()
    {
        //unlerp de la vie pour obtenir un chiffre entre 0 et 1
        float unlerpLife = Mathf.InverseLerp(0, maxLife, _life);

        //Changement du scale du visuel en fonction de la vie
        float lerpHeartScale = Mathf.Lerp(scaleMinMax.x, scaleMinMax.y, unlerpLife);
        float lerpHeartRotation = Mathf.Lerp(rotationMinMax.y, rotationMinMax.x, unlerpLife);

        //Création du vecteur de scale
        Vector3 newScale = Vector3.one * lerpHeartScale;

        //Remplissage du coeur
        heartObjects[1].GetComponent<Image>().fillAmount = unlerpLife;


        //Application du scale et de la rotation sur tous les sprites de coeur
        foreach (GameObject heart in heartObjects)
        {
            //Nouvelle rotation
            Quaternion newRotation = Quaternion.Euler(heart.GetComponent<RectTransform>().localEulerAngles.x, heart.GetComponent<RectTransform>().localEulerAngles.y, lerpHeartRotation);

            //application du scale
            heart.GetComponent<RectTransform>().localScale = newScale;
            //application de la rotation
            heart.GetComponent<RectTransform>().localRotation = newRotation;
        }
        
    }


    //Fonction de mort
    private void Die()
    {
        isDead = true;

        //Le joueur meurt
        _life = 0;

        // Mise à jour de l'affichage
        UpdateDisplay();

        //On fait jouer les particules
        ParticleSystem[] particles = damageParticles.GetComponentsInChildren<ParticleSystem>();
        foreach (ParticleSystem partSystem in particles)
        {
            partSystem.Play();
        }

        StartCoroutine(TransitionDie());
    }

    [Header("Mort")]
    public Image dieImage;
    public float speedFade;
    public Animator skinAnimator;

    IEnumerator TransitionDie()
    {
        SoundManager sm = FindObjectOfType<SoundManager>();
        sm.DieSound();

        PlayerSwitcher ps = GetComponent<PlayerSwitcher>();
        if (ps.form == 1)
        {
            ps.ChangeForm();

            yield return new WaitWhile(() => ps.isInTransformation);
        }
        
        skinAnimator.SetTrigger("Dead");

        for (float a = 0; a < 1; a += Time.deltaTime * speedFade)
        {
            Color newColor = new Color(dieImage.color.r, dieImage.color.g, dieImage.color.b, a);

            dieImage.color = newColor;
            yield return new WaitForEndOfFrame();

            gm._canvasGame[2].GetComponent<Animator>().SetBool("Close", true);
        }

        yield return new WaitForSeconds(2f);

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    IEnumerator Shake(int number, float power)
    {
        for (int i = 0; i < number; i++)
        {
            float randomX = Random.Range(-power, power);
            float randomY = Random.Range(-power, power);

            foreach (GameObject heart in heartObjects)
            {
                heart.GetComponent<RectTransform>().position = baseTransformPosition + new Vector3(randomX, randomY, 0);
            }

            yield return new WaitForEndOfFrame();
        }

        foreach (GameObject heart in heartObjects)
        {
            heart.GetComponent<RectTransform>().position = baseTransformPosition;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Lava")
        {
            TakeDamage(minorDamage);
        }
    }
}
