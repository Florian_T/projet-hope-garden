﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationControllerBird : MonoBehaviour
{
    [Header("Random de vitesse")]
    [Tooltip("Vitesse minimale et maximale")]
    public Vector2 clampSpeed = new Vector2(1,3.2f);
    [Tooltip("Le taux de random")]
    public float randomAmount;
    [Tooltip("La vitesse de changement du random")]
    public float speedRandomSin;

    [Header("Vol Stationaire")]
    [Tooltip("Chance en % que l'oiseau passe en vol stationaire")]
    public float chanceOfStationary;
    [Tooltip("Combien de temps entre chaque recalcule de vol stationaire")]
    public float checkedTime;

    //La vitesse visée entre les valeurs de clampSpeed
    private float lerpSpeed;
    //Le temps actuel pour le chekedTime
    private float timed;

    //L'animator
    Animator ac;
    //Le script de déplacement de l'oiseau
    PlayerBirdController pbc;
    
    private void Start()
    {
        //On récupère l'Animator
        ac = GetComponentInChildren<Animator>();

        //On récupère le script de déplacement de l'oiseau
        pbc = GetComponentInParent<PlayerBirdController>();
    }
    
    void Update()
    {
        //Calcule du lerpSpeed
        lerpSpeed = Mathf.InverseLerp(pbc.clampSpeed.x, pbc.clampSpeed.y, pbc.speed);
        lerpSpeed += Mathf.Sin(Time.time * speedRandomSin) * randomAmount;

        //Assignation du lerpSpeed et nouvelle vitesse de l'animation
        ac.SetFloat("Speed", Mathf.Lerp(clampSpeed.x, clampSpeed.y, lerpSpeed));

        //Boucle de x secondes
        timed += Time.deltaTime;

        if (timed >= checkedTime)
        {
            timed = 0;

            //Si le chiffre tombe dans le pourcentage alors on fait que le prochain state est le stationaire
            float randomStationnary = Random.Range(0, 100);
            if (randomStationnary <= chanceOfStationary) 
            {
                ac.SetTrigger("Stationaire");
            }
        }
        
    }
}
