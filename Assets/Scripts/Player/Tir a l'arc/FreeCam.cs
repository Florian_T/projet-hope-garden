﻿using UnityEngine;
using UnityEngine.UI;

public class FreeCam : MonoBehaviour
{
    [SerializeField] private float turnSpeed = 1.5f;
    [SerializeField] private float turnsmoothing = .1f;
    [SerializeField] private float tiltMax = 75f;
    [SerializeField] private float tiltMin = 45f;

    private float lookAngle;
    private float tiltAngle;

    private float smoothX = 0;
    private float smoothY = 0;
    private float smoothXvelocity = 0;
    private float smoothYvelocity = 0;

    public Transform pivot;
    private Transform cam;
    public GameObject holder2;

    private float distance = -7.0f;

    private void Awake()
    {
        cam = GetComponentInChildren<Camera>().transform;
        pivot = cam.parent.parent;

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        HandleRotationMovement();

        if(GetComponent<ShootBow>().isShooting == true)
        {
            distance = -4.0f;
        }
        else
        {
            distance = -7.0f;
        }

        holder2.transform.localPosition = new Vector3(0, 0, distance);
    }

    void HandleRotationMovement()
    {
        float x = Input.GetAxis("Mouse X");
        float y = Input.GetAxis("Mouse Y");

        smoothX = Mathf.SmoothDamp(smoothX, x, ref smoothXvelocity, turnsmoothing);
        smoothY = Mathf.SmoothDamp(smoothY, y, ref smoothYvelocity, turnsmoothing);

        lookAngle += smoothX * turnSpeed;

        transform.rotation = Quaternion.Euler(0f, lookAngle, 0);

        tiltAngle -= smoothY * turnSpeed;
        tiltAngle = Mathf.Clamp(tiltAngle, -tiltMin, tiltMax);

        pivot.localRotation = Quaternion.Euler(tiltAngle, 0, 0);
    }
}
