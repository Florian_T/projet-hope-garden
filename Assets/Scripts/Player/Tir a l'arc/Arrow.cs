﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    // VARIABLES ET OBJETS

    //Vélocité de la flèche
    [HideInInspector]
    public Vector3 velocity;
    public GameObject particlePrefabOnWeakPoint;
    public GameObject particlePrefabOnOtherPoint;

    public GameObject[] goADesactiver;
    public TrailRenderer trailOfArrow;
    public float speedFadeTrail;

    //Est ce que la flèche est en l'air
    private bool isInAir = true;

    //Récupération des donées de positions
    private Transform _anchor;

    private GameManager gm;

    void Start()
    {
        //Récupération du rigidbody
        gm = FindObjectOfType(typeof(GameManager)) as GameManager;
    }

    void Update()
    {
        //Tant que la flèche est en l'air
        if (isInAir)
        {
            //Ajout de gravité
            velocity -= Vector3.up * 9.81f * Time.deltaTime * gm._timeScale;

            transform.rotation = Quaternion.LookRotation(velocity);
            transform.Translate(velocity * Time.deltaTime * gm._timeScale, Space.World);
        }
        //Si la flèche n'est plus en l'air et que _anchor a été set
        else if (_anchor != null)
        {
            //Set la position et la rotation de la flèche aux position et rotation de _anchor
            transform.position = _anchor.position;
            transform.rotation = _anchor.rotation;
        }

        RaycastHit hit;
        Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * velocity.magnitude * Time.deltaTime, Color.yellow);
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, velocity.magnitude * Time.deltaTime))
        {
            if (hit.transform.gameObject.tag != this.tag && hit.transform.gameObject.tag != "Player" && hit.transform.gameObject.tag != "Phoenix" && hit.transform.gameObject.name != "BoxAttackDetection")
            {
                if (hit.transform.tag == "EasterEgg")
                {
                    Destroy(gameObject);
                }
                else
                {
                    velocity = Vector3.zero;

                    //La flèche n'est plus en l'air
                    isInAir = false;

                    //Créer un objet anchor
                    GameObject anchor = new GameObject("ARROW_ANCHOR");
                    //anchor prend la posisition de la flèche
                    anchor.transform.position = hit.point;
                    //anchor prand la rotation de la flèche
                    anchor.transform.rotation = transform.rotation;
                    //anchor devient enfant du parent
                    anchor.transform.parent = hit.transform;
                    //Assignation de _anchor afin de set la position de la flèche
                    _anchor = anchor.transform;

                    //Permet à la flèche d'être traversée.
                    GetComponent<Collider>().isTrigger = true;

                    foreach (GameObject go in goADesactiver)
                    {
                        go.SetActive(false);
                    }

                    StartCoroutine(DesactivateTrail());
                }
            }
            else if (hit.transform.gameObject.tag == "Phoenix")
            {
                //Récupération du boss parameters
                BossParameters bp = hit.transform.GetComponentInParent<BossParameters>();
                //Bolean pour détecter si le boss à déjà reçu les dégâts
                bool asDamageOnWeakPoint = false;

                //On test s'il a collide un point sensible
                foreach (string colName in bp.collidersParametersDefaultState.colliders)
                {
                    if (colName == hit.collider.name)
                    {
                        //S'il collide un point sensible on retire le nombre de point de vie associé
                        hit.transform.GetComponentInParent<PhoenixSoin>().BossDamage(bp.actualColliders.damageOnWeakPoints);

                        asDamageOnWeakPoint = true;

                        //On instancie les particules
                        GameObject particleArrow_clone = Instantiate(particlePrefabOnWeakPoint);
                        Destroy(particleArrow_clone, 3);
                        particleArrow_clone.transform.position = transform.position;

                        break;
                    }
                }

                //S'il n'a pas subit de dégât alors il n'a pas touché de point sensible mais un point lambda
                if (!asDamageOnWeakPoint)
                {
                    //On instancie les particules
                    GameObject particleArrow_clone = Instantiate(particlePrefabOnOtherPoint);
                    Destroy(particleArrow_clone, 3);
                    particleArrow_clone.transform.position = transform.position;

                    //on retire le nombre de point de vie associer à cette zone
                    hit.transform.GetComponentInParent<PhoenixSoin>().BossDamage(bp.actualColliders.damageOnOtherPoints);
                }

                //On détruit la flèche
                Destroy(gameObject);
            }            
        }
    }

    IEnumerator DesactivateTrail()
    {
        float stackedDuration = trailOfArrow.time;

        for (float timer = stackedDuration; timer > 0; timer -= Time.deltaTime * speedFadeTrail)
        {
            trailOfArrow.time = timer;
            trailOfArrow.widthMultiplier = Mathf.InverseLerp(0, stackedDuration, timer);
            yield return new WaitForEndOfFrame();
        }

        trailOfArrow.gameObject.SetActive(false);
    }

    // ARRET DE LA FLECHE
    /*private void OnTriggerEnter(Collider other)
    {
        //Si l'objet collidé n'est pas une flèche et n'est pas le joueur
        /*if (other.gameObject.tag != this.tag && other.gameObject.tag != "Player" && other.gameObject.tag != "Phoenix" && other.gameObject.name != "BoxAttackDetection")
        {
            if (other.tag == "EasterEgg")
            {
                Destroy(gameObject);
            }
            else
            {
                //La flèche n'est plus en l'air
                isInAir = false;

                //Créer un objet anchor
                GameObject anchor = new GameObject("ARROW_ANCHOR");
                //anchor prend la posisition de la flèche
                anchor.transform.position = transform.position;
                //anchor prand la rotation de la flèche
                anchor.transform.rotation = transform.rotation;
                //anchor devient enfant du parent
                anchor.transform.parent = other.transform;
                //Assignation de _anchor afin de set la position de la flèche
                _anchor = anchor.transform;

                //Permet à la flèche d'être traversée.
                GetComponent<Collider>().isTrigger = true;

                foreach (GameObject go in goADesactiver)
                {
                    go.SetActive(false);
                }

                velocity = Vector3.zero;
            }
        }
        else if (other.gameObject.tag == "Phoenix")
        {
            //Récupération du boss parameters
            BossParameters bp = other.GetComponentInParent<BossParameters>();
            //Bolean pour détecter si le boss à déjà reçu les dégâts
            bool asDamage = false;

            //On test s'il a collide un point sensible
            foreach (string colName in bp.collidersParametersDefaultState.colliders)
            {
                if (colName == other.name)
                {
                    //S'il collide un point sensible on retire le nombre de point de vie associé
                    other.GetComponentInParent<PhoenixSoin>().BossDamage(bp.actualColliders.damageOnWeakPoints);
                    
                    asDamage = true;

                    //On instancie les particules
                    GameObject particleArrow_clone = Instantiate(particlePrefab);
                    Destroy(particleArrow_clone, 2);
                    particleArrow_clone.transform.position = transform.position;
                    
                    break;
                }
            }

            //S'il n'a pas subit de dégât alors il n'a pas touché de point sensible mais un point lambda
            if (!asDamage)
            {
                //on retire le nombre de point de vie associer à cette zone
                other.GetComponentInParent<PhoenixSoin>().BossDamage(bp.actualColliders.damageOnOtherPoints);
            }

            //On détruit la flèche
            Destroy(gameObject);
        }
    }*/
}
