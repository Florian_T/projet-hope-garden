﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootBow : MonoBehaviour
{  
    // VARIABLES ET OBJETS

    public float chargeShoot = 0;                                                        // Taux de charge du tir (de 0 à 1)
    [Range(0, 1)]
    public float conditionToShoot;

    [Header("Parametres du tir")]
    public float chargeRate = 3f;                                                             // Vitesse du bandage de l'arc
    public float power = 70f;                                                                   // Puissance du tir

    [Header("GameObjects et Transforms nécessaires")]
    public GameObject arrowPrefab;                                                              // Prefab de la flèche
    public Transform spawnArrow;                                                                // Lieu ou la flèche va spawn
    public Transform TargetShoot;                                                               // Zone visée par le joueur
    public RectTransform PanelCrossHair;                                                        // Réticule
    public Vector3 angleOffset; //offset de l'angle

    [Header("Réticule")]
    public float crosshairMinSize = 75f;                                                        // Taille minimum réticule
    public float crosshairMaxSize = 150f;                                                       // Taille maximun réticule
    private float size;                                                                         // Taille du réticule après calcul


    // Booléen, le joueur est il en train de tirer/viser ?
    [HideInInspector]
    public bool isShooting;
    [HideInInspector] public bool asShoot;
    [HideInInspector] public bool asBeginShoot;

    private bool _shootOnce = false;

    [HideInInspector] public bool isInGame;

    private float timeScale;
    private GameManager gm;

    //animation
    public Animator animBody;
    public Animator animArc;

    private void Start()
    {
        gm = FindObjectOfType(typeof(GameManager)) as GameManager;
    }

    void Update()
    {
        if (!gm.gameIsPause && !GetComponent<PlayerLife>().isDead)
        {
            if (isInGame)
            {
                //TimeScale
                timeScale = gm._timeScale;

                if (asShoot)
                    asShoot = false;
                if (asBeginShoot)
                    asBeginShoot = false;

                // TIR
                if (Input.GetMouseButtonDown(0) || Input.GetAxis("Fire1") > 0)
                {
                    asBeginShoot = true;
                }

                // BANDER L'ARC
                if (Input.GetMouseButton(0) || Input.GetAxis("Fire1") > 0)                                                            // Lors d'un appui sur le clic gauche
                {
                    if (_shootOnce == false)
                    {
                        _shootOnce = true;
                        SoundManager sm = FindObjectOfType<SoundManager>();
                        sm.BendBow();
                    }

                    isShooting = true;

                    //Quand le joueur tire, peut importe où il est, il se tourne vers la direction du tir
                    if (!GetComponent<CharacterController>().isGrounded)
                        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, GetComponent<PlayerHumanController>().mouseX, 0), GetComponent<PlayerHumanController>().lerpRotation);
                    // Le joueur vise t'il ? OUI
                    if (chargeShoot < 1)                                                                // Tant que la charge du tir n'est pas maximale (<1)
                    {
                        chargeShoot += Mathf.Clamp((chargeRate * Time.deltaTime), 0, 1);            // La charge du tir augmente (en fonction de chargeRate)
                    }
                }

                // LANCER LA FLECHE
                // Au laché du clic gauche
                if (Input.GetMouseButtonUp(0) || Input.GetAxis("Fire1") == 0 && isShooting)
                {
                    SoundManager sm = FindObjectOfType<SoundManager>();
                    sm.StopBend();

                    // Le joueur vise t'il ? NON
                    isShooting = false;
                    _shootOnce = false;

                    //Le joueur vient de tirer la flèche ?
                    asShoot = true;

                    if (chargeShoot >= conditionToShoot)
                    {
                        // Création de la flèche
                        GameObject newArrow = Instantiate(arrowPrefab) as GameObject;
                        // Positionnement de la flèche
                        newArrow.transform.position = spawnArrow.position;

                        sm.Shoot();

                        //Récupération du vecteur forward
                        Vector3 fwd = spawnArrow.transform.forward;
                        //Calcule de la puissance en fonction de la puissance de base et de la force mise au moment du tir
                        float calculatedPower = power * chargeShoot;
                        //Le vecteur de direction actuel du joueur
                        Vector3 playerMovement = GetComponent<PlayerSwitcher>().movementVector * 2;

                        //Assignation de la vélocité
                        newArrow.GetComponent<Arrow>().velocity = fwd * calculatedPower + playerMovement;
                    }

                    // La charge du tir repasse à 0
                    chargeShoot = 0;
                }
                // VISER                                                                        
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);                            // Le Raycast part de la position de la souris (qui est toujours au centre de l'écran

                TargetShoot.position = ray.origin + ray.direction * 50;                                 // La cible est déplacée en direction du dernier point du Raycast

                spawnArrow.LookAt(TargetShoot);                                                         // La zone de spawn des flèches est toujours orienté vers la cible calculée précédemment
                spawnArrow.rotation = Quaternion.Euler(spawnArrow.eulerAngles.x + angleOffset.x, spawnArrow.eulerAngles.y + angleOffset.y, spawnArrow.eulerAngles.z + angleOffset.z);

                // RETICULE
                if (isShooting)
                {
                    PanelCrossHair.gameObject.SetActive(true);
                    size = Mathf.Lerp(150, 75, chargeShoot);                                                // Calcul de la taille du réticule en fonction de la charge du tir 
                    PanelCrossHair.sizeDelta = new Vector2(size, size);                                     // La taille du réticule diminue au fur et à mesure de la charge du tir 
                }
                else
                {
                    PanelCrossHair.gameObject.SetActive(false);
                }

                Animate();
            }
        }
    }

    //Animation du tir à l'arc
    void Animate()
    {
        animBody.SetFloat("chargePercent", chargeShoot);
        animArc.SetFloat("TimePlayBande", chargeShoot);

        animBody.SetBool("isShooting", isShooting);

        if (asBeginShoot)
        {
            animBody.SetTrigger("BeginAttack");
            animArc.SetTrigger("asBeginShoot");
        }

        if (asShoot)
        {
            animBody.SetTrigger("EndingAttack");
            animArc.SetTrigger("asShoot");
        }
            
    }
}
