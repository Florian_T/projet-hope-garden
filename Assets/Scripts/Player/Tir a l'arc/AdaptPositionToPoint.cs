﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdaptPositionToPoint: MonoBehaviour
{
    public Transform point;
    public float lerpPositionSpeed = 7;

    private Vector3 basePosition;
    private float lerpPosition;

    public bool chargeShoot;
    public bool speedBird;

    void Start()
    {
        basePosition = transform.localPosition;
    }
    
    void Update()
    {
        if (chargeShoot)
        {
            lerpPosition = Mathf.Lerp(lerpPosition, GetComponentInParent<ShootBow>().chargeShoot, lerpPositionSpeed * Time.deltaTime);
        }
        else if (speedBird)
        {
            PlayerBirdController pbc = GetComponentInParent<PlayerBirdController>();
            float unlerpSpeed = Mathf.InverseLerp(pbc.clampSpeed.y, pbc.clampSpeed.x, pbc.speed);

            lerpPosition = Mathf.Lerp(lerpPosition, unlerpSpeed, lerpPositionSpeed * Time.deltaTime);
        }

        transform.localPosition = Vector3.Lerp(basePosition, point.localPosition, lerpPosition);
    }
}
