﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionLava : MonoBehaviour
{
    private PlayerLife playerLifeScript;

    //Le gameManager
    private GameManager gm;
    //Le joueur
    private GameObject player;

    private float _beforeNextHurt = 1f;

    // Start is called before the first frame update
    void Start()
    {
        //Récupération du GameManager
        gm = FindObjectOfType(typeof(GameManager)) as GameManager;
        //récupération du joueur
        player = gm.player;
    }

    // Update is called once per frame
    void Update()
    {
        if(_beforeNextHurt > 0)
        {
            _beforeNextHurt -= Time.deltaTime;
        }
    }

    public void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "LavaCollision" && _beforeNextHurt <= 0)
        {
            playerLifeScript = player.gameObject.GetComponent<PlayerLife>();
            //playerLifeScript.TakeDamage(playerLifeScript.minorDamage);
            _beforeNextHurt = 1f;
            //PlayerLife PlLife = (PlayerLife)other.transform.gameObject.GetComponent(typeof(PlayerLife));
            playerLifeScript.TakeDamage(playerLifeScript.minorDamage);

        }
    }
}
