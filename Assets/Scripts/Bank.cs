﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Bank : MonoBehaviour
{
    #region Singleton
    private static Bank _instance;

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this);
        }

        SceneManager.sceneLoaded += OnSceneLoaded;
    }
    #endregion

    private VolumeManager vm;
    public bool asReload;

    public float volumeMaster;
    public float volumeMusic;
    public float volumeSFX;

    private GameManager gm;
    public float sensitivity;
    public float sensitivityBird;

    private void Update()
    {
        if (asReload)
        {
            vm = FindObjectOfType<VolumeManager>();
            vm.volumeMaster = volumeMaster;
            vm.volumeMusic = volumeMusic;
            vm.volumeSFX = volumeSFX;

            vm.sliderMaster.value = volumeMaster;
            vm.sliderMusic.value = volumeMusic;
            vm.sliderSFX.value = volumeSFX;

            gm = FindObjectOfType<GameManager>();
            gm.sliderSensiH.value = sensitivity;
            gm.sliderSensiB.value = sensitivityBird;

            asReload = false;
        }
        else
        {
            vm = FindObjectOfType<VolumeManager>();
            volumeMaster = vm.volumeMaster;
            volumeMusic = vm.volumeMusic;
            volumeSFX = vm.volumeSFX;

            gm = FindObjectOfType<GameManager>();
            sensitivity = gm._sensitivity;
            sensitivityBird = gm._sensitivityBird;
        }
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        asReload = true;
    }
}
