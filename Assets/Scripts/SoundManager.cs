﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour
{
    public bool activeUpdate = true;
    public AudioMixerGroup mixer;
    //public AudioClip novaFireSound;
    //public AudioClip novaExplosion;
    //public AudioClip novaPhoenixSound;
    public AudioClip windSound;
    public AudioClip windSpeedSound;

    public AudioClip transformationSound;
    public AudioClip bendSound;
    public AudioClip dieSound;

    public AudioClip[] Hurts;

    public AudioClip[] BowShoots;

    public AudioClip[] wingsSounds;

    public AudioClip[] stepsSounds;
    
    public AudioSource audSrcNova;
    public AudioSource audSrcHurt;

    private AudioSource audSrcWind;
    public AudioSource audSrcWindSpeed;

    public AudioSource audSrcBowShoot;

    public AudioSource audSrcWings;
    public AudioSource audSrcTransformation;
    public AudioSource audSrcBend;

    public AudioSource audSrcDie;
    public AudioSource audSrcSteps;

    private bool asPlay;

    private Scene actualScene;
    private Scene previousScene;

    private bool paused;

    private bool _doOnceSpeed = false;

    private void Start()
    {
        audSrcNova = gameObject.AddComponent<AudioSource>();
        audSrcNova.spatialBlend = 1;
        audSrcHurt = gameObject.AddComponent<AudioSource>();

        audSrcWind = gameObject.AddComponent<AudioSource>();
        audSrcWind.loop = true;
        audSrcWind.volume = 0.3f;

        audSrcWind.clip = windSound;
        audSrcWind.Play();

        audSrcWindSpeed = gameObject.AddComponent<AudioSource>();
        audSrcWindSpeed.loop = true;

        audSrcBowShoot = gameObject.AddComponent<AudioSource>();

        audSrcWings = gameObject.AddComponent<AudioSource>();

        audSrcTransformation = gameObject.AddComponent<AudioSource>();
        audSrcTransformation.volume = 0.5f;

        audSrcBend = gameObject.AddComponent<AudioSource>();

        audSrcSteps = gameObject.AddComponent<AudioSource>();

        audSrcDie = gameObject.AddComponent<AudioSource>();

        AudioSource[] audioSources = GetComponents<AudioSource>();
        foreach (AudioSource ads in audioSources)
        {
            ads.outputAudioMixerGroup = mixer;
        }
    }


    private void Update()
    {
        if (activeUpdate)
        {
            PlayerBirdController Bird = FindObjectOfType<PlayerBirdController>();
            if (Bird.birdMode)
            {
                if (_doOnceSpeed == false)
                {
                    WindSpeed();
                    _doOnceSpeed = true;
                }
                float _speedBird = Bird.speed;
                //Debug.Log("speedbird" + _speedBird);
                audSrcWindSpeed.volume = Mathf.Clamp01(_speedBird / 100);
                //Debug.Log("lerp" + Mathf.Clamp01(_speedBird / 100));
                audSrcWindSpeed.volume = Mathf.InverseLerp(Bird.clampSpeed.x + Bird.clampAcceleration.x, Bird.clampSpeed.y + Bird.clampAcceleration.y, _speedBird);
                //Debug.Log("lerp" + Mathf.InverseLerp(Bird.clampSpeed.x + Bird.clampAcceleration.x, Bird.clampSpeed.y + Bird.clampAcceleration.y, _speedBird));
            }
            else
            {
                if (audSrcWindSpeed.volume > 0)
                {
                    audSrcWindSpeed.volume -= Time.deltaTime;
                }
                _doOnceSpeed = false;
            }
        }
    }

    // NOVA 

    public IEnumerator FadeOut (AudioSource audioSource, float fadeTime)
    {
        Debug.Log(audioSource.volume);

        audioSource.volume -= fadeTime * Time.deltaTime;
     
        if (audioSource.volume > 0)
        {
            Debug.Log("if");
            StartCoroutine(FadeOut(audSrcNova, 1));
            Debug.Log("if2");
        }
        else
        {
            Debug.Log("else");
            audioSource.Stop();
            audioSource.volume = 1;
            yield return null;
        }
    }

    public void ChargeNova()
    {
        //audSrcNova.PlayOneShot(novaPhoenixSound);
    }

    public void FireNova()
    {
        //audSrcNova.PlayOneShot(novaExplosion);
        //audSrcNova.clip = novaFireSound;
       // audSrcNova.Play();
        
    }

    // HURT

    public void Hurt()
    {
        int _chooseSound = Random.Range(0, Hurts.Length);
        audSrcHurt.PlayOneShot(Hurts[_chooseSound]);
    }

    // WIND SPEED

    public void WindSpeed()
    {
        audSrcWindSpeed.clip = windSpeedSound;
        audSrcWindSpeed.Play();
    }

    // BOW SHOOT

    public void Shoot()
    {
        int _chooseSound = Random.Range(1, BowShoots.Length);
        audSrcBowShoot.PlayOneShot(BowShoots[_chooseSound]);
        audSrcBowShoot.PlayOneShot(BowShoots[0]);
    }

    // WINGS

    public void WingsFlapping()
    {
        int _chooseSound = Random.Range(0, wingsSounds.Length);
        audSrcWings.PlayOneShot(wingsSounds[_chooseSound]);
    }

    // TRANSFORMATION

    public void Transformation()
    {
        audSrcTransformation.PlayOneShot(transformationSound);
    }

    // BEND BOW

    public void BendBow()
    {
        audSrcBend.PlayOneShot(bendSound);
    }

    public void StopBend()
    {
        audSrcBend.Stop();
    }

    // FALL

    public void Fall()
    {
        //audSrcFall.PlayOneShot(fallSound);

        /*StepSound Steps = FindObjectOfTypes<StepSound>();
        Steps.Step();*/

        int _chooseSound = Random.Range(0, stepsSounds.Length);
        audSrcSteps.PlayOneShot(stepsSounds[_chooseSound]);
    }

    // DIE

    public void DieSound()
    {
        audSrcDie.PlayOneShot(dieSound);
    }
}

