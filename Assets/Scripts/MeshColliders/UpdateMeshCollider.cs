﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateMeshCollider : MonoBehaviour
{
    SkinnedMeshRenderer meshRenderer;
    MeshCollider collide;
    public float updateTime;
    private float time;

    void Start()
    {
        meshRenderer = GetComponent<SkinnedMeshRenderer>();
        collide = GetComponent<MeshCollider>();
    }

    private void LateUpdate()
    {
        time += Time.deltaTime;
        if (time >= updateTime)
        {
            time = 0;
            UpdateCollider();
        }
    }

    public void UpdateCollider()
    {
        Mesh colliderMesh = new Mesh();
        meshRenderer.BakeMesh(colliderMesh);
        collide.sharedMesh = null;
        collide.sharedMesh = colliderMesh;
    }
}
