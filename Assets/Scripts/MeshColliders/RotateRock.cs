﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateRock : MonoBehaviour
{
    public Transform[] rockList;
    private Vector3[] rotations;
    private float[] speedRotations;

    public Vector2 speedRange;

    public bool justRotateParent;

    // Start is called before the first frame update
    void Start()
    {
        rockList = GetComponentsInChildren<Transform>();
        rockList[0] = null;

        rotations = new Vector3[rockList.Length];
        speedRotations = new float[rockList.Length];

        for (int i = 1; i < rockList.Length; i++)
        {
            rotations[i] = new Vector3(Random.Range(-1, 1), Random.Range(-1, 1), Random.Range(-1, 1)).normalized;
            speedRotations[i] = Random.Range(speedRange.x,speedRange.y);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (justRotateParent == false)
        {
            for (int i = 1; i < rockList.Length; i++)
            {
                rockList[i].localEulerAngles += rotations[i] * speedRotations[i] * Time.deltaTime;
            }
        }
        else
        {
            transform.localEulerAngles += rotations[1] * speedRotations[1] * Time.deltaTime;
        }
        
    }
}
