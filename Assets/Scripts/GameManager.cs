﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public bool _godMod = false;
    public bool _IsGameFinish = false;

    [HideInInspector]
    public float _timeScale = 1;
    [Range(0.001f,1f)] public float _timeScaleMin = 0.1f;
    [Tooltip("La vitesse à laquelle le temps se rescale au ralentit")]
    public float speedToGoToMinTimeScale;
    [Tooltip("La vitesse à laquelle le temps se rescale à la normal")]
    public float speedToReturnToBaseTimeScale;
    //Valeur pour savoir si on peut revenir au timeScale de base
    private bool returnToBaseTime;
    [Tooltip("Le temps avant que le temps ne se remette normalement même si l'on est en train de tirer")]
    public float timeBeforeReturnToBaseTimeScale = 5;

    public GameObject player;
    public Transform[] spawnPhoenixPositions;
  
    
    private void Start()
    {
        //Cache le curseur
        Cursor.visible = true;
        //passe le curseur en "Locked" pour éviter de voir la sourie lorsqu'on se déplace
        Cursor.lockState = CursorLockMode.None;

        ChangeStateToMenuPrincipal();
        DesactivateCanvas(_canvasPause);

        SetActivePlayerScript(false);

        int randomPoint = Random.Range(0, spawnPhoenixPositions.Length);
        goInGame[0].transform.position = spawnPhoenixPositions[randomPoint].position;

        phoenix = goInGame[0];
    }

    //Switch menu principal & ingame
    public enum State
    {
        MenuPrincipal,
        Options,
        Credits,
        Game
    }

    [HideInInspector] public bool gameIsPause;

    [Header("Switch")]
    public State state;
    public GameObject[] _canvasGame;
    public GameObject[] _canvasMenuPrincipal;
    public GameObject[] _canvasCredits;
    public GameObject[] _canvasOptions;
    public GameObject[] _canvasPause;

    public GameObject[] goInGame;
    private GameObject phoenix;

    [Header("EventSystem pour Controle Manette")]
    public GameObject[] boutonsManette;
    public GameObject[] controlInputs;

    private bool optionInPause = false;

    public float _sensitivity;
    public float _sensitivityBird;
    public Slider sliderSensiH;
    public Slider sliderSensiB;
    

    void Update()
    {
        ScalingTime();

        if (state == State.Game && !gameIsPause)
        {
            //Cache le curseur
            Cursor.visible = false;
            //passe le curseur en "Locked" pour éviter de voir la sourie lorsqu'on se déplace
            Cursor.lockState = CursorLockMode.Locked;

            _timeScale = 1;
        }
        else if (state == State.Game && gameIsPause)
        {
            if (InputManager.FindObjectOfType<InputManager>().GetComponent<InputManager>().m_State == InputManager.eInputState.Controler)
            {
                //cache le curseur
                Cursor.visible = false;
                //Ne lock plus le curseur
                Cursor.lockState = CursorLockMode.Locked;
            }

            else
            {
                //montre le curseur
                Cursor.visible = true;
                //Ne lock plus le curseur
                Cursor.lockState = CursorLockMode.None;
            }

            _timeScale = 0;
        }

        if (state == State.Credits || state == State.MenuPrincipal || state == State.Options)
        {
            if(InputManager.FindObjectOfType<InputManager>().GetComponent<InputManager>().m_State == InputManager.eInputState.Controler)
            {
                //cache le curseur
                Cursor.visible = false;
                //Ne lock plus le curseur
                Cursor.lockState = CursorLockMode.Locked;
            }
            else
            {
                //montre le curseur
                Cursor.visible = true;
                //Ne lock plus le curseur
                Cursor.lockState = CursorLockMode.None;
            }
                       
                DesactivateCanvas(_canvasGame);
        }
        
        if (Input.GetButtonDown("Pause") && !gameIsPause && state == State.Game && player.GetComponent<PlayerLife>()._life > 0)
        {
            SetPauseUnpause();
        }

        makePhoenixLifeVisible();
    }

    [Header("Barre de vie du phoenix.")]
    public GameObject[] lifeBars;
    private bool asShowLifeBar;
    private bool reduceSecondLifeBar;
    public float timeBeforeReduceSecondLifeBar = 1;
    private float timedDeforeReduceSecondLifeBar;
    public float speedInReduceSecondLifeBar = 0.1f;
    private float stackedUnlerp = 10;

    void makePhoenixLifeVisible()
    {
        if (state != State.Game)
        {
            foreach (GameObject go in lifeBars)
            {
                go.GetComponent<RectTransform>().localScale = Vector3.up + Vector3.forward;
            }
        }

        if (state == State.Game)
        {
            PhoenixSoin lifeScript = phoenix.GetComponent<PhoenixSoin>();

            float unlerpLife = Mathf.InverseLerp(0, lifeScript.maxLife, lifeScript._phoenixLife);

            #region En entrant dans le combat
            if (phoenix.GetComponent<BossBehaviour>().isInCombat && unlerpLife > 0)
            {
                asShowLifeBar = true;

                _canvasGame[2].GetComponent<Animator>().SetBool("Open", true);
            }
            #endregion

            lifeBars[0].GetComponent<Image>().fillAmount = unlerpLife;

            if (stackedUnlerp > unlerpLife)
            {
                stackedUnlerp = unlerpLife;

                //Le boss à pris un dégat
                //On réduit la deuxième barre de vie
                reduceSecondLifeBar = true;
                timedDeforeReduceSecondLifeBar = 0;
            }
            else if (stackedUnlerp < unlerpLife)
            {
                stackedUnlerp = unlerpLife;
                lifeBars[1].GetComponent<Image>().fillAmount = unlerpLife;
            }

            if (reduceSecondLifeBar)
            {
                if (timedDeforeReduceSecondLifeBar < timeBeforeReduceSecondLifeBar)
                {
                    timedDeforeReduceSecondLifeBar += Time.deltaTime;
                }
                else
                {
                    if (lifeBars[1].GetComponent<Image>().fillAmount > unlerpLife + Time.deltaTime * speedInReduceSecondLifeBar)
                    {
                        lifeBars[1].GetComponent<Image>().fillAmount -= Time.deltaTime * speedInReduceSecondLifeBar;
                    }
                    else
                    {
                        lifeBars[1].GetComponent<Image>().fillAmount = unlerpLife;
                    }
                }
            }
            
            #region A la victoire du joueur
            if (unlerpLife <= 0 && phoenix.GetComponent<BossBehaviour>().isInCombat && asShowLifeBar)
            {
                _canvasGame[2].GetComponent<Animator>().SetBool("Close", true);
            }
            #endregion
        }
    }

    public void SetOptionsInPause()
    {
        DesactivateCanvas(_canvasPause);
        ActivateCanvas(_canvasOptions);
        optionInPause = true;
    }

    public void UnsetOptionsInPause()
    {
        DesactivateCanvas(_canvasOptions);
        ActivateCanvas(_canvasPause);
        optionInPause = false;
    }

    public void SetPauseUnpause()
    {
        gameIsPause = !gameIsPause;

        if (gameIsPause)
        {
            ShowPause();
            MenuByInput();
        }
        else
        {
            HidePause();
        }
    }

    /*void TestGodMod()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            if (_godMod == true)
            {
                _godMod = false;
            }
            else
            {
                _godMod = true;
            }
        }
    }*/

    private float timer;
    private bool asResetTimer;

    void ScalingTime()
    {
        if (player.GetComponent<ShootBow>().isShooting)
        {
            if (!asResetTimer)
            {
                asResetTimer = true;
                timer = timeBeforeReturnToBaseTimeScale;
            }
                

            if (timer > 0)
            {
                returnToBaseTime = false;
                timer -= Time.deltaTime;

                _timeScale = Mathf.Lerp(_timeScale, _timeScaleMin, speedToGoToMinTimeScale * Time.deltaTime);
            }
            else if (timer <= 0)
            {
                returnToBaseTime = true;
            }
        }

        if (player.GetComponent<ShootBow>().asShoot)
        {
            returnToBaseTime = true;
            asResetTimer = false;
        }

        if (returnToBaseTime)
        {
            _timeScale = Mathf.Lerp(_timeScale, 1, speedToReturnToBaseTimeScale * Time.deltaTime);
        }
    }

    #region Menu
    public void ShowPause()
    {
        DesactivateCanvas(_canvasGame);
        ActivateCanvas(_canvasPause);
    }

    public void HidePause()
    {
        DesactivateCanvas(_canvasPause);
        ActivateCanvas(_canvasGame);
    }

    public void ChangeStateToGame()
    {
        ChangeState(State.Game);

        DesactivateCanvas(_canvasMenuPrincipal);
        DesactivateCanvas(_canvasCredits);
        DesactivateCanvas(_canvasOptions);

        if (player.GetComponent<PlayerSwitcher>().form == 0)
        {
            ActivateCanvas(_canvasGame);
        }
        else
        {
            GameObject[] unSuperbeArrayProvisoireParceQueParfoisIlNeFautPasActiverTousLesCanvas = new GameObject[1];
            unSuperbeArrayProvisoireParceQueParfoisIlNeFautPasActiverTousLesCanvas[0] = _canvasGame[0];

            ActivateCanvas(unSuperbeArrayProvisoireParceQueParfoisIlNeFautPasActiverTousLesCanvas);
        }
            

        SetActiveGoInGame(true);
        
        SetActivePlayerScript(true);

        MusicManager mm = FindObjectOfType(typeof(MusicManager)) as MusicManager;
        mm.isInGame = true;
    }

    public void ButtonReturn()
    {
        if (gameIsPause)
        {
            UnsetOptionsInPause();
        }
        else
        {
            ChangeStateToMenuPrincipal();
        }
    }

    public void ChangeStateToMenuPrincipal()
    {
        ChangeState(State.MenuPrincipal);

        DesactivateCanvas(_canvasGame);
        DesactivateCanvas(_canvasCredits);
        DesactivateCanvas(_canvasOptions);

        ActivateCanvas(_canvasMenuPrincipal);

        SetActiveGoInGame(false);
    }

    public void ChangeStateToCredits()
    {
        ChangeState(State.Credits);

        DesactivateCanvas(_canvasGame);
        DesactivateCanvas(_canvasMenuPrincipal);
        DesactivateCanvas(_canvasOptions);

        ActivateCanvas(_canvasCredits);

        SetActiveGoInGame(false);
    }

    public void ChangeStateToOptions()
    {
        ChangeState(State.Options);

        DesactivateCanvas(_canvasGame);
        DesactivateCanvas(_canvasMenuPrincipal);
        DesactivateCanvas(_canvasCredits);

        ActivateCanvas(_canvasOptions);

        SetActiveGoInGame(false);
    }

    void DesactivateCanvas(GameObject[] canvasToDesactivate)
    {
        foreach (GameObject canva in canvasToDesactivate)
        {
            canva.SetActive(false);
        }
    }

    void ActivateCanvas(GameObject[] canvasToActivate)
    {
        foreach (GameObject canva in canvasToActivate)
        {
            canva.SetActive(true);
        }
    }

    void SetActiveGoInGame(bool active)
    {
        foreach (GameObject go in goInGame)
        {
            go.SetActive(active);
        }
    }

    void SetActivePlayerScript(bool active)
    {
        player.GetComponent<PlayerBirdController>().isInGame = active;
        player.GetComponent<PlayerHumanController>().isInGame = active;
        player.GetComponent<PlayerSwitcher>().isInGame = active;
        player.GetComponent<ShootBow>().isInGame = active;
        player.GetComponent<PlayerLife>().isInGame = active;
        player.GetComponent<DebugPlayer>().enabled = active;

        CameraPositionAndRotationController[] cprcs = player.GetComponentsInChildren<CameraPositionAndRotationController>();
        foreach (CameraPositionAndRotationController cprc in cprcs)
        {
            cprc.enabled = active;
        }
    }

    public void ChangeState(State newstate)
    {
        state = newstate;
    }
    #endregion

    public void Quit()
    {
        Application.Quit();
    }

    public void ReloadLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void MenuByInput()
    {
        /*if (InputManager.FindObjectOfType<InputManager>().GetComponent<InputManager>().m_State == InputManager.eInputState.Controler)
        {
            if (state == State.MenuPrincipal)
            {
                EventSystem.current.SetSelectedGameObject(boutonsManette[0]);
            }

            if (state == State.Options)
            {
                EventSystem.current.SetSelectedGameObject(boutonsManette[1]);
                controlInputs[0].SetActive(false);
                controlInputs[1].SetActive(true);
            }

            if (state == State.Credits)
            {
                EventSystem.current.SetSelectedGameObject(boutonsManette[2]);
            }

            if (state == State.Game && gameIsPause && !optionInPause)
            {
                EventSystem.current.SetSelectedGameObject(boutonsManette[3]);
            }

            if (state == State.Game && gameIsPause && optionInPause)
            {
                EventSystem.current.SetSelectedGameObject(boutonsManette[4]);
                controlInputs[0].SetActive(false);
                controlInputs[1].SetActive(true);
            }
        }

        if (InputManager.FindObjectOfType<InputManager>().GetComponent<InputManager>().m_State == InputManager.eInputState.MouseKeyboard)
        {
            if (state == State.MenuPrincipal)
            {
                EventSystem.current.SetSelectedGameObject(null);
            }

            if (state == State.Options)
            {
                EventSystem.current.SetSelectedGameObject(null);
                controlInputs[0].SetActive(true);
                controlInputs[1].SetActive(false);
            }

            if (state == State.Credits)
            {
                EventSystem.current.SetSelectedGameObject(null);
            }

            if (state == State.Game && gameIsPause)
            {
                EventSystem.current.SetSelectedGameObject(null);
                controlInputs[0].SetActive(true);
                controlInputs[1].SetActive(false);
            }
        }*/

        if (InputManager.FindObjectOfType<InputManager>().GetComponent<InputManager>().m_State == InputManager.eInputState.Controler)
        {
            if (state == State.MenuPrincipal)
            {
                EventSystem.current.SetSelectedGameObject(boutonsManette[0]);
            }

            if (state == State.Options)
            {
                EventSystem.current.SetSelectedGameObject(boutonsManette[1]);
                controlInputs[0].SetActive(false);
                controlInputs[1].SetActive(true);
            }

            if (state == State.Credits)
            {
                EventSystem.current.SetSelectedGameObject(boutonsManette[2]);
            }

            if (state == State.Game && gameIsPause && !optionInPause)
            {
                EventSystem.current.SetSelectedGameObject(boutonsManette[3]);
            }

            if (state == State.Game && gameIsPause && optionInPause)
            {
                EventSystem.current.SetSelectedGameObject(boutonsManette[4]);
                controlInputs[0].SetActive(false);
                controlInputs[1].SetActive(true);
            }
        }

        if (InputManager.FindObjectOfType<InputManager>().GetComponent<InputManager>().m_State == InputManager.eInputState.MouseKeyboard)
        {
            if (state == State.MenuPrincipal)
            {
                EventSystem.current.SetSelectedGameObject(null);
            }

            if (state == State.Options)
            {
                EventSystem.current.SetSelectedGameObject(null);
                controlInputs[0].SetActive(true);
                controlInputs[1].SetActive(false);
            }

            if (state == State.Credits)
            {
                EventSystem.current.SetSelectedGameObject(null);
            }

            if (state == State.Game && gameIsPause)
            {
                EventSystem.current.SetSelectedGameObject(null);
                controlInputs[0].SetActive(true);
                controlInputs[1].SetActive(false);
            }
        }
    }
}
