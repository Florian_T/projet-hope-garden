﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CineCameraController : MonoBehaviour
{
    Animator anim;

    void Start()
    {
        anim = GetComponent<Animator>();
    }
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Keypad0))
        {
            anim.SetInteger("AnimToPlay", 0);
        }

        if (Input.GetKeyDown(KeyCode.Keypad1))
        {
            anim.SetInteger("AnimToPlay", 1);
        }

        if (Input.GetKeyDown(KeyCode.Keypad2))
        {
            anim.SetInteger("AnimToPlay", 2);
        }

        if (Input.GetKeyDown(KeyCode.Keypad3))
        {
            anim.SetInteger("AnimToPlay", 3);
        }

        if (Input.GetKeyDown(KeyCode.Keypad4))
        {
            anim.SetInteger("AnimToPlay", 4);
        }

        if (Input.GetKeyDown(KeyCode.Keypad5))
        {
            anim.SetInteger("AnimToPlay", 5);
        }
    }
}
